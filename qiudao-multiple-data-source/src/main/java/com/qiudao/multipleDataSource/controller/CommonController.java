package com.qiudao.multipleDataSource.controller;

import com.qiudao.multipleDataSource.bo.ali.App;
import com.qiudao.multipleDataSource.bo.iot.Company;
import com.qiudao.multipleDataSource.service.ICommonServcie;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Description: 公共控制方法
 * @author: gdc
 * @date: 2019/10/9
 * @version 1.0
 */
@RestController
@RequestMapping("/comm/v1")
public class CommonController {

    @Resource
    private ICommonServcie commonServcie;

    /**
     * http://localhost:8888/v1/comm/apps
     */
    @RequestMapping(value = "/apps")
    public List<App> getApps(){
        return commonServcie.getApps();
    }

    /**
     * http://localhost:8888/v1/comm/company
     */
    @RequestMapping(value = "/company")
    public List<Company> getCompany(){
        return commonServcie.getCompany();
    }
}
