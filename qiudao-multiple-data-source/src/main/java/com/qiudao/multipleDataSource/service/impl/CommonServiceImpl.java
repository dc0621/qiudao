package com.qiudao.multipleDataSource.service.impl;

import com.qiudao.multipleDataSource.bo.ali.App;
import com.qiudao.multipleDataSource.bo.iot.Company;
import com.qiudao.multipleDataSource.dao.ali.IAppDao;
import com.qiudao.multipleDataSource.dao.iot.ICompanyDao;
import com.qiudao.multipleDataSource.service.ICommonServcie;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * Description: 公共控制方法
 * @author: gdc
 * @date: 2019/10/9
 * @version 1.0
 */
@Service
public class CommonServiceImpl implements ICommonServcie {

    @Resource
    private IAppDao appDao;
    @Resource
    private ICompanyDao companyDao;

    @Override
    public List<App> getApps() {
        return appDao.getApps();
    }

    @Override
    public List<Company> getCompany() {
        return companyDao.findCompanyListByQuery(new HashMap<>());
    }
}
