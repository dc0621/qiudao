package com.qiudao.multipleDataSource.service;

import com.qiudao.multipleDataSource.bo.ali.App;
import com.qiudao.multipleDataSource.bo.iot.Company;

import java.util.List;

/**
 * Description: 公共业务方法
 * @author: gdc
 * @date: 2019/10/9
 * @version 1.0
 */
public interface ICommonServcie {
    List<App> getApps();

    List<Company> getCompany();
}

