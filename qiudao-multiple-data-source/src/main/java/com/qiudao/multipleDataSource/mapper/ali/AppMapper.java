package com.qiudao.multipleDataSource.mapper.ali;


import com.qiudao.multipleDataSource.bo.ali.App;

import java.util.List;
import java.util.Map;

/**
 * @Title: 应用设置表
 * @Author: gdc
 * @Date: 2019/8/15
 * @Return
 */
public interface AppMapper {

    /**
     * 通过查询条件获取应用列表
     * @param map               查询条件
     * @return                  应用列表
     */
    List<App> findAppListByQuery(Map<String, Object> map);

}