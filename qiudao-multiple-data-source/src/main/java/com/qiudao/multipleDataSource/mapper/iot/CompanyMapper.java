package com.qiudao.multipleDataSource.mapper.iot;

import com.qiudao.multipleDataSource.bo.iot.Company;

import java.util.List;
import java.util.Map;

/**
 * @Title: 公司设置表
 * @Author: gdc
 * @Date: 2019/8/15
 * @Return
 */
public interface CompanyMapper {

    /**
     * 通过给定的条件查询
     * @param map       查询条件
     * @return          返回结果
     */
    List<Company> findCompanyListByQuery(Map<String, Object> map);
}