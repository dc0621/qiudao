package com.qiudao.multipleDataSource.dao.ali.impl;

import com.qiudao.multipleDataSource.bo.ali.App;
import com.qiudao.multipleDataSource.dao.ali.IAppDao;
import com.qiudao.multipleDataSource.mapper.ali.AppMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

@Repository
public class AppDaoImpl implements IAppDao {

    @Resource
    private AppMapper appMapper;

    @Override
    public List<App> getApps() {
        return appMapper.findAppListByQuery(new HashMap<>());
    }
}
