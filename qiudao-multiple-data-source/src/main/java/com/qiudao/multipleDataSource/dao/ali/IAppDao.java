package com.qiudao.multipleDataSource.dao.ali;

import com.qiudao.multipleDataSource.bo.ali.App;

import java.util.List;

public interface IAppDao {
    List<App> getApps();
}
