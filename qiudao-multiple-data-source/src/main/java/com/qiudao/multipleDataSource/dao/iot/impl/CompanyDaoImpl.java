package com.qiudao.multipleDataSource.dao.iot.impl;

import com.qiudao.multipleDataSource.bo.iot.Company;
import com.qiudao.multipleDataSource.dao.iot.ICompanyDao;
import com.qiudao.multipleDataSource.mapper.iot.CompanyMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Repository
public class CompanyDaoImpl implements ICompanyDao {

    @Resource
    private CompanyMapper companyMapper;

    @Override
    public List<Company> findCompanyListByQuery(Map<String, Object> map) {
        return companyMapper.findCompanyListByQuery(map);
    }
}
