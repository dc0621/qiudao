package com.qiudao.multipleDataSource.dao.iot;

import com.qiudao.multipleDataSource.bo.iot.Company;

import java.util.List;
import java.util.Map;

/**
 * Description: 公共 Dao方法
 * @author: gdc
 * @date: 2019/10/9
 * @version 1.0
 */
public interface ICompanyDao {
    List<Company> findCompanyListByQuery(Map<String, Object> map);
}
