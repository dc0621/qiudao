package com.qiudao.multipleDataSource.bo.iot;

import com.qiudao.multipleDataSource.bo.BaseBo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Description: 配置公司实体类
 * @author: gdc
 * @date: 2019\8\15 0015
 * @version 1.0
 */
@Setter
@Getter
@ToString
public class Company extends BaseBo implements Serializable {

    @ApiModelProperty(value=" 公司编码，平台唯一自生成")
    private String code;

    @ApiModelProperty(value="公司名称")
    private String name;

    @ApiModelProperty(value="公司外部扩展码，用于第三方机构设置使用")
    private String expandCode;

    private static final long serialVersionUID = 1L;
}