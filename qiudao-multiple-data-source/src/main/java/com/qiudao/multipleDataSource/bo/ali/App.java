package com.qiudao.multipleDataSource.bo.ali;

import com.qiudao.multipleDataSource.bo.BaseBo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Description: APP 实体类
 * @author: gdc
 * @date: 2019\8\15 0015
 * @version 1.0
 */
@Setter
@Getter
@ToString
public class App extends BaseBo implements Serializable {

    @ApiModelProperty(value="应用名称")
    private String name;
    @ApiModelProperty(value="应用编码，平台唯一自生成")
    private String code;
    @ApiModelProperty(value="应用认证密码")
    private String password;
    @ApiModelProperty(value="系统地址")
    private String appUrl;

    private static final long serialVersionUID = 1L;
}