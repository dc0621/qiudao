package com.qiudao.multipleDataSource.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Description: 基础实体类，所有BO 都需要继承此类
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
@Setter
@Getter
public class BaseBo implements Serializable {
    /**
     * ID
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    protected Long id;
    /**
     * 是否有效
     */
    protected Short isValid;
    /**
     * 创建时间
     */
    protected Date createTime;
    /**
     * 创建人ID
     */
    protected String createUserId;
    /**
     * 操作时间
     */
    protected Date opTime;
    /**
     * 版本号
     */
    protected Integer lastVer;
    /**
     * 操作人ID
     */
    protected String opUserId;
    /**
     * 操作人名称
     */
    protected String opUserName;
}
