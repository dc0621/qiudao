
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `app`
-- ----------------------------
DROP TABLE IF EXISTS `app`;
CREATE TABLE `app` (
  `id` bigint(32) NOT NULL COMMENT 'ID',
  `name` varchar(32) NOT NULL COMMENT '应用名称',
  `code` varchar(32) NOT NULL COMMENT '应用编码，平台唯一自生成',
  `password` varchar(32) NOT NULL COMMENT '应用认证密码',
  `app_url` varchar(255) DEFAULT NULL COMMENT '系统地址',
  `is_valid` smallint(2) DEFAULT NULL COMMENT '是否有效 0:无效 1:有效',
  `create_time` datetime DEFAULT NULL COMMENT '记录生成时间',
  `create_user_id` varchar(32) DEFAULT NULL COMMENT '记录生成人ID',
  `last_ver` int(11) DEFAULT NULL COMMENT '版本号',
  `op_time` datetime DEFAULT NULL COMMENT '操作时间',
  `op_user_id` varchar(32) DEFAULT NULL COMMENT '操作人ID',
  `op_user_name` varchar(50) DEFAULT NULL COMMENT '操作人名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='应用表';

-- ----------------------------
-- Records of app
-- ----------------------------
INSERT INTO `app` VALUES ('40', '2.4系统', '00000031', 'EFD0C00ADDB63DF91671A63005962AB3', 'www.baidu.com', '1', '2018-09-13 21:19:00', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('41', '沁水信用社', '00000041', '14B613D9C026017E20949127A06F4925', 'www.baidu.com', '1', '2018-10-08 16:59:36', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('42', '嘉峪关燃气缴费系统', '00000042', '3389F1299A1444D884FDC7C966AA3FB5', '', '1', '2018-10-23 13:52:09', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('43', '唐山市丰南舜丰村镇银行股份有限公司', '00000043', '1D64C58933B54184B502168E11AD8C7D', '', '2', '2018-11-08 10:29:59', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('44', '东营黄河', '00000044', 'F746C76CCBE6D347594D149E3455FA49', 'www.baidu.com', '1', '2018-12-12 13:59:35', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('45', '奥德App', '00000045', '0A7BEB3E761E5293F6A5839B94F80A88', '', '2', '2019-01-16 14:17:17', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('46', '菏泽昆仑App.', '00000046', '8459FBAF986BC7A6E22C8D633C31BB92', '2345.com', '1', '2019-02-12 14:51:50', '1', '3', '2019-08-30 14:59:18', '1', '麦根');
INSERT INTO `app` VALUES ('47', '古交国', '00000047', '9E8755B5C6AD9C3E695F4A38C60B466C', 'www.mi.com', '1', '2019-03-18 09:57:01', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('48', '蓝宝石', '00000048', '271C457225AC6E393B9B293B3DF0AF29', 'www.mi.com', '1', '2019-04-22 09:49:30', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('49', '天津公用燃气', '00000049', 'E891FFC3EA190601A2612262F290677D', 'www.mi.com', '1', '2019-04-22 13:33:28', '1', '2', '2019-08-30 14:49:17', '1', '麦根');
INSERT INTO `app` VALUES ('50', '蓝焰回调App', '00000050', '671884F717DF902889868C31E2ABA1A7', 'www.baidu.com', '1', '2019-04-22 14:17:30', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('51', '罗江龙洲燃气', '00000051', 'BD9AE427E5391D1D9C6BD25CE8731099', '', '1', '2019-04-23 08:57:03', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('52', '测试应用', '00000052', '0A56A7DC9F4ED1C98E5FE4B74957185C', '', '1', '2019-05-28 10:38:46', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('53', '天津西青赛达', '00000053', 'F48EB24E6D5C8A83DDA3F3C749ABE787', '', '1', '2019-06-04 12:22:12', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('54', '山东莱芜', '00000054', '3ABD59337B1CB242E31B20B161719D98', '', '1', '2019-06-13 15:07:44', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('55', '南县华通', '00000055', '850836EC66781ACAF6F7439625D3EDF0', 'www.baidu.com', '1', '2019-06-27 13:24:51', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('56', 'ESLink易联云', '00000056', 'C618FA386DAFC31B04C972AFD4588FCB', '', '1', '2019-06-28 14:23:05', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('57', 'Alipay生活缴费', '00000057', 'DAC79D9038653ED10DD9D55A6471A200', '', '1', '2019-07-11 15:22:25', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('58', '舟山蓝焰', '00000058', '3AAF2086EC241EBDB0415AEA7FA68247', 'www.baidu.com', '1', '2019-08-12 08:56:30', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('59', '梅河口燃气', '00000059', 'AAEDFC52FA090889EE35D28A372562CD', 'www.mi.com', '1', '2019-08-19 09:35:41', '1', '1', '2019-08-20 11:11:20', '1', '麦根');
INSERT INTO `app` VALUES ('60', '超test1', '00000060', '3E39BFDBC2DCF9931C6629406F5C8D29', 'www.baidu.com', '1', '2019-08-19 16:36:49', '1', '2', '2019-08-30 14:49:07', '1', '麦根');
INSERT INTO `app` VALUES ('61', '赵县蓝天真兰', '00000061', '9B7CA33EF05BBCD52A4004BE553C83B2', 'www.baidu.com', '1', '2019-08-20 13:43:54', '1', '1', '2019-08-20 11:11:20', '1', '麦根');


-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `id` bigint(32) NOT NULL COMMENT 'ID',
  `code` varchar(32) NOT NULL COMMENT ' 公司编码，平台唯一自生成',
  `name` varchar(32) NOT NULL COMMENT '公司名称',
  `expand_code` varchar(32) DEFAULT NULL COMMENT '公司外部扩展码，用于第三方机构设置使用',
  `is_valid` smallint(2) DEFAULT NULL COMMENT '是否有效 0:无效 1:有效',
  `create_time` datetime DEFAULT NULL COMMENT '记录生成时间',
  `create_user_id` varchar(32) DEFAULT NULL COMMENT '记录生成人ID',
  `last_ver` int(11) DEFAULT NULL COMMENT '版本号',
  `op_time` datetime DEFAULT NULL COMMENT '操作时间',
  `op_user_id` varchar(32) DEFAULT NULL COMMENT '操作人ID',
  `op_user_name` varchar(50) DEFAULT NULL COMMENT '操作人名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='公司表';

-- ----------------------------
-- Records of company
-- ----------------------------
INSERT INTO `company` VALUES ('1', '1000', '阿里', 'aass', '1', '2019-10-09 22:20:16', '1', '1', '2019-10-09 22:20:23', '1', '张三');

