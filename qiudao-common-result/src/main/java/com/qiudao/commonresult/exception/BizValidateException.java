package com.qiudao.commonresult.exception;

/**
 * Description: 校验异常
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class BizValidateException extends BizException {

    /**
     * <code>serialVersionUID</code>.
     */
    private static final long serialVersionUID = 1L;

    /**
     * 默认异常构造器.
     */
    public BizValidateException() {
        super();
    }

    /**
     * 根据异常信息和原生异常构造对象.
     *
     * @param code    错误码
     * @param message 异常信息.
     * @param cause   原生异常.
     */
    public BizValidateException(final String code, final String message, final Throwable cause) {
        super(code, message, cause);
    }

    /**
     * 根据异常信息和原生异常构造对象.
     *
     * @param message 异常信息.
     * @param cause   原生异常.
     */
    public BizValidateException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * 根据异常信息构造对象.
     *
     * @param message 异常信息.
     */
    public BizValidateException(final String message) {
        super(message);
    }

    /**
     * 根据原生异常构造对象.
     *
     * @param cause 原生异常.
     */
    public BizValidateException(final Throwable cause) {
        super(cause);
    }
}

