package com.qiudao.commonresult.exception;


import com.qiudao.commonresult.share.resultcode.ResultCode;

/**
 * Description: 业务异常
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class ServiceException extends BizException {

    ResultCode resultCode;

    private static final long serialVersionUID = 1L;

    /**
     * 根据异常信息和原生异常构造对象.
     *
     * @param message 异常信息.
     * @param cause   原生异常.
     */
    public ServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * 根据异常信息构造对象.
     *
     * @param message 异常信息.
     */
    public ServiceException(final String message) {
        super(message);
    }

    public ServiceException(ResultCode resultCode) {
        super(resultCode.getMessage(), resultCode.getCode());
        this.resultCode = resultCode;
    }

    /**
     * 根据原生异常构造对象.
     *
     * @param cause 原生异常.
     */
    public ServiceException(final Throwable cause) {
        super(cause);
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }

    public ResultCode getResultCode() {
        return resultCode;
    }
}
