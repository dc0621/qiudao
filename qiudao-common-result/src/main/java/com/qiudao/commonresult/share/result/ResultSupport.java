package com.qiudao.commonresult.share.result;

import com.qiudao.commonresult.share.resultcode.BaseResultCode;
import com.qiudao.commonresult.share.resultcode.ResultCode;

import java.util.ArrayList;
import java.util.List;

/**
 * Description: 返回对象
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class ResultSupport<T> implements Result<T> {

    private static final long serialVersionUID = 4661096805690919752L;

    private boolean success = true;

    private String resultCode;

    private String i18nCode;

    private String message;

    private String tip;

    private T model;

    private int totalRecord;

    /**
     * 创建一个result。
     */
    public ResultSupport() {
    }

    /**
     * 创建一个result。
     *
     * @param success 是否成功
     */
    public ResultSupport(boolean success) {
        this.success = success;
    }

    public ResultSupport(String message, boolean success, T model) {
        this.message = message;
        this.success = success;
        this.model = model;
    }

    public ResultSupport(boolean success, String resultCode, String message) {
        this.success = success;
        this.resultCode = resultCode;
        this.message = message;
    }

    public ResultSupport(String resultCode, String message) {
        this.success = Boolean.FALSE;
        this.resultCode = resultCode;
        this.message = message;
    }

    public ResultSupport(ResultCode resultCode) {
        this.success = false;
        this.resultCode = resultCode.getCode();
        this.message = resultCode.getMessage();
    }

    public ResultSupport(boolean success, T model) {
        this.success = success;
        this.model = model;
    }

    private List<T> models = new ArrayList<T>();

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public T getModel() {
        return model;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }


    @Override
    public boolean isSuccess() {
        return success;
    }


    @Override
    public void setSuccess(boolean success) {
        this.success = success;
    }

    private static boolean isBlank(String str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if ((!Character.isWhitespace(str.charAt(i)))) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return the resultCode
     */
    @Override
    public String getResultCode() {
        if (!isSuccess() && isBlank(resultCode)) {
            return BaseResultCode.SYSTEM_DEFAULT.getCode();
        }
        return resultCode;
    }

    /**
     * @param resultCode the resultCode to set
     */
    @Override
    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    @Override
    public String getI18nCode() {
        return this.i18nCode;
    }

    @Override
    public void setI18nCode(String code) {
        this.i18nCode = code;
    }

    @Override
    public String getTip() {
        return this.tip;
    }

    @Override
    public void setTip(String tip) {
        this.tip = tip;
    }

    public List<T> getModels() {
        return models;
    }

    public void setModels(List<T> models) {
        this.models = models;
    }

    @Override
    public void setModel(T model) {
        this.model = model;
    }
}
