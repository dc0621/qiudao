package com.qiudao.commonresult.share.result;

import java.io.Serializable;

/**
 * Description: 返回结果接口，所有返回结果类都需要实现此类
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public interface Result<T> extends Serializable {

    /**
     * 设置请求成功标志。
     *
     * @param success 成功标志
     */
    void setSuccess(boolean success);

    /**
     * 请求是否成功。
     *
     * @return 如果成功，则返回<code>true</code>
     */
    boolean isSuccess();

    /**
     * 当出现业务异常或系统异常时，返回相应的错误码
     *
     * @return 返回码
     */
    String getResultCode();


    /**
     * 设置返回码
     *
     * @param code
     */
    void setResultCode(String code);


    /**
     * 当出现业务异常或系统异常时，返回相应的国际化错误码
     *
     * @return 返回码
     */
    String getI18nCode();


    /**
     * 设置国际化返回码
     *
     * @param i18nCode
     */
    void setI18nCode(String i18nCode);


    /**
     * 返回提示文案
     *
     * @return 返回提示文案
     */
    String getTip();


    /**
     * 设置提示文案
     *
     * @param tip
     */
    void setTip(String tip);

    /**
     * 取得model对象
     *
     * @return model对象
     */
    T getModel();

    /**
     * 设置model对象。
     *
     * @param model model对象
     */
    void setModel(T model);


    /**
     * 返回信息
     *
     * @return
     */
    String getMessage();

    /**
     * 返回错误信息
     *
     * @param message
     */
    void setMessage(String message);

}
