package com.qiudao.commonresult.share.resultcode;

/**
 * Description: 返回结果状态信息 枚举类
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public enum BaseResultCode implements ResultCode {

    /**
     * 系统默认异常码
     */
    SYSTEM_DEFAULT("SYSTEM_DEFAULT_ERROR", "服务端异常"),
    SYSTEM("SYSTEM_ERROR_1001", "系统异常"),
    DUBBO_REMOTE("SYSTEM_ERROR_DUBBO_1002", "服务调用异常"),
    SAVE_DATA("SAVE_DATA_1003", "保存数据失败"),
    UPDATE_DATA("UPDATE_DATA_1003", "修改数据失败"),
    OBJECT_NULL("OBJECT_NULL_1004", "对象不能为空"),
    FIELD_NULL("FIELD_NULL_1005", "属性不能为空"),
    RECORD_NOT_EXIST("RECORD_NOT_EXIST_1006", "记录不存在");

    BaseResultCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;

    private String message;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
