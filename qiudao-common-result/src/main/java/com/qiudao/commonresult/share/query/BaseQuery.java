package com.qiudao.commonresult.share.query;

import java.io.Serializable;
import java.util.List;

/**
 * Description:
 * query基类:
 *    你可以按照下面这种方式来编写service层代码：
 *       List<Sort> sorts = Sorts.newDefaultSorts();
 *       BaseQuery query = new BaseQuery();// Maybe you define an DemoQuery extends BaseQuery
 *       query.setSorts(sorts);
 *    默认创建的排序规则是按照业务表的create_time字段降序排列，如果业务需求需要按照表的其它字段(或者多个字段的)进行排序，
 *       List<Sort> sorts = Arrays.asList(new Sort("status", Sort.SortType.DESC), new Sort("name", Sort.SortType.ASC));
 *       query.setSorts(sorts);
 *     会是一个不错的选择
 * 在mapper.xml中你可以这样来使用查询类
 *      <select id="findListByQuery" resultMap="resultMap" parameterType="com.dfire.soa.consumer.ocean.retail.query.demo.DemoQuery">
 *         SELECT
 *           <include refid="queryColumns" />
 *                  FROM t_demo
 *              <include refid="queryParams" />
 *                  <if test="sorts != null and sorts.size > 0">
 *                      order by
 *                      <foreach collection="sorts" item="sort" separator=",">
 *                          ${sort.columnName} ${sort.sortType}
 *                      </foreach>
 *                  </if>
 *       </select>
 * 这样做的好处是使用一条动态SQL解决你所有的列表类排序查询业务，有效的控制了mapper文件中的SQL泛滥问题。更好一点的实践方法是：
 * 对DAO层基础的CRUD操作应该有一个基础性的接口IBaseDAO及一个基础的实现类BaseDAO来统一封装掉数据库的操作细节。
 *
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class BaseQuery implements Serializable {

    private static final long serialVersionUID = 5537264124199030276L;

    /**
     * 缺省页大小
     */
    public static final int DEFAULT_PAGE_SIZE = 20;

    /**
     * 最大页大小
     **/
    public static final int MAX_PAGE_SIZE = 1000;

    /**
     * 页大小
     */
    protected int pageSize;
    /**
     * 总数
     */
    private int totalRecord;
    /**
     * 当前页
     */
    private int pageIndex;

    /**
     * 总页数
     */
    private int totalPage;

    /**
     * 排序：参考：Constants.ORDERBY_*
     */
    private String orderBy;

    /**
     * 默认desc=true,desc=false即asc
     */
    private Boolean desc;

    /**
     * 排序列表
     */
    private List<Sort> sorts;

    public int getPageSize() {
        if (pageSize < 1) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    /**
     * 总条目数
     *
     * @param totalRecord
     */
    public void setTotalRecord(int totalRecord) {
        this.totalPage = (totalRecord + getPageSize() - 1) / getPageSize();
        this.totalRecord = totalRecord;
    }

    /**
     * 当前页
     *
     * @return
     */
    public int getPageIndex() {
        if (pageIndex < 1) {
            pageIndex = 1;
        }

        return pageIndex;
    }

    /**
     * 获取传进来的原始的pageIndex
     *
     * @param
     * @return int
     * @throws
     */
    public int getOriginalPageIndex() {
        return pageIndex;
    }

    public boolean hasNextPage() {
        if (pageIndex <= getTotalPage()) {
            return true;
        } else {
            return false;
        }
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }


    /**
     * mysqlDB开始位置
     *
     * @return
     */
    public int getStartPos() {
        int cPage = getPageIndex();

        if (cPage <= 1) {
            return 0;
        }

        cPage--;

        int pgSize = getPageSize();

        return (pgSize * cPage);
    }

    /**
     * 结束位置
     *
     * @return
     */
    public int getEndPos() {
        /**
         * 未设置总记录数或设置了总记录数 页码*每页数量<总记录数
         */
        if (getTotalRecord() == 0 || getPageIndex() * getPageSize() < getTotalRecord()) {
            return getPageIndex() * getPageSize();
        } else {
            return getTotalRecord();
        }
    }

    /**
     * 总页数
     *
     * @return
     */
    public int getTotalPage() {
        return totalPage;
    }

    /**
     * 排序：参考：Constants.ORDERBY_*
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * 排序：参考：Constants.ORDERBY_*
     */
    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    /**
     * 默认desc=true,desc=false即asc
     */
    public Boolean getDesc() {
        return desc;
    }

    /**
     * 默认desc=true,desc=false即asc
     */
    public void setDesc(Boolean desc) {
        this.desc = desc;
    }

    public List<Sort> getSorts() {
        return sorts;
    }

    public void setSorts(List<Sort> sorts) {
        this.sorts = sorts;
    }
}

