package com.qiudao.commonresult.share.query;

import java.io.Serializable;

/**
 * Description:  数据库查询排序
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class Sort implements Serializable {

    private static final long serialVersionUID = -8091016526771774468L;

    private String columnName;

    private String sortType;

    public Sort() {
    }

    public Sort(String columnName, SortType sortType) {
        this.columnName = columnName;
        this.sortType = sortType.getName();
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(SortType sortType) {
        this.sortType = sortType.getName();
    }

    public enum SortType {
        /**
         * 排序方式枚举
         */
        DESC("desc"), ASC("asc");

        private String name;

        private SortType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
