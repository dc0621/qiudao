package com.qiudao.commonresult.share.query;

import java.util.ArrayList;
import java.util.List;

/**
 * Description:    排序方式
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class Sorts {
    /**
     * 创建一个默认的排序对象，按照create_time降序排列
     *
     * @return
     */
    public static List<Sort> newDefaultSorts() {
        return newSortsOrderByCreateTimeDesc();
    }

    /**
     * 创建一个排序对象列表，按照create_time降序排序
     *
     * @return
     */
    public static List<Sort> newSortsOrderByCreateTimeDesc() {
        List<Sort> sorts = new ArrayList<>();
        sorts.add(new Sort("create_time", Sort.SortType.DESC));
        return sorts;
    }

    /**
     * 创建一个排序对象列表，按照create_time升序排序
     *
     * @return
     */
    public static List<Sort> newSortsOrderByCreateTimeAsc() {
        List<Sort> sorts = new ArrayList<>();
        sorts.add(new Sort("create_time", Sort.SortType.ASC));
        return sorts;
    }
}
