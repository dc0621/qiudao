package com.qiudao.commonresult.share.resultcode;

/**
 * Description: 返回结果状态信息接口，所有的状态信息都需要实现该接口
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public interface ResultCode {

    /**
     * 获取code
     *
     * @return 获取code
     */
    String getCode();

    /**
     * 获取消息
     *
     * @return 获取消息
     */
    String getMessage();

}
