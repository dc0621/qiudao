package com.qiudao.commandlinerunner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CommandlineRunnerApplication {

    public static void main(String[] args) {
        System.out.println("The service to start.");
        SpringApplication.run(CommandlineRunnerApplication.class, args);
        System.out.println("The service has started.");
    }

}
