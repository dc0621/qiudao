package com.qiudao.commandlinerunner.runner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


/**
 * Description: 实现CommandLineRunner接口；容器启动之后，加载实现类的逻辑资源，已达到完成资源初始化的任务（类似开机自启动）
 * - @Order 执行的优先级根据从小到大顺序
 *
 * @author: gdc
 * @date: 2021/6/4
 * @version 1.0
 */
@Order(1)
@Component
public class OrderRunner1 implements CommandLineRunner {

    @Override
    public void run(String... args) {
        System.out.println("OrderRunner1 init ....");
    }

}
