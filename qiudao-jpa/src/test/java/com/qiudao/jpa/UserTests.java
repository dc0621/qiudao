package com.qiudao.jpa;

import com.qiudao.jpa.bean.User;
import com.qiudao.jpa.dao.UserDao;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTests {

    @Resource
    private UserDao userDao;

    @Test
    public void addTest(){
        User user = new User();
        user.setId(2L);
        user.setName("杨过");
        user.setSex(true);
        user.setBirthday(new Date());
        user.setCreateDate(new Date());
        User newUser = userDao.save(user);
        log.info("newUser = {}", newUser);
    }

    @Test
    public void mergeTest(){
        User user = new User();
        user.setName("杨2");
        user.setSex(true);
        user.setBirthday(new Date());
        user.setCreateDate(new Date());
        User newUser = userDao.save(user);
        log.info("newUser = {}", newUser);
    }

    /**
     * 通过ID获取用户信息
     */
    @Test
    public void findUserTest(){
        Optional<User> userOp = userDao.findById(1L);
        if (userOp.isPresent()) {
            User user = userOp.get();
            log.info("user= {}", user);
        }
    }

    @Test
    public void findAllUserTest(){
        List<User> users = userDao.findAll();
        users.forEach(user -> {
            log.info(">>> {}", user );
        });
    }

    @Test
    public void findUserByName(){
        User user = userDao.findByName("杨过");
        log.info(">>> {}", user );
    }

    @Test
    public void updateUserTest(){
        User user = userDao.findById(1L).get();
        user.setSex(false);
        User newUser = userDao.save(user);
        log.info("userUser>>> ", newUser);
    }

    @Test
    public void UserTest(){
        Optional<User> userOP = userDao.findById(1L);
        if (userOP.isPresent()) {
            User user = userOP.get();
            userDao.delete(user);
        }
//        userDao.deleteById(2L);
//        userDao.deleteAll();
    }

}
