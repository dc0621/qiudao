package com.qiudao.jpa;

import com.qiudao.jpa.bean.Person;
import com.qiudao.jpa.dao.PersonRepostory;
import com.qiudao.jpa.service.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonTests {

    @Resource
    private PersonRepostory personRepostory;
    @Resource
    private PersonService personService;



    @Test
    public void testJpaRepository(){
        Person person = new Person();
        person.setBirth(new Date());
        person.setEmail("xy@atguigu.com");
        person.setLastName("xyz");
        person.setId(28);

        Person person2 = personRepostory.saveAndFlush(person);

        System.out.println(person == person2);
    }

    @Test
    public void testPagingAndSortingRespository(){
        //pageNo 从 0 开始.
        int pageNo = 6 - 1;
        int pageSize = 5;
        //Pageable 接口通常使用的其 PageRequest 实现类. 其中封装了需要分页的信息
        //排序相关的. Sort 封装了排序的信息
        //Order 是具体针对于某一个属性进行升序还是降序.
        Sort.Order order1 = new Sort.Order(Sort.Direction.DESC, "id");
        Sort.Order order2 = new Sort.Order(Sort.Direction.ASC, "email");
        Sort sort = Sort.by(order1, order2);

        PageRequest pageable = PageRequest.of(pageNo, pageSize, sort);
        Page<Person> page = personRepostory.findAll(pageable);

        System.out.println("总记录数: " + page.getTotalElements());
        System.out.println("当前第几页: " + (page.getNumber() + 1));
        System.out.println("总页数: " + page.getTotalPages());
        System.out.println("当前页面的 List: " + page.getContent());
        System.out.println("当前页面的记录数: " + page.getNumberOfElements());
    }

    @Test
    public void testCrudReposiory(){
        List<Person> persons = new ArrayList<>();

        for(int i = 'a'; i <= 'z'; i++){
            Person person = new Person();
            person.setBirth(new Date());
            person.setEmail((char)i + "" + (char)i + "@atguigu.com");
            person.setLastName((char)i + "" + (char)i);

            persons.add(person);
        }

        personService.savePersons(persons);
    }

    @Test
    public void testModifying(){
//		personRepostory.updatePersonEmail(1, "mmmm@atguigu.com");
        personService.updatePersonEmail(1,"mmmm@atguigu.com");
    }

    @Test
    public void testNativeQuery(){
        long count = personRepostory.getTotalCount();
        System.out.println(count);
    }

    @Test
    public void testQueryAnnotationLikeParam(){
//		List<Person> persons = personRepostory.testQueryAnnotationLikeParam("%A%", "%bb%");
//		System.out.println(persons.size());

//		List<Person> persons = personRepostory.testQueryAnnotationLikeParam("A", "bb");
//		System.out.println(persons.size());

        List<Person> persons = personRepostory.testQueryAnnotationLikeParam2("bb", "A");
        System.out.println(persons.size());
    }

    @Test
    public void testQueryAnnotationParams2(){
        List<Person> persons = personRepostory.testQueryAnnotationParams2("aa@atguigu.com", "AA");
        System.out.println(persons);
    }

    @Test
    public void testQueryAnnotationParams1(){
        List<Person> persons = personRepostory.testQueryAnnotationParams1("AA", "aa@atguigu.com");
        System.out.println(persons);
    }

    @Test
    public void testQueryAnnotation(){
        Person person = personRepostory.getMaxIdPerson();
        System.out.println(person);
    }

    @Test
    public void testKeyWords(){
        List<Person> persons = personRepostory.getByLastNameStartingWithAndIdLessThan("X", 10);
        System.out.println(persons);

        persons = personRepostory.getByLastNameEndingWithAndIdLessThan("X", 10);
        System.out.println(persons);

        persons = personRepostory.getByEmailInAndBirthLessThan(Arrays.asList("AA@atguigu.com", "FF@atguigu.com",
                "SS@atguigu.com"), new Date());
        System.out.println(persons.size());
    }

    @Test
    public void testHelloWorldSpringData() {
        System.out.println(personRepostory.getClass().getName());

        Person person = personRepostory.getByLastName("AA");
        System.out.println(person);
    }


}
