package com.qiudao.jpa.dao;

import com.qiudao.jpa.bean.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Description: 用户服务数据接口类
 * @Author: gdc
 * @date: 2019/8/11
 * @version 1.0
 */
@Repository
public interface UserDao extends JpaRepository<User, Long> {

    /**
     * 通过名称查询 User
     * @param name  名称
     * @return      User
     */
    User findByName(String name);

    /**
     * 通过名称和性别查询User
     * @param name  名称
     * @param sex   性别
     * @return      User
     */
    User findByNameAndSex(String name, boolean sex);

    User findByNameOrAndBirthday(String name, Date birthday);
    User findByNameLike(String name);
    User findByNameNotLike(String name);
    User findByCreateDateLessThan(Date createDate);
    User findByCreateDateIn(Collection<Date> dates);
    User findByNameLikeOrderByBirthdayDesc(String name);
    User findByBirthdayIsNull();
    User findByBirthdayNotNull();
    User findBySexIsTrue();
    User findBySexFalse();


    /**
     * 自定义查询
     * 添加 nativeQuery = true 的属性，就可以采用原生 SQL 语句的方式来编写查询
     * @param name1     名称1
     * @param name2     名称2
     * @return          User 列表
     */
    @Query(nativeQuery = true, value = "SELECT * from auth_user u where u.user_name in (:name1, :name2)")
    List<User> findTwoUser(@Param("name1") String name1, @Param("name2") String name2);

}
