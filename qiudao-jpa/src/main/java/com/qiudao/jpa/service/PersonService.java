package com.qiudao.jpa.service;

import com.qiudao.jpa.bean.Person;
import com.qiudao.jpa.dao.PersonRepostory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Description: Person 业务层
 * Transactional > 添加事务支持
 * @author: gdc
 * @date: 2019/8/11
 * @version 1.0
 */
@Transactional
@Service
public class PersonService {

    @Resource
    private PersonRepostory personResptory;

    public void savePersons(List<Person> persons){
        personResptory.saveAll(persons);
    }


    public void updatePersonEmail(Integer id, String email){
        personResptory.updatePersonEmail(id, email);
    }
}
