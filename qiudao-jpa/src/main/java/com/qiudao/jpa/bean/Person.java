package com.qiudao.jpa.bean;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Table(name = "jpa_person")
@Entity
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String lastName;
    private String email;
    @Temporal(TemporalType.DATE)
    private Date birth;

}
