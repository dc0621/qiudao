package com.qiudao.jpa.bean;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;



/**
 * @Description 创建一个User 表
 *
 * @Author gdc
 * @Date 2019/8/11
*/



// @Entity 是一个必选的注解，声明这个类对应了一个数据库表。
// @Table(name = "AUTH_USER") 是一个可选的注解。声明了数据库实体对应的表信息。包括表名称、索引信息等。这里声明这个实体类对应的表名是 AUTH_USER。如果没有指定，则表名和实体的名称保持一致。
@Data
@Entity
@Table(name = "AUTH_USER")
public class User {
    //  @Id 注解声明了实体唯一标识对应的属性。
    // @GeneratedValue 主键生成方式：IDENTITY 主键由数据库生成, 采用数据库自增长
    //                             AUTO主键由程序控制, 是默认选项 ,不设置就是这个
    //                             SEQUENCE 通过数据库的序列产生主键, MYSQL  不支持
    //                             Table 提供特定的数据库产生主键, 该方式更有利于数据库的移植
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    // @Column 用来声明实体属性的表字段的定义。字段的类型根据实体属性类型自动推断
    // name: 定义了被标注字段在数据库表中所对应字段的名称
    // unique: 表示该字段是否为唯一标识，默认为false
    // nullable: 表示该字段是否可以为null值，默认为true
    // insertable: 表示在使用“INSERT”脚本插入数据时，是否需要插入该字段的值。
    // updatable: 表示在使用“UPDATE”脚本插入数据时，是否需要更新该字段的值。insertable和updatable属性一般多用于只读的属性，例如主键和外键等。这些字段的值通常是自动生成的。
    // table: 表示当映射多个表时，指定表的表中的字段。默认值为主表的表名。
    // length: 表示字段的长度，当字段的类型为varchar时，该属性才有效，默认为255个字符。
    // precision和scale precision属性和scale属性表示精度，当字段类型为double时，precision表示数值的总长度，scale表示小数点所占的位数
    @Column(name = "user_name", nullable = false, length = 32)
    private String name;
    @Column(nullable = false,length = 1)
    private boolean sex;
    @Temporal(TemporalType.DATE)
    private Date birthday;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    // @Transient表示该属性并非一个到数据库表的字段的映射,ORM框架将忽略该属性. 如果一个属性并非数据库表的字段映射,就务必将其标示为@Transient,否则,ORM框架默认其注解为@Basic
    @Transient
    public String getInfo(){
        return "name=" + name + ", sex= " + sex;
    }
}
