package com.qiudao.idgenerator.service;


import com.qiudao.idgenerator.bo.Id;

import java.util.Date;

/**
 * Description:
 * 生成和反解唯一的ID
 *
 * @author liuhuan
 * @version 1.0
 * @date 2019-03-23  14:22
 */
public interface IdService {

    /**
     * 生成唯一ID
     *
     * @return ID内容
     */
    long genId();

    /**
     * 反解ID内容
     *
     * @param id ID内容
     * @return 反解结果
     */
    Id expId(long id);

    /**
     * 人为伪造某一时间的ID
     *
     * @param time 时间戳
     * @param seq  序列号
     * @return 生成的唯一ID
     */
    long makeId(long time, long seq);

    /**
     * 人为伪造某一时间的ID
     *
     * @param time    时间戳
     * @param seq     序列号
     * @param machine 机器码
     * @return 生成的唯一ID
     */
    long makeId(long time, long seq, long machine);

    /**
     * 人为伪造某一时间的ID
     *
     * @param genMethod 生成方式
     * @param time      时间戳
     * @param seq       序列号
     * @param machine   机器码
     * @return 生成的唯一ID
     */
    long makeId(long genMethod, long time, long seq, long machine);

    /**
     * 人为伪造某一时间的ID
     *
     * @param type      ID类型
     * @param genMethod 生成方式
     * @param time      时间戳
     * @param seq       序列号
     * @param machine   机器码
     * @return 生成的唯一ID
     */
    long makeId(long type, long genMethod, long time, long seq, long machine);

    /**
     * 人为伪造某一时间的ID
     *
     * @param version   版本号
     * @param type      ID类型
     * @param genMethod 生成方式
     * @param time      时间戳
     * @param seq       序列号
     * @param machine   机器码
     * @return 生成的唯一ID
     */
    long makeId(long version, long type, long genMethod, long time, long seq, long machine);

    /**
     * 整形时间转换为格式化时间
     *
     * @param time 时间戳
     * @return 结果
     */
    Date transTime(long time);
}
