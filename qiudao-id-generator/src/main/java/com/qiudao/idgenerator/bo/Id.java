package com.qiudao.idgenerator.bo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Description: Id 对象
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Id {

    /**
     * 序列号
     * 分为两类
     * 最大峰值型20位
     * 最小力度型10位
     */
    private Long seq;

    /**
     * 时间戳
     * 最大峰值型  30位
     * 最小力度型 40位
     */
    private Long time;

    /**
     * 生成方式 2位
     * 00:嵌入式发布
     * 01:中心服务器发布模式
     * 02:REST发布
     * 03:保留
     */
    private Long genMethod;

    /**
     * ID类型 1位
     * 0:最大峰值型
     * 1:最小力度型
     */
    private Long type;

    /**
     * 版本 1位
     * 用来扩展
     */
    private Long version;

}
