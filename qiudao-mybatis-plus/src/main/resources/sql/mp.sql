-- 创建库
CREATE DATABASE mp;
-- 使用库
USE mp;
-- 创建表
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `last_name` varchar(20) DEFAULT NULL COMMENT '名称',
  `email` varchar(30) DEFAULT NULL COMMENT '邮箱',
  `gender` tinyint(1) DEFAULT NULL COMMENT '性别',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `is_del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否删除，0否，1是',
  `create_by` int(11) NOT NULL DEFAULT '0' COMMENT '创建人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` int(11) NOT NULL DEFAULT '0' COMMENT '修改人',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;
INSERT INTO employee(`last_name`, `email`, `gender`, `age`, `is_del`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('Tom', 'tom@atguigu.com', '1', 22, 0, 0, now(), 0, now());
INSERT INTO employee(`last_name`, `email`, `gender`, `age`, `is_del`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('Jerry', 'jerry@atguigu.com', '0', 25, 0, 0, now(), 0, now());
INSERT INTO employee(`last_name`, `email`, `gender`, `age`, `is_del`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('Black', 'black@atguigu.com', '1', 30, 0, 0, now(), 0, now());
INSERT INTO employee(`last_name`, `email`, `gender`, `age`, `is_del`, `create_by`, `create_time`, `update_by`, `update_time`) VALUES ('White', 'white@atguigu.com', '0', 35, 0, 0, now(), 0, now());



DROP TABLE IF EXISTS `people`;
CREATE TABLE `people`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
	`manager_id` bigint(20) NULL DEFAULT NULL COMMENT '直属上级id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

INSERT INTO `people` VALUES (1, 'dyz-ann', 18, 'dyz@163.com', NULL, now());
INSERT INTO `people` VALUES (2, '大boss', 40, 'boss@baomidou.com', NULL, now());
INSERT INTO `people` VALUES (3, '王天风', 25, 'wtf@baomidou.com', 2, now());
INSERT INTO `people` VALUES (4, '李艺伟', 28, 'lyw@baomidou.com', 3, now());
INSERT INTO `people` VALUES (5, '张雨琪', 31, 'zjq@baomidou.com', 3, now());
INSERT INTO `people` VALUES (6, '刘红雨', 32, 'lhm@baomidou.com', 3, now());