package com.qiudao.mybatisplus.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Description: 基础 请求参数
 * @author: gdc
 * @date: 2021/6/2
 * @version 1.0
 */
@Data
public class BaseRequest implements Serializable {

    @ApiModelProperty(value = "当前页码")
    private int currPage = 1;

    @ApiModelProperty(value = "每页记录数")
    private int pageSize = 10;

}