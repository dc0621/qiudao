package com.qiudao.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qiudao.mybatisplus.entity.People;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


public interface PeopleMapper extends BaseMapper<People> {

    /**
     * 自定义查询
     * @param wrapper   包装条件
     * @return          对象列表
     */
    @Select("select * from mp_people ${ew.customSqlSegment}")
    List<People> selectCustomSize(@Param(Constants.WRAPPER) Wrapper wrapper);

    /**
     * 自定义分页查询
     * @param page
     * @param wrapper
     * @return
     */
    IPage<People> selectPeoplePage(Page<People> page, @Param(Constants.WRAPPER) Wrapper<People> wrapper);
}
