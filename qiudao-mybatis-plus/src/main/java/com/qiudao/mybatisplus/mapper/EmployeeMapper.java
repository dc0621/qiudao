package com.qiudao.mybatisplus.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qiudao.mybatisplus.entity.Employee;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author gdc
 * @since 2021-03-27
 */
public interface EmployeeMapper extends BaseMapper<Employee> {

}
