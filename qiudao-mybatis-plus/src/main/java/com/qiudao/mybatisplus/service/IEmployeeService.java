package com.qiudao.mybatisplus.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.qiudao.mybatisplus.entity.Employee;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author gdc
 * @since 2021-03-27
 */
public interface IEmployeeService extends IService<Employee> {

}
