package com.qiudao.mybatisplus.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qiudao.mybatisplus.entity.Employee;
import com.qiudao.mybatisplus.request.EmployeeRequest;
import com.qiudao.mybatisplus.service.IEmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author gdc
 * @since 2021-03-27
 */
@RestController
@RequestMapping("/employee")
@Api(value = "员工业务", tags = "员工业务")
public class EmployeeController {

    @Autowired
    private IEmployeeService employeeService;

    @GetMapping("/get/{id}")
    @ApiOperation(value = "通过ID查询", notes = "通过ID查询")
    public Employee get(@PathVariable(value = "id") Long id) {
        return employeeService.getById(id);
    }

    @GetMapping("/list")
    @ApiOperation(value = "列表查询", notes = "列表查询")
    public List<Employee> list() {
        return employeeService.list();
    }

    @PostMapping("/page")
    @ApiOperation(value = "分页查询", notes = "分页查询")
    public IPage<Employee> page(@RequestBody EmployeeRequest request) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(request, employee);
        QueryWrapper queryWrapper = new QueryWrapper<Employee>().setEntity(employee);
        return employeeService.page(new Page<>(request.getCurrPage(), request.getPageSize()), queryWrapper);
    }

    @PostMapping("/insert")
    @ApiOperation(value = "新增员工信息", notes = "新增员工信息")
    public Boolean insert(@RequestBody EmployeeRequest request){
        Employee employee = new Employee();
        BeanUtils.copyProperties(request, employee);
        return employeeService.save(employee);
    }
}
