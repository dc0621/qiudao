package com.qiudao.mybatisplus.util;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Description: 演示例子，执行 main 方法控制台输入模块表名回车自动生成对应项目目录中
 * @author: gdc
 * @date: 2021/3/27
 * @version 1.0
 */
public class CodeGenerator {

    /**
     * <p>
     * 读取控制台内容
     * </p>
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder help = new StringBuilder();
        help.append("请输入" + tip + "：");
        System.out.println(help.toString());
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotBlank(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }

    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        String projectPath = System.getProperty("user.dir");

        // step 1 全局配置
        mpg.setGlobalConfig(getGlobalConfig(projectPath));

        // step 2 数据源配置
        mpg.setDataSource(getDataSourceConfig());

        // step 3 包配置
        PackageConfig pc = getPackageConfig();
        mpg.setPackageInfo(pc);

        // step 4 自定义配置
        mpg.setCfg(getInjectionConfig(projectPath));

        // step 5 配置模板
        //mpg.setTemplate(getTemplateConfig());

        // step 6 策略配置
        mpg.setStrategy(getStrategyConfig(pc));
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }


    /**
     * 获取全局配置
     */
    private static GlobalConfig getGlobalConfig(String projectPath) {
        GlobalConfig gc = new GlobalConfig();
        gc.setOutputDir(projectPath + "/qiudao-mybatis-plus/src/main/java");
        gc.setAuthor("gdc");
        gc.setOpen(false);
        gc.setSwagger2(true);                                           // 实体属性 Swagger2 注解
        //gc.setActiveRecord(true);                                     // 是否支持 AR 模式
        gc.setFileOverride(true);                                       // 是否文件覆盖
        gc.setDateType(DateType.TIME_PACK);                             // 日期类型（JAVA8）
        gc.setBaseResultMap(true);                                      // 生成 BaseResultMap
        gc.setBaseColumnList(true);                                     // 生成 列信息
        return gc;
    }

    /**
     * 获取数据源配置
     */
    private static DataSourceConfig getDataSourceConfig() {
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://192.168.2.100:3306/mp?characterEncoding=utf8&useUnicode=true&useSSL=false&serverTimezone=UTC");
        // dsc.setSchemaName("public");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("123456");
        return dsc;
    }

    /**
     * 获取包配置
     */
    private static PackageConfig getPackageConfig() {
        PackageConfig pc = new PackageConfig();
        //pc.setModuleName(scanner("模块名"));                        // 设置模块名称
        pc.setParent("com.qiudao.qiudaoMybatisPlus");               // 父包名
        //pc.setEntity("bo");                                             // 设置实体类的包名
        //pc.setMapper("mapper");
        //pc.setService("service");
        //pc.setController("controller");
        return pc;
    }

    /**
     * 获取自定义配置
     */
    private static InjectionConfig getInjectionConfig(String projectPath) {
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        // 如果模板引擎是 freemarker
        String templatePath = "/templates/mapper.xml.ftl";
        // 如果模板引擎是 velocity
        // String templatePath = "/templates/mapper.xml.vm";

        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return projectPath + "/qiudao-mybatis-plus/src/main/resources/com/gdc/qiudao/qiudaoMybatisPlus/mapper/"
                        //+ pc.getModuleName()
                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        /*
        cfg.setFileCreate(new IFileCreate() {
            @Override
            public boolean isCreate(ConfigBuilder configBuilder, FileType fileType, String filePath) {
                // 判断自定义文件夹是否需要创建
                checkDir("调用默认方法创建的目录，自定义目录用");
                if (fileType == FileType.MAPPER) {
                    // 已经生成 mapper 文件判断存在，不想重新生成返回 false
                    return !new File(filePath).exists();
                }
                // 允许生成模板文件
                return true;
            }
        });
        */
        cfg.setFileOutConfigList(focList);
        return cfg;
    }

    /**
     * 获取配置模板
     */
    private static TemplateConfig getTemplateConfig() {
        TemplateConfig templateConfig = new TemplateConfig();

        // 配置自定义输出模板
        //指定自定义模板路径，注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别
        //templateConfig.setEntity("templates/entity2.java");
        //templateConfig.setService();
        //templateConfig.setController();
        //templateConfig.setXml(null);
        return templateConfig;
    }

    /**
     * 获取策略配置
     */
    private static StrategyConfig getStrategyConfig(PackageConfig pc) {
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);              // 从数据库表到文件的命名策略
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);        // 数据库表字段映射到实体的命名策略

        strategy.setEntityLombokModel(true);                                // 是否设置lombok 注解
        strategy.setEntitySerialVersionUID(true);                           // 实体是否生成 serialVersionUID

        strategy.setSuperEntityClass("com.qiudao.qiudaoMybatisPlus.entity.BaseEntity");                         // 自定义继承的父类,没有就不需要设置
        strategy.setSuperEntityColumns("id", "is_del", "create_by", "create_time", "update_by", "update_time");     // 写于父类中的公共字段

        //strategy.setSuperControllerClass("你自己的父类控制器,没有就不用设置!");   // 自定义继承的Controller类全称
        strategy.setRestControllerStyle(true);                              // Controller 是否使用 @RestController 注解

        strategy.setInclude(scanner("表名，多个英文逗号分割").split(","));
        strategy.setControllerMappingHyphenStyle(true);
        strategy.setTablePrefix(pc.getModuleName() + "_");                  // 表前缀
        //strategy.setFieldPrefix("");                                      // 字段前缀

        List<TableFill> tableFills = Lists.newArrayList();
        tableFills.add(new TableFill("create_time", FieldFill.INSERT));
        tableFills.add(new TableFill("create_id", FieldFill.INSERT));
        tableFills.add(new TableFill("update_time", FieldFill.UPDATE));
        tableFills.add(new TableFill("update_id", FieldFill.UPDATE));
        tableFills.add(new TableFill("is_del", FieldFill.INSERT));
        strategy.setTableFillList(tableFills);                              // 设置 表填充字段
        strategy.setLogicDeleteFieldName("is_del");                         // 设置逻辑删除字段
        //strategy.setVersionFieldName("version");                            // 乐观锁属性名称
        return strategy;
    }

}