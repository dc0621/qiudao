package com.qiudao.mybatisplus.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.*;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 定义实体类 people 与数据库表 people 绑定
 * // @TableName 用来设置填写对应的表名
 * // @Data相当于@Getter @Setter @RequiredArgsConstructor @ToString @EqualsAndHashCode这5个注解的合集。
 * 当使用@Data注解时，则有了@EqualsAndHashCode注解，那么就会在此类中存在equals(Object other) 和 hashCode()方法，且不会使用父类的属性，这就导致了可能的问题。
 * 比如，有多个类有相同的部分属性，把它们定义到父类中，恰好id（数据库主键）也在父类中，那么就会存在部分对象在比较时，它们并不相等，
 * 却因为lombok自动生成的equals(Object other) 和 hashCode()方法判定为相等，从而导致出错。
 *
 * 修复此问题的方法很简单：
 * 1. 使用@Getter @Setter @ToString代替@Data并且自定义equals(Object other) 和 hashCode()方法，比如有些类只需要判断主键id是否相等即足矣。
 * 2. 或者使用在使用@Data时同时加上@EqualsAndHashCode(callSuper=true)注解。
 */
@TableName("people")
@Getter
@Setter
@ToString
@Builder
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class People extends Model<People> {

    /**
     * // @TableId和@TableField 不能同时使用，如果数据库中主键名称是id，实体属性是userId，你可以这样映射@TableId("id")
     * 主键策略6种：
     * AUTO(0),         数据库自增
     * NONE(1),         无状态
     * INPUT(2),        自行输入
     * ID_WORKER(3),    分布式全局唯一ID 长整型类型
     * UUID(4),         32位UUID字符串
     * ID_WORKER_STR(5);    分布式全局唯一ID 字符串类型
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * //@TableField      字段注解
     * strategy 添加策略，该属性在3.1.2 后被其他属性替换 insertStrategy 、 updateStrategy 、   whereStrategy
     */
    @TableField(value = "name", insertStrategy = FieldStrategy.NOT_NULL, updateStrategy = FieldStrategy.NOT_NULL)
    private String name;

    private Integer age;

    private String email;
    @TableField("manager_id")
    private Long managerId;
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 排除非表字段的三种方式
     * 1、private transient String remark; 无法序列化
     * 2、private static String remark; 实例无法独有一份
     * 3、@TableField(exist=false)
     */
    @TableField(exist = false)
    private String remark;

}
