package com.qiudao.mybatisplus.mapper;

import com.qiudao.mybatisplus.entity.People;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * Description: 测试 插入功能
 * @author: gdc
 * @date: 2019/8/18
 * @version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PeopleInsertMapperTest {

    @Resource
    private PeopleMapper peopleMapper;

    /**
     * 插入数据
     */
    @Test
    public void insert(){
        People people = People.builder()
                .id(12345L)
                .name("gdc")
                .age(18)
                .email("gdc@163.com")
                .createTime(LocalDateTime.now())
                .remark("备注")
                .build();
        int result = peopleMapper.insert(people);
        Assert.assertEquals(1, result);
    }

}
