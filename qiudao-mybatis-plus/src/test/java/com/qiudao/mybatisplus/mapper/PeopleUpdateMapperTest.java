package com.qiudao.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.additional.update.impl.LambdaUpdateChainWrapper;
import com.qiudao.mybatisplus.entity.People;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * Description: 测试 更新 操作
 * @author: gdc
 * @date: 2019/8/18
 * @version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PeopleUpdateMapperTest {

    @Resource
    private PeopleMapper peopleMapper;

    @Test
    public void updateById(){
        People people = People.builder().id(12345L).age(19).build();
        int result = peopleMapper.updateById(people);
        Assert.assertEquals(1, result);
    }

    @Test
    public void update(){
        UpdateWrapper<People> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", 12345L);
        People people = People.builder().id(12345L).age(19).build();
        int result = peopleMapper.update(people, updateWrapper);
        Assert.assertEquals(1, result);
    }

    @Test
    public void update2(){
        People wherePeople = People.builder().id(12345L).build();
        UpdateWrapper<People> updateWrapper = new UpdateWrapper<>(wherePeople);

        People people = People.builder().age(21).build();
        int result = peopleMapper.update(people, updateWrapper);
        Assert.assertEquals(1, result);
    }

    @Test
    public void update3(){
        UpdateWrapper<People> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", 12345L)
                .set("age", 22);
        int result = peopleMapper.update(null, updateWrapper);
        Assert.assertEquals(1, result);
    }

    /**
     * 使用 Lambda 包装类进行数据操作
     */
    @Test
    public void updateByWrapperLambda(){
        LambdaUpdateWrapper<People> updateWrapper = new LambdaUpdateWrapper<>();
//        LambdaUpdateWrapper<People> updateWrapper = Wrappers.lambdaUpdate();
        updateWrapper.eq(People::getId, 12345L)
                .set(People::getAge, 23);
        int result = peopleMapper.update(null, updateWrapper);
        Assert.assertEquals(1, result);
    }

    /**
     * Lambda 链式 包装类更新
     */
    @Test
    public void updateByWrapperLambdaChain(){
        LambdaUpdateChainWrapper<People> updateChainWrapper = new LambdaUpdateChainWrapper<>(peopleMapper);
        boolean result = updateChainWrapper.eq(People::getId, 12345L)
                .set(People::getAge, 24).update();
        Assert.assertTrue(result);
    }


}
