package com.qiudao.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.additional.query.impl.LambdaQueryChainWrapper;
import com.qiudao.mybatisplus.entity.People;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description: 测试 查询方法
 * @author: gdc
 * @date: 2019/8/17
 * @version 1.0
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class PeopleSelectMapperTest {

    @Resource
    private PeopleMapper peopleMapper;

    /**
     * 通过ID 查询单条信息
     */
    @Test
    public void selectById(){
        People people = peopleMapper.selectById(1087982257332887553L);
        log.info(people.toString());
    }

    /**
     * 通过ID list 查询符合条件的信息
     */
    @Test
    public void selectBatchIds(){
        List<Long> idList = Arrays.asList(1087982257332887553L, 1088248166370832385L, 1088250446457389058L, 1L);
        List<People> peoples = peopleMapper.selectBatchIds(idList);
        peoples.forEach(System.out::println);
    }

    /**
     * 通过 Map 添加查询条件， key 为 数据库的字段， value 为期望的结果
     */
    @Test
    public void selectByMap(){
        Map<String, Object> columnMap = new HashMap<>();
        columnMap.put("name", "大boss");
        columnMap.put("manager_id",null);
        List<People> peopleList = peopleMapper.selectByMap(columnMap);
        Assert.assertEquals(1, peopleList.size());
        peopleList.forEach(System.out::println);
    }


    /**
     * 通过查询包装类添加条件，进行查询
     */
    @Test
    public void selectMaps1(){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        queryWrapper.likeRight("name", "大").lt("age", 45).select("id","name", "manager_id");

        /*
        selectMaps 使用场景 查询其中几列 ,结果 ： {name=王天风, id=1088248166370832385}  ， 返回的列为NULL 不显示
         */
        List<Map<String, Object>> mapList = peopleMapper.selectMaps(queryWrapper);
        mapList.forEach(System.out::println);
    }

    /*
    SELECT avg(age) avg_age,max(age) max_age,min(age) min_age FROM mp_people GROUP BY manager_id HAVING sum(age) < ?
     */
    @Test
    public void selectMaps2(){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("avg(age) avg_age", "max(age) max_age", "min(age) min_age")
                .groupBy("manager_id")
                .having("sum(age) < {0}", 500);
        List<Map<String, Object>> mapList = peopleMapper.selectMaps(queryWrapper);
        mapList.forEach(System.out::println);
    }

    /*
    注意：是返回第一个字段的值. 如：
        DEBUG==>  Preparing: SELECT id,name FROM people WHERE name LIKE ? AND age < ?
        DEBUG==> Parameters: 王%(String), 40(Integer)
        TRACE<==    Columns: id, name
        TRACE<==        Row: 1088248166370832385, 王天风
        DEBUG<==      Total: 1
        1088248166370832385
    查询两个字段，实际返回一个字段
     */
    @Test
    public void selectObjs(){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        queryWrapper.likeRight("name", "王")
                .lt("age", 40)
                .select("id", "name");
        List<Object> objs = peopleMapper.selectObjs(queryWrapper);
        objs.forEach(System.out::println);
    }

    /**
     * 获取符合条件的 条数， 注意：不能加 select 列
     * SELECT COUNT( 1 ) FROM mp_people WHERE name LIKE ? AND age < ?
     */
    @Test
    public void selectCount(){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        queryWrapper.likeRight("name", "王").lt("age", 40);
        Integer count = peopleMapper.selectCount(queryWrapper);
        log.info("selectCount = {}", count);
    }

    /**
     * selectOne() 查询单条数据，如果返回多条，则报错
     */
    @Test
    public void selectOne(){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        queryWrapper.likeRight("name", "王").lt("age", 40);
        People people = peopleMapper.selectOne(queryWrapper);
        log.info("people => {}", people);
    }

    /**
     *  lambda 包装类查询方式
     * SELECT id,name,age,email,manager_id,create_time FROM mp_people WHERE name LIKE ? AND age < ?
     */
    @Test
    public void selectLambda1(){
        /*
        实例化三种方法， 三选一
         */
        LambdaQueryWrapper<People> queryWrapper = new QueryWrapper<People>().lambda();
//        LambdaQueryWrapper<People> queryWrapper = new LambdaQueryWrapper<>();
//        LambdaQueryWrapper<Object> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(People::getName, "王").lt(People::getAge, 40);
        List<People> peopleList = peopleMapper.selectList(queryWrapper);
        peopleList.forEach(System.out::println);
    }

    /**
     * SELECT id,name,age,email,manager_id,create_time FROM mp_people WHERE name LIKE ? AND ( age < ? OR email IS NOT NULL )
     */
    @Test
    public void selectLambda2(){
        LambdaQueryWrapper<People> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.likeRight(People::getName, "王")
                .and(lqw -> lqw.lt(
                        People::getAge, 40)
                        .or()
                        .isNotNull(People::getEmail)
                );
        List<People> peopleList = peopleMapper.selectList(queryWrapper);
        peopleList.forEach(System.out::println);
    }

    /**
     * SELECT id,name,age,email,manager_id,create_time FROM mp_people WHERE name LIKE ? AND age >= ?
     */
    @Test
    public void selectLambda3(){
        List<People> peopleList = new LambdaQueryChainWrapper<People>(peopleMapper)
                .like(People::getName, "王")
                .ge(People::getAge, 20)
                .list();
        peopleList.forEach(System.out::println);
    }

    /**
     * 自定义查询
     */
    @Test
    public void selectCustomSize(){
        LambdaQueryWrapper<People> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.likeRight(People::getName, "王")
                .and(pqw ->
                        pqw.lt(People::getAge, 40)
                        .isNotNull(People::getEmail)
                );
        List<People> peopleList = peopleMapper.selectCustomSize(queryWrapper);
        peopleList.forEach(System.out::println);
    }

    /**
     * 自定义查询
     * select * from mp_people WHERE name LIKE ? AND ( age < ? )
     */
    @Test
    public void selectCustomSize2(){
        LambdaQueryWrapper<People> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.likeRight(People::getName, "王")
                .and(
                        qw -> qw.lt(People::getAge, 40)
                );
        List<People> peopleList = peopleMapper.selectCustomSize(queryWrapper);
        peopleList.forEach(System.out::println);
    }

}