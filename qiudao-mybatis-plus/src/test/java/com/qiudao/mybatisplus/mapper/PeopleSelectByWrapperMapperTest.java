package com.qiudao.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qiudao.mybatisplus.entity.People;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description: 通过包装类条件，测试 查询 功能
 * @author: gdc
 * @date: 2019/8/18
 * @version 1.0
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class PeopleSelectByWrapperMapperTest {
    @Resource
    private PeopleMapper peopleMapper;

    /**
     * 分页查询
     * SELECT COUNT( 1 ) FROM mp_people WHERE name LIKE ? OR ( age < ? AND age > ? AND email IS NOT NULL )
     * SELECT id,name,age,email,manager_id,create_time FROM mp_people WHERE name LIKE ? OR ( age < ? AND age > ? AND email IS NOT NULL ) LIMIT ?,?
     */
    @Test
    public void selectPage(){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        queryWrapper.likeRight("name", "王").or(wq -> wq.lt("age", 40).gt("age", 20).isNotNull("email"));
        int result = peopleMapper.selectCount(queryWrapper);

        Page<People> page = new Page<>(0, 2);
        IPage<People> peopleIPage = peopleMapper.selectPage(page, queryWrapper);
        log.info("  selectPages -> {}", peopleIPage);
        log.info("  getCurrent  -> {}", peopleIPage.getCurrent());
        log.info("  getPages -> {}", peopleIPage.getPages());
        log.info("  getTotal -> {}", peopleIPage.getTotal());

        List<People> records = peopleIPage.getRecords();
        records.forEach(System.out::println);
    }

    /**
     * 分页查询
     * SELECT id,name,age,email,manager_id,create_time FROM mp_people WHERE ( age < ? OR email IS NOT NULL ) AND name LIKE ? LIMIT ?,?
     */
    @Test
    public void selectMapsPage(){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        queryWrapper.nested(wq -> wq.lt("age", 40).or().isNotNull("email")).likeRight("name", "王");
        IPage<Map<String, Object>> page = new Page<>(1, 2);

        IPage<Map<String, Object>> iPage = peopleMapper.selectMapsPage(page, queryWrapper);
        log.info("  selectMapsPage -> {}", iPage.getRecords());
        log.info("  selectMapsPage -> {}", iPage);
        log.info("  getPages -> {}", iPage.getPages());
        log.info("  getTotal -> {}", iPage.getTotal());
        List<Map<String, Object>> peopleRecords = iPage.getRecords();
        peopleRecords.forEach(System.out::println);
    }

    /**
     * 自定义分页查询，解决多表分页
     */
    @Test
    public void selectMyPage(){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        queryWrapper.likeRight("name", "大");

        Page<People> page = new Page<>(1, 2);
        IPage<People> peopleIPage = peopleMapper.selectPeoplePage(page, queryWrapper);
        log.info(" selectPage - " + peopleIPage);
        log.info(" getPages - " + peopleIPage.getPages());
        log.info(" getTotal - " + peopleIPage.getTotal());
        List<People> peopleList = peopleIPage.getRecords();
        peopleList.forEach(System.out::println);
    }

    @Test
    public void selectMapsPage2(){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        /*
        SELECT id,name,age,email,manager_id,create_time FROM people WHERE age IN (?,?,?,?)
         */
        queryWrapper.in("age", Arrays.asList(30, 31, 34, 18));
        IPage<Map<String, Object>> page = new Page<>(1, 2);
        IPage<Map<String, Object>> list = peopleMapper.selectMapsPage(page, queryWrapper);
        log.info(" selectMapsPage - " + list.getRecords());
    }

    @Test
    public void selectList2(){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        /*
        SELECT id,name,age,email,manager_id,create_time FROM people WHERE age IN (?,?,?,?) limit 1
         */
        queryWrapper.in("age", Arrays.asList(30,31,34,18)).last("limit 1");
        List<People> peopleList = peopleMapper.selectList(queryWrapper);
        peopleList.forEach(System.out::println);
    }

    @Test
    public void selectList3(){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        /*
        SELECT id,name,age,email FROM people WHERE age IN (?,?,?,?) limit 1
         */
        queryWrapper.select("id", "name", "age")
                .in("age", Arrays.asList(30, 31, 34, 18))
                .last("limit 1");
        List<People> peopleList = peopleMapper.selectList(queryWrapper);
        peopleList.forEach(System.out::print);
    }

    /**
     * 查询，排除  多余的列
     */
    @Test
    public void selectList4(){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        queryWrapper.select(People.class, info -> !info.getColumn().equals("create_time") && !info.getColumn().equals("manager_id"))
                .in("age", Arrays.asList(30, 31, 34, 18))
                .last("limit 1");
        List<People> peopleList = peopleMapper.selectList(queryWrapper);
        peopleList.forEach(System.out::println);
    }

    /**
     *  通过给定的条件，进行不同的条件包装查询
     */
    @Test
    public void testCondition(){
        String name = "大";
        String email = "";
        condition(name, email);
        condition2(name, email);
    }

    private void condition(String name, String email) {
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(name)){
            queryWrapper.like("name", name);
        }
        if (!StringUtils.isEmpty(email)) {
            queryWrapper.like("email", email);
        }

        List<People> peoples = peopleMapper.selectList(queryWrapper);
        peoples.forEach(System.out::println);
    }

    /**
     * 通过 like(Boolean, key, value) ，将结果为 true 的情况添加至查询条件
     * @param name      名字
     * @param email     邮箱
     */
    private void condition2(String name, String email){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(StringUtils.isEmpty(name), "name", name)
                .like(!StringUtils.isEmpty(email), "email", email);
        List<People> peoples = peopleMapper.selectList(queryWrapper);
        peoples.forEach(System.out::println);
    }

    /*
    allEq(Map)  会将 Map 中所有的数据，进行条件查询
    SELECT id,name,age,email,manager_id,create_time FROM people WHERE name = ? AND age IS NULL
     */
    @Test
    public void selectByWrapperAllEq(){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", "大Boss");
        map.put("age", null);
        queryWrapper.allEq(map);

        List<People> peoples = peopleMapper.selectList(queryWrapper);
        peoples.forEach(System.out::println);
    }

    /**
     * allEq(map, boolean)  若 boolean = false, 则查询的map条件中, 结果非 false 不会添加到实际 的 查询条件中
     * SELECT id,name,age,email,manager_id,create_time FROM mp_people WHERE name = ?
     */
    @Test
    public void selectByWrapperAllEq2(){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        Map<String, Object> map = new HashMap<>();
        map.put("name", "大Boss");
        map.put("age", null);
        queryWrapper.allEq(map, false);
        List<People> peoples = peopleMapper.selectList(queryWrapper);
        peoples.forEach(System.out::println);
    }

    /**
     * allEq(BiPredicate filter, map)  通过filter 过滤条件，将不想查询的条件去掉
     * SELECT id,name,age,email,manager_id,create_time FROM mp_people WHERE age IS NULL
     */
    @Test
    public void selectByWrapperAllEq3(){
        QueryWrapper<People> queryWrapper = new QueryWrapper<>();
        Map<String, Object> map = new HashMap<>();
        map.put("name", "大Boss");
        map.put("age", null);
        queryWrapper.allEq((k, v) -> !k.equals("name"), map);

        List<People> peoples = peopleMapper.selectList(queryWrapper);
        peoples.forEach(System.out::println);
    }



}
