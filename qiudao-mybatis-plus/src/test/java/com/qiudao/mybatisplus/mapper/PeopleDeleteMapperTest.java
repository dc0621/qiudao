package com.qiudao.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qiudao.mybatisplus.entity.People;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Description: 测试 删除功能
 * @author: gdc
 * @date: 2019/8/18
 * @version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PeopleDeleteMapperTest {

    @Resource
    private PeopleMapper peopleMapper;

    /**
     * 插入需要测试删除的数据
     */
    @Test
    public void insert(){
        for (int i = 0; i < 5; i++) {
            People people = People.builder().id((long) (1000 + i)).name("test_" + i).build();
            peopleMapper.insert(people);
        }
    }

    /**
     * 通过ID 删除 数据
     */
   @Test
   public void deleteById(){
       int result = peopleMapper.deleteById(1000L);
       Assert.assertEquals(1, result);
   }

    /**
     * 通过 Map 条件删除条件
     */
   @Test
    public void deleteByMap(){
       Map<String, Object> map = new HashMap<>();
       map.put("id", 1001L);
       int result = peopleMapper.deleteByMap(map);
       Assert.assertEquals(1, result);
   }

    /**
     * 通过批量ID 删除数据
     */
   @Test
    public void deleteBatchIds(){
       int result = peopleMapper.deleteBatchIds(Arrays.asList(1002L, 1003L));
       Assert.assertEquals(2, result);
   }

    /**
     * 通过Lambda 包装类，查询出对应条件，将其数据删除
     */
   @Test
    public void deleteByWrapper(){
       LambdaQueryWrapper<People> queryWrapper = new LambdaQueryWrapper<>();
//       LambdaQueryWrapper<People> queryWrapper = Wrappers.lambdaQuery();
       queryWrapper.eq(People::getId, 1004L);
       int result = peopleMapper.delete(queryWrapper);
       Assert.assertEquals(1, result);
   }
}
