package com.qiudao.axis.util;

import com.qiudao.axis.enums.HttpEnum;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.encoding.XMLType;

import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Description: webService请求工具
 * 可参考地址： https://blog.csdn.net/u013934408/article/details/85262465
 * 			  https://www.cnblogs.com/kenlyy/p/5019175.html
 * @author: gdc
 * @date: 2019\6\26 0026
 * @version 1.0
 */
public class AxisUtil {

	private static Service service = new Service();

	/**
	 * reportResult = WebUtil.sendWebservice(HttpEnum.URL.getUrl(), HttpEnum.SOAPACTION.getUrl(), HttpEnum.UP_METER_RECORDS.getUrl(), "p_MeterRecords", new Object[]{meterRecordStr}, XMLType.XSD_INTEGER);
	 */
	public static BigInteger sendWebservice(String url, String soapaction, String operationName, String param, Object[] params, QName returnType) {
		// 域名，这是在server定义的
		//Service service = new Service();
		BigInteger ret = new BigInteger("0");
		try {
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress(url);
			// 设置要调用哪个方法
			call.setOperationName(new QName(soapaction, operationName));
			// 设置要传递的参数
			call.addParameter(new QName(soapaction, param),  XMLType.XSD_STRING,  ParameterMode.IN);
			// 输出类型
			call.setReturnType(returnType);
			call.setUseSOAPAction(true);
			call.setSOAPActionURI(soapaction + operationName);
			// 调用方法并传递参数
			ret = (BigInteger) call.invoke(params);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public static BigDecimal sendWebserviceToGetOverdue(String url, String soapaction, String operationName, String param, Object[] params, QName returnType) {
		// 域名，这是在server定义的
		//Service service = new Service();
		BigDecimal ret = null;
		try {
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress(url);
			// 设置要调用哪个方法
			call.setOperationName(new QName(soapaction, operationName));
			// 设置要传递的参数
			call.addParameter(new QName(soapaction, param),  XMLType.XSD_STRING,  ParameterMode.IN);

			// 标准的类型
			call.setReturnType(returnType);
			call.setUseSOAPAction(true);
			call.setSOAPActionURI(soapaction + operationName);
			// 调用方法并传递参数
			ret = (BigDecimal) call.invoke(params);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public static void main(String[] args){
		// 获取欠费金额
		BigDecimal ownAmount = AxisUtil.sendWebserviceToGetOverdue(HttpEnum.URL.getUrl(), HttpEnum.SOAPACTION.getUrl(), HttpEnum.GET_OVERDUE_AMOUNT.getUrl(), "p_MeterID", new Object[]{"281508018836"}, javax.xml.rpc.encoding.XMLType.XSD_DECIMAL);
		System.out.println("表号 281508018836 的欠费金额为：  " +  ownAmount );
	}
}