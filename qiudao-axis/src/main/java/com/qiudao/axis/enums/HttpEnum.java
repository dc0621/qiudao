package com.qiudao.axis.enums;

import lombok.Getter;

/**
 * Description: http信息设置
 * @author: gdc
 * @date: 2019/6/10
 * @version 1.0
 */
@Getter
public enum HttpEnum {

    /**
     * webservice 地址
      */
    URL("http://220.170.148.35:7007/Meter.asmx","外部正式URL"),
    SOAPACTION("http://gasmis.com/", "域名"),
    UP_METER_RECORDS("upMeterRecords","上传抄表记录"),
    GET_OVERDUE_AMOUNT("getOverdueByMeterID","获取欠费金额"),
    ;

    /**
     * url地址
     */
    private String url;
    /**
     * 信息描述
     */
    private String detail;

    HttpEnum(String url, String detail) {
        this.url = url;
        this.detail = detail;
    }
}