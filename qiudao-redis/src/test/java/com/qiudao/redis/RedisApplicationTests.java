package com.qiudao.redis;

import com.qiudao.redis.util.RedisUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RedisApplicationTests {

    @Test
    void contextLoads() {

        boolean test = RedisUtil.set("test", "123");
        System.out.println(test);

        System.out.println(RedisUtil.get("test"));

        boolean flag = RedisUtil.set("name", 666, 1000);
        System.out.println(flag);
        System.out.println(RedisUtil.get("name"));
    }

}
