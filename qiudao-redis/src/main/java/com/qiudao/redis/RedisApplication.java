package com.qiudao.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * Description: Redis 应用启动类
 *
 * - @EnableCaching 开启注解
 *
 * @author: gdc
 * @date: 2021/6/3
 * @version 1.0
 */
@EnableCaching
@SpringBootApplication
public class RedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisApplication.class, args);
    }

}
