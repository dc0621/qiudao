package com.qiudao.redis.bo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;


@Data
@Accessors(chain = true)
public class Dog {

    private Long id;

    private String name;

    private Integer age;

    private Boolean sex;

    @JSONField(format = "yyyy-MM-dd")
    private LocalDate birthday;
}
