package com.qiudao.redis.controller;

import com.alibaba.fastjson.JSON;
import com.qiudao.redis.bo.Dog;
import com.qiudao.redis.util.RedisUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

/**
 * Description: Redis Controller
 * @author: gdc
 * @date: 2021/6/4
 * @version 1.0
 */
@RestController
public class RedisController {

    final String REDIS_PREFIX = "dog:";

    @GetMapping("/set/{name}/{value}")
    public String setVal(@PathVariable(value = "name") String name, @PathVariable(value = "value") String value){
        boolean setFlag = RedisUtil.set(name, value);
        System.out.println("存放的结果为：" + setFlag);
        String result = RedisUtil.get(name, String.class);
        System.out.println("获取缓存值为：" + result);
        return result;
    }

    @GetMapping("/get/{id}")
    public Dog get(@PathVariable(value = "id") Long id){
        Dog dog = new Dog().setId(id).setName("小狗" + 1).setAge(4).setSex(true).setBirthday(LocalDate.of(2020, 1, 1));
        boolean setFlag = RedisUtil.set(REDIS_PREFIX + id, JSON.toJSONString(dog));
        System.out.println("保存结果：" + setFlag);

        String dogJson = RedisUtil.getString(REDIS_PREFIX + id);
        System.out.println("获取结果：" + dogJson);
        return JSON.parseObject(dogJson, Dog.class);
    }

}
