package com.qiudao.rabbitmq.config;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * Description: 扇形交换机，消费者
 * @author: gdc
 * @date: 2019/11/23
 * @version 1.0
 */
@Component
public class FanoutReceiver {
    /**
     * 处理  队列 fanout.A  的数据
     */
    @RabbitListener(queues = "fanout.A")
    public void processA(Map info, Channel channel, Message message) {
        try {
            channel.basicQos(0, 1, false);
            System.out.println("FanoutReceiverA消费者收到消息  : " + info.toString());
            // 消息确认接收
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            try {
                // 消息拒绝接收
                channel.basicReject(message.getMessageProperties().getDeliveryTag(), true);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

    }

    /**
     * 处理  队列 fanout.B  的数据
     */
    @RabbitListener(queues = "fanout.B")
    public void processB(Map info, Channel channel, Message message) {
        try {
            channel.basicQos(0, 1, false);
            System.out.println("FanoutReceiverB消费者收到消息  : " + info.toString());
            // 消息确认接收
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            try {
                // 消息拒绝接收
                channel.basicReject(message.getMessageProperties().getDeliveryTag(), true);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

    }

    /**
     * 处理  队列 fanout.C  的数据
     */
    @RabbitListener(queues = "fanout.C")
    public void processC(Map info, Channel channel, Message message) {
        try {
            channel.basicQos(0, 1, false);
            System.out.println("FanoutReceiverC消费者收到消息  : " +  info.toString());
            // 消息确认接收
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            try {
                // 消息拒绝接收
                channel.basicReject(message.getMessageProperties().getDeliveryTag(), true);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}

