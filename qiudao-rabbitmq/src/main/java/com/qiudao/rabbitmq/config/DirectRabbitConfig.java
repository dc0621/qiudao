package com.qiudao.rabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Description: 直连型交换机 RabbitMq 配置关系
 * @author: gdc
 * @date: 2019/11/23
 * @version 1.0
 */
@Configuration
public class DirectRabbitConfig {

    /**
     *  Direct交换机 起名：testDirectExchange
     */
    @Bean
    DirectExchange testDirectExchange() {
        return new DirectExchange("testDirectExchange");
    }

    /**
     * 队列 起名：testDirectQueue
     */
    @Bean
    public Queue testDirectQueue() {
        //true 是否持久
        return new Queue("testDirectQueue", true);
    }

    /**
     * 绑定  将队列和交换机绑定, 并设置用于匹配键：testDirectRouting
     */
    @Bean
    Binding bindingDirect() {
        return BindingBuilder.bind(testDirectQueue()).to(testDirectExchange()).with("testDirectRouting");
    }
}
