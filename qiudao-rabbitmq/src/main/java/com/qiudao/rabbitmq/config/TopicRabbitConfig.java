package com.qiudao.rabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Description: Topic 主题交换机 Rabbit Config
 * @author: gdc
 * @date: 2019/11/23
 * @version 1.0
 */
@Configuration
public class TopicRabbitConfig {
    /**
     * 队列
     */
    public final static String MAN = "topic.man";
    public final static String WOMAN = "topic.woman";
    /**
     * 主题交换机
     */
    public final static String TOPIC_EXCHANGE = "topic.common.exchange";
    /**
     * 绑定关系
     *      *  (星号) 用来表示一个单词 (必须出现的)
     *      #  (井号) 用来表示任意数量（零个或多个）单词
     *  队列Q1 绑定键为 *.TT.*    队列Q2绑定键为  TT.#
     *      如果一条消息携带的路由键为 A.TT.B，那么队列Q1将会收到；
     *      如果一条消息携带的路由键为TT.AA.BB，那么队列Q2将会收到；
     *  当一个队列的绑定键为 "#"（井号） 的时候，这个队列将会无视消息的路由键，接收所有的消息。
     *  当 * (星号) 和 # (井号) 这两个特殊字符都未在绑定键中出现的时候，此时主题交换机就拥有的直连交换机的行为。
     *  所以主题交换机也就实现了扇形交换机的功能，和直连交换机的功能。
     */
    public final static String KEY_MAN = "key.man";
    public final static String KEY_COMMON = "key.#";

    /**
     * man 队列
     */
    @Bean
    public Queue manQueue() {
        return new Queue(MAN);
    }

    /**
     * woman 队列
     */
    @Bean
    public Queue womanQueue() {
        return new Queue(WOMAN);
    }

    /**
     * 公共交换机
     */
    @Bean
    public TopicExchange exchange(){
        return new TopicExchange(TOPIC_EXCHANGE);
    }

    /**
     * 绑定关系_man
     * 将manQueue和exchange绑定,而且绑定的键值为topic.man
     * 这样只要是消息携带的路由键是key.man,才会分发到该队列

     */
    @Bean
    public Binding bindingExchangeMan() {
        return BindingBuilder.bind(manQueue()).to(exchange()).with(KEY_MAN);
    }

    /**
     * 绑定关系_woman
     * 将womanQueue和exchange绑定,而且绑定的键值为用上通配路由键规则topic.#
     * 这样只要是消息携带的路由键是以key.开头,都会分发到该队列
     */
    @Bean
    public Binding bindingExchangeWoman() {
        return BindingBuilder.bind(womanQueue()).to(exchange()).with(KEY_COMMON);
    }

}
