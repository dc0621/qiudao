package com.qiudao.rabbitmq.config;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * Description: 直连型交换机 ，RabbitMq 消费者接收消息
 * RabbitListener  监听队列为 testDirectQueue
 * @author: gdc
 * @date: 2019/11/23
 * @version 1.0
 */
@Component
public class DirectReceiver {
 
    @RabbitHandler
    @RabbitListener(queues = "testDirectQueue")
    public void process(Map info, Channel channel, Message message) {
        try {
            channel.basicQos(0, 1, false);
            System.out.println("DirectReceiver消费者收到消息  : " + info.toString());
            // 消息确认接收
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            try {
                // 消息拒绝接收
                channel.basicReject(message.getMessageProperties().getDeliveryTag(), true);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

    }
 
}
