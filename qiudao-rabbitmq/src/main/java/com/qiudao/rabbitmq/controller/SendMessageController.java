package com.qiudao.rabbitmq.controller;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Description: RabbitMq 生产者消息发送
 * @author: gdc
 * @date: 2019/11/23
 * @version 1.0
 */
@RestController
public class SendMessageController {

    @Resource
    RabbitTemplate rabbitTemplate;

    /**
     * 发送 直连型交换机 信息数据
     */
    @GetMapping("/sendDirectMessage")
    public String sendDirectMessage() {
        Map<String, Object> map = new HashMap<>();
        map.put("messageId", String.valueOf(UUID.randomUUID()));
        map.put("messageData", "test message, hello!");
        map.put("createTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        //将消息携带绑定键值：TestDirectRouting 发送到交换机TestDirectExchange
        rabbitTemplate.convertAndSend("testDirectExchange", "testDirectRouting", map);
        return "ok";
    }

    /**
     * 发送 主题型交换机 信息数据
     * 由于 woman 队列，绑定的key 为 key.#,因此当前发送的消息会有两个队列接收
     */
    @GetMapping("/sendTopicMessage/man")
    public String sendTopicMessage1() {
        Map<String, Object> manMap = new HashMap<>();
        manMap.put("messageId", String.valueOf(UUID.randomUUID()));
        manMap.put("messageData", "message: M A N ");
        manMap.put("createTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        rabbitTemplate.convertAndSend("topic.common.exchange", "key.man", manMap);
        return "ok";
    }

    /**
     * 发送 主题型交换机 信息数据
     */
    @GetMapping("/sendTopicMessage/woman")
    public String sendTopicMessage2() {
        Map<String, Object> womanMap = new HashMap<>();
        womanMap.put("messageId", String.valueOf(UUID.randomUUID()));
        womanMap.put("messageData", "message: woman is all ");
        womanMap.put("createTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        rabbitTemplate.convertAndSend("topic.common.exchange", "key.woman", womanMap);
        return "ok";
    }

    /**
     * 测试扇形交换机
     * 扇型交换机，这个交换机没有路由键概念，就算你绑了路由键也是无视的。
     * 这个交换机在接收到消息后，会直接转发到绑定到它上面的所有队列。
     */
    @GetMapping("/sendFanoutMessage")
    public String sendFanoutMessage() {
        Map<String, Object> map = new HashMap<>();
        map.put("messageId", String.valueOf(UUID.randomUUID()));
        map.put("messageData", "message: testFanoutMessage ");
        map.put("createTime", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        rabbitTemplate.convertAndSend("fanoutExchange", null, map);
        return "ok";
    }


}