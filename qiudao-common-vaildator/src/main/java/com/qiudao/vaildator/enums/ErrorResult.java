package com.qiudao.vaildator.enums;

import lombok.Getter;

@Getter
public enum ErrorResult {
    CHECK_NAME("NAME_ERROR_CODE","name需与方法签名及其实现的参数名一致"),
    CHECK_VALUE("VALUE_ERROR_CODE","请传入验证参数"),
    CHECK_RULE("RULE_ERROR_CODE","未实现此验证规则"),
    PHONE("PHONE_ERROR_CODE","电话号码格式不正确"),
    MOBILE("MOBILE_ERROR_CODE","手机号码格式不正确"),
    EMAIL("EMAIL_ERROR_CODE","邮箱格式不正确"),
    DOMAIN("DOMAIN_ERROR_CODE","域名格式不正确"),
    URL("URL_ERROR_CODE","网址格式不正确"),
    DATE("DATE_ERROR_CODE","时间格式不正确"),
    MAX("MAX_ERROR_CODE","必须小于{1}的数字"),
    MAX_LENGTH("MAX_LENGTH_ERROR_CODE","字符串长度必须小于{1}个字符"),
    MIX("MIX_ERROR_CODE","必须大于{1}的数字"),
    MIN_LENGTH("MIX_LENGTH_ERROR_CODE","字符串长度必须大于{1}个字符"),
    NOT_BLANK("NOT_BLANK_ERROR_CODE","不能为NULL且不能为空"),
    NOT_EMPTY("NOT_EMPTY_ERROR_CODE","不能为空"),
    NOT_NULL("NOT_NULL_ERROR_CODE","不能为NULL"),
    RANGE("RANGE_ERROR_CODE","必须大于等于{1}且小于等于{2}"),
    SIZE("SIZE_ERROR_CODE","长度必须大于等于{1}且小于等于{2}"),
    ENUM("ENUM_ERROR_CODE","只能为({})中的一个值"),
    NOT_EID("EID_ERROR_CODE","实体ID不正确");

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误信息
     */
    private String message;

    ErrorResult(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
