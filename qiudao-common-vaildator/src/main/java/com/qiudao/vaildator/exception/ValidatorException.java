package com.qiudao.vaildator.exception;

import lombok.Data;

/**
 * Description: 自定义验证异常
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
@Data
public class ValidatorException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * 错误编码
     */
    private String code;

    /**
     * 默认异常构造器.
     */
    public ValidatorException() {
        super();
    }

    /**
     * 根据异常信息构造对象.
     * @param message   异常信息.
     */
    public ValidatorException(String message) {
        super(message);
    }

    /**
     * 根据异常信息构造对象.
     * @param code      异常编码.
     * @param message   异常信息.
     */
    public ValidatorException(String code, String message) {
        super(message);
        this.code = code;
    }

    /**
     * 根据异常信息构造对象.
     * @param cause     原生异常.
     */
    public ValidatorException(Throwable cause) {
        super(cause);
    }

    /**
     * 根据异常信息构造对象.
     * @param message   异常信息.
     * @param cause     原生异常.
     */
    public ValidatorException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 根据异常信息构造对象.
     * @param code      异常编码.
     * @param message   异常信息.
     * @param cause
     */
    public ValidatorException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    /**
     * 当抛出此异常时，就会通过这个方法填充当前线程的栈帧信息。
     * 如果，我们重新了此方法（native实现是将线程的栈帧信息记录到此 Throwable 对象中），不记录当前线程的栈帧信息，
     * 那么，当此异常被抛出时，就无法保存堆栈信息了
     * @return
     */
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
