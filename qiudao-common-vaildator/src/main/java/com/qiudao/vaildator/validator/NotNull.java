package com.qiudao.vaildator.validator;

import com.qiudao.vaildator.enums.ErrorResult;
import com.qiudao.vaildator.validator.abstracts.ValidatorAdapter;

/**
 * Description:     对象不能为空校验
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class NotNull extends ValidatorAdapter {

    /**
     * 判断是否为null，null返回false,验证失败
     *
     * @param object
     * @return
     */
    @Override
    public boolean validate(Object object) {
        setErrorCode(ErrorResult.NOT_NULL.getCode());
        setMessage(ErrorResult.NOT_NULL.getMessage());
        return object != null;
    }

    @Override
    public byte getValidateType() {
        Byte type = VALIDATE_TYPE.get(NotNull.class);
        return type == null ? TYPE_DEFAULT : type;
    }
}