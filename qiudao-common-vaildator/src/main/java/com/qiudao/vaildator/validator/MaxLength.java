package com.qiudao.vaildator.validator;

import com.qiudao.vaildator.enums.ErrorResult;
import com.qiudao.vaildator.validator.abstracts.ValidatorAdapter;
import net.sf.oval.constraint.MaxLengthCheck;

/**
 * Description: 最大长度验证适配
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class MaxLength extends ValidatorAdapter {


    /**
     * 判断字符串是否大于指定数量
     *
     * @param object
     * @param v
     * @return
     */
    @Override
    public boolean validate(Object object, double[] v) {
        MaxLengthCheck check = new MaxLengthCheck();
        check.setMax((int) v[0]);
        setErrorCode(ErrorResult.MAX_LENGTH.getCode());
        setMessage(ErrorResult.MAX_LENGTH.getMessage().replace("{1}", v[0] + ""));
        return check.isSatisfied(null, object, null, null);
    }

    @Override
    public byte getValidateType() {
        Byte type = VALIDATE_TYPE.get(MaxLength.class);
        return type == null ? TYPE_DEFAULT : type;
    }
}

