package com.qiudao.vaildator.validator;

import com.qiudao.vaildator.enums.ErrorResult;
import com.qiudao.vaildator.validator.abstracts.ValidatorAdapter;
import net.sf.oval.constraint.MinCheck;

/**
 * Description:     最小值验证适配
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class Min extends ValidatorAdapter {
    /**
     * 判断是否小于指定数字
     *
     * @param object
     * @param value
     * @return
     */
    @Override
    public boolean validate(Object object, double[] value) {
        setErrorCode(ErrorResult.MIX.getCode());
        setMessage(ErrorResult.MIX.getMessage().replace("{1}", value[0] + ""));
        MinCheck check = new MinCheck();
        check.setMin(value[0]);
        return check.isSatisfied(null, object, null, null);
    }

    @Override
    public byte getValidateType() {
        Byte type = VALIDATE_TYPE.get(Min.class);
        return type == null ? TYPE_DEFAULT : type;
    }
}
