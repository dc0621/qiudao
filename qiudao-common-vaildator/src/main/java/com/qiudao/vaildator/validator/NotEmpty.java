package com.qiudao.vaildator.validator;

import com.qiudao.vaildator.enums.ErrorResult;
import com.qiudao.vaildator.validator.abstracts.ValidatorAdapter;
import net.sf.oval.constraint.NotEmptyCheck;

/**
 * Description:     对象不能为空（不包括字符串）校验
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class NotEmpty extends ValidatorAdapter {
    /**
     * 只判断是否为空，NULL 返回true,验证通过
     *
     * @param object
     * @return
     */
    @Override
    public boolean validate(Object object) {
        setErrorCode(ErrorResult.NOT_EMPTY.getCode());
        setMessage(ErrorResult.NOT_EMPTY.getMessage());
        return new NotEmptyCheck().isSatisfied(null, object, null, null);
    }

    @Override
    public byte getValidateType() {
        Byte type = VALIDATE_TYPE.get(NotEmpty.class);
        return type == null ? TYPE_DEFAULT : type;
    }
}
