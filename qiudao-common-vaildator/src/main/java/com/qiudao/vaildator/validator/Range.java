package com.qiudao.vaildator.validator;

import com.qiudao.vaildator.enums.ErrorResult;
import com.qiudao.vaildator.validator.abstracts.ValidatorAdapter;
import net.sf.oval.constraint.RangeCheck;

/**
 * Description:     范围验证适配
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class Range extends ValidatorAdapter {

    /**
     * 判断数据是否在 v1 与 v2区间内，包含v1 或v2
     *
     * @param object
     * @param v
     * @return
     */
    @Override
    public boolean validate(Object object, double[] v) {
        setErrorCode(ErrorResult.RANGE.getCode());
        setMessage(ErrorResult.RANGE.getMessage().replace("{1}", v[0] + "").replace("{2}", v[1] + ""));
        RangeCheck check = new RangeCheck();
        check.setMin(v[0]);
        check.setMax(v[1]);
        return check.isSatisfied(null, object, null, null);
    }

    @Override
    public byte getValidateType() {
        Byte type = VALIDATE_TYPE.get(Range.class);
        return type == null ? TYPE_DEFAULT : type;
    }
}
