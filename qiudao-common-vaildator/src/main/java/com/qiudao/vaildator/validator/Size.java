package com.qiudao.vaildator.validator;

import com.qiudao.vaildator.enums.ErrorResult;
import com.qiudao.vaildator.validator.abstracts.ValidatorAdapter;
import net.sf.oval.constraint.SizeCheck;

/**
 * Description:     大小校验适配
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class Size extends ValidatorAdapter {

    /**
     * 判断指定字符串是否在v1 与 v2范围内，包含v1 与 v2
     *
     * @param object
     * @param v
     * @return
     */
    @Override
    public boolean validate(Object object, double[] v) {
        setErrorCode(ErrorResult.SIZE.getCode());
        setMessage(ErrorResult.SIZE.getMessage().replace("{1}", v[0] + "").replace("{2}", v[1] + ""));
        SizeCheck check = new SizeCheck();
        check.setMin((int) v[0]);
        check.setMax((int) v[1]);
        return check.isSatisfied(null, object, null, null);
    }

    @Override
    public byte getValidateType() {
        Byte type = VALIDATE_TYPE.get(Size.class);
        return type == null ? TYPE_DEFAULT : type;
    }
}
