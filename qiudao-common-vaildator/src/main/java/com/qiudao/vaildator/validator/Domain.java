package com.qiudao.vaildator.validator;


import com.qiudao.vaildator.enums.ErrorResult;
import com.qiudao.vaildator.regex.Regex;
import com.qiudao.vaildator.util.RegexUtil;
import com.qiudao.vaildator.validator.abstracts.ValidatorAdapter;

/**
 * Description: 域名验证适配
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class Domain extends ValidatorAdapter {

    /**
     * 验证是否域名
     *
     * @param object
     * @return
     */
    @Override
    public boolean validate(Object object) {
        if (object == null) {
            return true;
        }
        String mobile = object.toString();
        if (RegexUtil.match(mobile, Regex.DOMAIN)) {
            return true;
        }
        setErrorCode(ErrorResult.DOMAIN.getCode());
        setMessage(ErrorResult.DOMAIN.getMessage());
        return false;
    }

    @Override
    public byte getValidateType() {
        Byte type = VALIDATE_TYPE.get(Domain.class);
        return type == null ? TYPE_DEFAULT : type;
    }
}

