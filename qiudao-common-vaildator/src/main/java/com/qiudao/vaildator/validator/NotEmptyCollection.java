package com.qiudao.vaildator.validator;

import com.qiudao.vaildator.enums.ErrorResult;
import com.qiudao.vaildator.validator.abstracts.ValidatorAdapter;

import java.util.Collection;

/**
 * Description: 集合是否为空校验
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class NotEmptyCollection extends ValidatorAdapter {
    /**
     * 判断是否为null，null返回false,验证失败
     *
     * @param object
     * @return
     */
    @Override
    public boolean validate(Object object) {
        setErrorCode(ErrorResult.NOT_EMPTY.getCode());
        setMessage(ErrorResult.NOT_EMPTY.getMessage());
        return object != null && object instanceof Collection && !((Collection) object).isEmpty();
    }

    @Override
    public byte getValidateType() {
        Byte type = VALIDATE_TYPE.get(NotEmptyCollection.class);
        return type == null ? TYPE_DEFAULT : type;
    }
}
