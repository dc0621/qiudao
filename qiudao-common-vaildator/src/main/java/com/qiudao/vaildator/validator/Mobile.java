package com.qiudao.vaildator.validator;

import com.qiudao.vaildator.enums.ErrorResult;
import com.qiudao.vaildator.regex.Regex;
import com.qiudao.vaildator.util.RegexUtil;
import com.qiudao.vaildator.validator.abstracts.ValidatorAdapter;

/**
 * Description:     手机号验证适配
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class Mobile extends ValidatorAdapter {

    /**
     * 验证是否手机号码
     *
     * @param object
     * @return
     */
    @Override
    public boolean validate(Object object) {
        if (object == null) {
            return true;
        }
        String mobile = object.toString();
        if (RegexUtil.match(mobile, Regex.MOBILE)) {
            return true;
        }
        setErrorCode(ErrorResult.MOBILE.getCode());
        setMessage(ErrorResult.MOBILE.getMessage());
        return false;
    }

    @Override
    public byte getValidateType() {
        Byte type = VALIDATE_TYPE.get(Mobile.class);
        return type == null ? TYPE_DEFAULT : type;
    }
}
