package com.qiudao.vaildator.validator;

import com.qiudao.vaildator.enums.ErrorResult;
import com.qiudao.vaildator.validator.abstracts.ValidatorAdapter;
import net.sf.oval.constraint.EmailCheck;

/**
 * Description: 邮箱验证适配
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class Email extends ValidatorAdapter {

    /**
     * 验证邮箱格式正确性
     *
     * @param object
     * @return
     */
    @Override
    public boolean validate(Object object) {
        setErrorCode(ErrorResult.EMAIL.getCode());
        setMessage(ErrorResult.EMAIL.getMessage());
        return new EmailCheck().isSatisfied(null, object, null, null);
    }

    @Override
    public byte getValidateType() {
        Byte type = VALIDATE_TYPE.get(Email.class);
        return type == null ? TYPE_DEFAULT : type;
    }
}

