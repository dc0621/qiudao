package com.qiudao.vaildator.validator.abstracts;

import lombok.Data;

import java.util.HashMap;

@Data
public abstract class ValidatorAdapter {
    /**
     * 验证信息
     */
    private String message = "default validator error message!";

    /**
     * 错误码
     */
    private String errorCode = "DEFAULT_VALIDATOR";

    protected static final HashMap<Class<? extends ValidatorAdapter>, Byte> VALIDATE_TYPE = new HashMap<>();

    protected static final byte TYPE_DEFAULT = (byte) 0;
    public static final byte TYPE_OBJECT = (byte) 1;
    public static final byte TYPE_VALUE = (byte) 2;
    public static final byte TYPE_ENUM = (byte) 3;


    /**
     * 验证对象是否满足规则
     * 规则由子类继承实现
     * @param o     验证对象
     * @return      结果
     */
    public boolean validate(final Object o) {
        return true;
    }

    /**
     * 验证对象o 和 数据v 是否满足条件
     * 规则由子类继承实现
     * @param o     验证对象
     * @param v     验证数据
     * @return      结果
     */
    public boolean validate(final Object o, final double[] v) {
        return true;
    }

    /**
     * 验证对象o和验证类c是否满足验证条件
     * @param o     验证对象
     * @param c     验证类
     * @return      true or false
     */
    public boolean validate(final Object o, final Class c) {
        return true;
    }

    /**
     * 获取验证类型
     * @return      验证类型
     */
    public abstract byte getValidateType();

}
