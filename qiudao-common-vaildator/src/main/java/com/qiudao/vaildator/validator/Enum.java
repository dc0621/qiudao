package com.qiudao.vaildator.validator;

import com.qiudao.vaildator.enums.ErrorResult;
import com.qiudao.vaildator.validator.abstracts.ValidatorAdapter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Description: Enum验证适配
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class Enum extends ValidatorAdapter {

    private static final String SPLIT = ".";

    @Override
    public boolean validate(final Object object, final double[] values) {
        setErrorCode(ErrorResult.ENUM.getCode());
        if (null == object) {
            setMessage("不能为空");
            return false;
        } else {
            StringBuilder sb = new StringBuilder("");
            try {
                for (double v : values) {
                    sb.append(doubleTrans(v)).append(", ");
                    final double doubleValue = Double.parseDouble(object.toString());
                    if (v == doubleValue) {
                        return true;
                    }
                }
            } catch (final NumberFormatException e) {
                setMessage("参数转换失败，枚举验证目前仅支持验证数字类型");
                return false;
            }
            sb.setLength(sb.length() - 2);
            setMessage(ErrorResult.ENUM.getMessage().replace("{}", sb.toString()));
            return false;
        }
    }

    @Override
    public boolean validate(final Object object, final Class c) {
        setErrorCode(ErrorResult.ENUM.getCode());
        if (null == object || c == null) {
            setMessage("不能为空");
            return false;
        } else {
            try {
                Object[] objs = c.getEnumConstants();
                Method getId = c.getMethod("getId");

                if (null == objs || objs.length == 0) {
                    setMessage("枚举中没有定义属性");
                    return false;
                } else {
                    StringBuilder sb = new StringBuilder("");
                    for (Object obj : objs) {
                        String v = getId == null ? obj.toString() : getId.invoke(obj).toString();
                        sb.append(v).append(", ");
                        if (object.toString().equals(v)) {
                            return true;
                        }
                    }
                    sb.setLength(sb.length() - 2);
                    setMessage(ErrorResult.ENUM.getMessage().replace("{}", sb.toString()));
                    return false;
                }
            } catch (NoSuchMethodException e) {
                setMessage("需要匹配的枚举属性请使用id并实现getId方法");
                return false;
            } catch (IllegalAccessException | InvocationTargetException e) {
                setMessage("系统异常，验证失败");
                return false;
            }
        }
    }

    private String doubleTrans(double num) {
        String s = String.valueOf(num);
        if (s.indexOf(SPLIT) > 0) {
            //去掉多余的0
            s = s.replaceAll("0+?$", "");
            //如最后一位是.则去掉
            s = s.replaceAll("[.]$", "");
        }
        return s;
    }

    @Override
    public byte getValidateType() {
        Byte type = VALIDATE_TYPE.get(Enum.class);
        return type == null ? TYPE_DEFAULT : type;
    }
}
