package com.qiudao.vaildator.validator;

import com.qiudao.vaildator.enums.ErrorResult;
import com.qiudao.vaildator.validator.abstracts.ValidatorAdapter;
import net.sf.oval.constraint.MaxCheck;

/**
 * Description: 最大值验证适配
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class Max extends ValidatorAdapter {

    /**
     * 判断值是否大于value
     *
     * @param object
     * @param value
     * @return
     */
    @Override
    public boolean validate(Object object, double[] value) {
        MaxCheck check = new MaxCheck();
        check.setMax(value[0]);
        setErrorCode(ErrorResult.MAX.getCode());
        setMessage(ErrorResult.MAX.getMessage().replace("{1}", value[0] + ""));
        return check.isSatisfied(null, object, null, null);
    }

    @Override
    public byte getValidateType() {
        Byte type = VALIDATE_TYPE.get(Max.class);
        return type == null ? TYPE_DEFAULT : type;
    }
}
