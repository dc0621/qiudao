package com.qiudao.vaildator.validator;


import com.qiudao.vaildator.enums.ErrorResult;
import com.qiudao.vaildator.validator.abstracts.ValidatorAdapter;

/**
 * Description:     不能为空，包括字符串验证适配
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class NotBlank extends ValidatorAdapter {
    /**
     * 判断是否为空或Null 返回false,验证失败
     *
     * @param object
     * @return
     */
    @Override
    public boolean validate(Object object) {
        setErrorCode(ErrorResult.NOT_BLANK.getCode());
        setMessage(ErrorResult.NOT_BLANK.getMessage());
        if (object == null) {
            return false;
        }
        return !isBlank(object.toString());
    }

    private boolean isBlank(String str) {
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if ((!Character.isWhitespace(str.charAt(i)))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public byte getValidateType() {
        Byte type = VALIDATE_TYPE.get(NotBlank.class);
        return type == null ? TYPE_DEFAULT : type;
    }
}

