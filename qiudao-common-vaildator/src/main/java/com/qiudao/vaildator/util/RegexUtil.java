package com.qiudao.vaildator.util;

import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Description: 正则匹配工具类
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
public class RegexUtil {

    /**
     * 通过 字符串和正则规则匹配
     * @param str       需要匹配的数据
     * @param regex     正则表达式规则
     * @return          返回匹配结果
     */
    public static boolean match(String str, String regex){
        if (StringUtils.isEmpty(str) || StringUtils.isEmpty(regex)) {
            return false;
        }
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }

}
