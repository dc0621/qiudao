package com.qiudao.vaildator.annotation;


import com.qiudao.vaildator.validator.abstracts.ValidatorAdapter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description: 参数校验规则明细
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface Check {

    /**
     * 规则适配器，使用者可以自己写规则验证，继承 Validator 并 实现相应接口即可
     * @return
     */
    Class<? extends ValidatorAdapter> adapter() default ValidatorAdapter.class;

    /**
     * 比较参数，目前只能设置 double 类型参数，用于校验数据大小或字符串长度，验证长度时，会把比较double数据转换为int 类型
     * @return
     */
    double[] v() default {};


    Class<? extends Enum> c() default Enum.class;

    /**
     * 需要验证的参数名称，不设置按 check 所在的索引匹配参数
     * @return
     */
    String name() default "";

    /**
     * 错误编码
     * @return
     */
    String errorCode() default "0";

    /**
     * 错误内容
     * @return
     */
    String message() default "";

    /**
     * I18N 国际化编码
     * @return
     */
    String i18nCode() default "";


}
