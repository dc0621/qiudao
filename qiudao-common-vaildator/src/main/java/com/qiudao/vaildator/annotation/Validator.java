package com.qiudao.vaildator.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description: 接口注解， 用于 ValidatorAspect 对使用此注解的接口进行拦截
 * @author: gdc
 * @date: 2019/8/31
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Validator {

    Check[] value() default {};
}
