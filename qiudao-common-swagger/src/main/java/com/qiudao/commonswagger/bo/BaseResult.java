package com.qiudao.commonswagger.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Description: 公共的返回结果
 * @author: gdc
 * @date: 2019/9/14
 * @version 1.0
 *
 * \@ApiModel()         用于响应类上，表示一个返回响应数据的信息
 * value                表示对象名
 * description          描述
 */
@ApiModel(value = "公共返回结果", description = "所有的接口公共返回值")
@Data
public class BaseResult {

    /**
     * \@ApiModelProperty()用于方法，字段； 表示对model属性的说明或者数据操作更改
     * value            字段说明
     * name             重写属性名字
     * dataType         重写属性类型
     * required         是否必填
     * example          举例说明
     * hidden           隐藏
     */
    @ApiModelProperty(value = "状态码", required = true)
    private String code;
    @ApiModelProperty(value = "状态信息")
    private String msg;
}
