package com.qiudao.commonutil.enums;

/**
 * Description:
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public enum CompanyTypeEnum {

    // 0、总公司    1、公司总部    2、营业厅
    GOLDCARD((short) 0, "总公司"),
    BRAND((short) 1, "公司总部"),
    BUSINESS_HALL((short) 2, "营业厅");

    private short code;
    private String remark;

    private CompanyTypeEnum(short code, String remark) {
        this.code = code;
        this.remark = remark;
    }

    public short getCode() {
        return code;
    }

    public String getRemark() {
        return remark;
    }

    public static CompanyTypeEnum codeOf(short code) {
        for (CompanyTypeEnum type : CompanyTypeEnum.values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return null;
    }

    public boolean isBrand() {
        return this.equals(CompanyTypeEnum.BRAND);
    }

    public static boolean isBrand(int companyType) {
        return CompanyTypeEnum.BRAND.equals(codeOf((short) companyType));
    }
}
