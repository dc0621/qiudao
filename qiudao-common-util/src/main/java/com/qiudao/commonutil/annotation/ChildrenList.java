package com.qiudao.commonutil.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Description:  
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)//在运行时可以获取
@Target({ElementType.FIELD})//作用到类的域上面
public @interface ChildrenList {
}

