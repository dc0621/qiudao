package com.qiudao.commonutil.base;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description:  
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class DataResult {

    /**
     * 用于存储错误描述的关键词
     */
    public static final String ERROR_DESC = "errorDesc";

    private boolean isSuccess;

    private Map<String, Object> data = new HashMap<String, Object>();

    public DataResult() {

    }

    public DataResult(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public Object get(String key) {
        if (StringUtils.isBlank(key)) {
            return null;
        }

        return this.data.get(key);
    }

    public void set(String key, Object val) {
        this.data.put(key, val);
    }

    public Map<String, Object> getData() {
        return this.data;
    }

    @SuppressWarnings("unchecked")
    public List<String> getErrors() {
        List<String> errors = (List<String>) this.get("errors");
        if (errors == null) {
            errors = new ArrayList<String>();
            this.set("errors", errors);
        }
        return errors;
    }

    public String getDefaultErrorMessage() {
        return (String) this.get(ERROR_DESC);
    }

    public void setDefaultErrorMessage(String message) {
        this.set(ERROR_DESC, message);
    }

    public boolean isSuccess() {
        return this.isSuccess;
    }

    public boolean isFail() {
        return !this.isSuccess;
    }

    public void setSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public String toJsonString() {
        return com.alibaba.fastjson.JSON.toJSONString(this);
    }
}
