package com.qiudao.commonutil.base;

import com.qiudao.commonutil.util.MapUtil;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Description:
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class QueryInfo implements Serializable {

    private static final long serialVersionUID = -419037466726245205L;

    private int pageNo = 0;

    private int pageSize = 15;

    private String sortColumn = null;

    private String sortDir = null;

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getSortColumn() {
        return sortColumn;
    }

    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    public String getSortDir() {
        return sortDir;
    }

    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }

    public int getStartIndex() {
        int first = (this.pageNo - 1) * pageSize;
        return first < 0 ? 0 : first;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public Map<String, Object> toParams() {
        Map<String, Object> params = new HashMap<>();
        MapUtil.populateMap(params, this);
        return params;
    }

}

