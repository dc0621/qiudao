package com.qiudao.commonutil.base;

import java.util.Map;

/**
 * Description:
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public interface IOrderBy {
    public static final String ASC = "asc";
    public static final String DESC = "desc";

    /**
     * 返回排序字段及方向
     *
     * @return
     */
    public Map<String, Object> toParam();
}
