package com.qiudao.commonutil.base;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Description:
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class Pagination implements Serializable {

    private static final long serialVersionUID = -1535934177602889578L;

    public static final String RECORD_START = "paginationStart";

    /**
     * 每页显示记录数
     */
    private int pageSize = 20;

    /**
     * 当前记录下标，从1开始
     */
    private int pageNo = 0;

    /**
     * 返回结果总数
     */
    private int totalRows = 0;

    /**
     * desc asc
     */
    private String order;
    private String sort;

    public Pagination() {
    }

    public Pagination(int pageNo, int pageSize) {
        this.setPageNo(pageNo);
        this.setPageSize(pageSize);
    }

    // --------------- 返回的属性 ---------------------

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
        int pageNo = getPageNo();
        if (pageNo > (this.totalRows / getPageSize() + 1)) {
            setPageNo(this.totalRows / getPageSize() + 1);
        }
    }

    // *********** 计算属性 ***********

    /**
     * mysql 分页起始位置，从0开始
     */
    public int getPaginationStart() {
        if (this.pageNo <= 0) {
            pageNo = 1;
        }
        return pageSize * (pageNo - 1);
    }

    /**
     * 获取本次查询总的页数
     */
    public int getTotalPages() {
        return (totalRows + pageSize - 1) / pageSize;
    }

    public Map<String, Object> getQueryCondition() {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("pageSize", this.getPageSize());
        params.put("paginationStart", this.getPaginationStart());
        return params;
    }

    // 在使用缩略数字分页情况下使用
    private int pagelen = 7;

    public int getPagelen() {
        return pagelen;
    }

    public void setPagelen(int pagelen) {
        this.pagelen = pagelen;
    }

    /**
     * 取得默认大小(<code>DEFAULT_SLIDER_SIZE</code>)的页码滑动窗口，并将当前页尽可能地放在滑动窗口的中间部位。参见{@link #getSlider(int n)}。
     *
     * @return 包含页码的数组
     */
    public int[] getSlider() {
        return getSlider(pagelen);
    }

    /**
     * 取得指定大小的页码滑动窗口，并将当前页尽可能地放在滑动窗口的中间部位。例如: 总共有13页，当前页是第5页，取得一个大小为5的滑动窗口，将包括 3，4，5，6, 7这几个页码，第5页被放在中间。如果当前页是12，则返回页码为
     * 9，10，11，12，13。
     *
     * @param width 滑动窗口大小
     * @return 包含页码的数组，如果指定滑动窗口大小小于1或总页数为0，则返回空数组。
     */
    public int[] getSlider(int width) {
        int pages = getTotalPages();

        if ((pages < 1) || (width < 1)) {
            return new int[0];
        } else {
            if (width > pages) {
                width = pages;
            }

            int[] slider = new int[width];
            int first = pageNo - ((width - 1) / 2);

            if (first < 1) {
                first = 1;
            }

            if (((first + width) - 1) > pages) {
                first = pages - width + 1;
            }

            for (int i = 0; i < width; i++) {
                slider[i] = first + i;
            }

            return slider;
        }
    }

    /***
     * 获取缩略数字情况下左右偏移数量
     *
     * @return
     */
    public int getPageOffset() {
        int tmp = this.getPagelen() % 2 > 0 ? getPagelen() + 1 : getPagelen();
        return tmp / 2;
    }

    public int getOffsetStart() {
        int min = 1;
        if (this.getTotalPages() > this.getPagelen()) {
            if (this.getPageNo() <= this.getPageOffset()) {
                min = 1;
            } else {
                if ((this.getPageNo() + this.getPageOffset()) >= (this.getTotalPages() + 1)) {
                    min = this.getTotalPages() - this.getPagelen() + 1;
                } else {
                    min = this.getPageNo() - this.getPageOffset() + 1;
                }
            }
        }
        return min;
    }

    public int getOffsetEnd() {
        int max = this.getTotalPages();
        if (this.getTotalPages() > this.getPagelen()) {
            if (this.getPageNo() <= this.getPageOffset()) {
                max = this.getPagelen();
            } else {
                if ((this.getPageNo() + this.getPageOffset()) >= (this.getTotalPages() + 1)) {
                    max = this.getTotalPages();
                } else {
                    max = this.getPageNo() + this.getPageOffset() - 1;
                }
            }
        }
        return max;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public boolean hasNext() {
        return this.getTotalPages() > this.pageNo;
    }
}

