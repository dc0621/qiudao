package com.qiudao.commonutil.base;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description:  基础查询对象
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class QueryResult<T> implements Serializable {

    /**
     * 查询结果
     */
    private List<T> resultList;

    /**
     * 分页控制信息
     */
    private Pagination pagination = new Pagination();

    /**
     * 其他扩展数据
     */
    private Map<String, Object> datamap = new HashMap<>();

    public List<T> getResultList() {
        return resultList;
    }

    public void setResultList(List<T> resultList) {
        this.resultList = resultList;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public Map<String, Object> getDatamap() {
        return datamap;
    }

    public void setDatamap(Map<String, Object> datamap) {
        this.datamap = datamap;
    }

    public void putData(String key, Object data) {
        if (this.getDatamap() == null) {
            this.setDatamap(new HashMap<String, Object>());
        }
        this.getDatamap().put(key, data);
    }

    public Object getData(String key) {
        if (StringUtils.isBlank(key)) {
            return null;
        }
        if (this.getDatamap() != null) {
            return this.getDatamap().get(key);
        }
        return null;
    }

    /**
     * 配置分页信息，如果当前页码超出实际总页数，当前页会被设置为最后一页
     *
     * @param pageNo
     * @param pageSize
     * @param totalCount
     */
    public void configPagination(int pageNo, int pageSize, int totalCount) {
        Pagination pagination = new Pagination();
        pagination.setPageNo(pageNo);
        pagination.setPageSize(pageSize);
        pagination.setTotalRows(totalCount);

        if (pageNo > pagination.getTotalPages()) {
            pagination.setPageNo(pagination.getTotalPages());
        }

        this.pagination = pagination;
    }
}

