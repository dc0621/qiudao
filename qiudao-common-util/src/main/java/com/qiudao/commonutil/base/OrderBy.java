package com.qiudao.commonutil.base;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * Description:
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class OrderBy implements IOrderBy {

    private String column;   // 排序字段
    private String direction; // 排序方向

    public OrderBy(String column, String direction) {
        this.column = column;
        this.direction = direction;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public Map<String, Object> toParam() {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(this.getColumn()) && StringUtils.isNotBlank(this.getDirection())) {
            params.put("sortColumn", this.getColumn());
            params.put("sortDir", this.getDirection());
        }
        return params;
    }

    public static void main(String[] args) {
        OrderBy sort = new OrderBy("id", "asc");
        System.out.println(ToStringBuilder.reflectionToString(sort));
    }
}

