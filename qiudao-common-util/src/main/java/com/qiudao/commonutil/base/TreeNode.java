package com.qiudao.commonutil.base;

import lombok.Getter;
import lombok.Setter;

/**
 * Description:  
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
@Setter
@Getter
public class TreeNode {
    //排序ID
    private String id;

    //父类ID
    private String parentId;

    private Object object;
}
