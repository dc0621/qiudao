package com.qiudao.commonutil.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import java.util.List;
import java.util.Map;

/**
 * Description: JSON转换处理工具类(fastjson实现)
 * 依赖com.alibaba.fastjson
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class FastJsonUtils {
    /**
     * 将Json字符串转换成Java对象
     *
     * @param json 准备转换的Json字符串
     * @param clz  准备转换的类
     * @return 转换之后的Java对象
     */
    public static <T> T jsonToBean(String json, Class<T> clz) {
        return JSON.parseObject(json, clz);
    }

    /**
     * 将Json字符串转换成Java对象
     *
     * @param json 准备转换的Json字符串
     * @param tr   准备转换的类,支持list泛型
     * @return 转换之后的Java对象
     */
    public static <T> T jsonToBean(String json, TypeReference<T> tr) {
        return JSON.parseObject(json, tr);
    }

    /**
     * 将Json字符串转换成Java对象列表
     *
     * @param json 准备转换的Json字符串
     * @param clz  准备转换的类
     * @return 转换之后的Java对象列表
     */
    public static <T> List<T> jsonToList(String json, Class<T> clz) {
        return JSON.parseArray(json, clz);
    }


    /**
     * 将Java对象转换成Json字符串
     *
     * @param obj 准备转换的对象
     * @return 转换之后的Json字符串
     */
    public static String beanToJson(Object obj) {
        return JSON.toJSONString(obj);
    }


    /**
     * 将Json字符串转换成Map对象
     *
     * @param json 准备转换的Json字符串
     * @return 转换之后的Map对象
     */
    public static Map<String, Object> jsonToMap(String json) {
        return JSON.parseObject(json, new TypeReference<Map<String, Object>>() {});
    }

}
