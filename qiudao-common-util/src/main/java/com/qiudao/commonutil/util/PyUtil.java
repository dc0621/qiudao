package com.qiudao.commonutil.util;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;

/**
 * Description: 拼音转换工具类
 * 依赖 com.belerweb.pinyin4j
 *      slf4j
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class PyUtil {

    private static final Logger logger = LoggerFactory.getLogger(PyUtil.class);

    /**
     * 汉字转拼音缩写
     *
     * @param str //要转换的汉字字符串
     * @return String  //拼音缩写
     */
    public static String getPYString(String str) {
        StringBuffer firstSpell = new StringBuffer();
        char[] charOfCN = str.trim().toCharArray();
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        format.setCaseType(HanyuPinyinCaseType.UPPERCASE);
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        for (int i = 0; i < charOfCN.length; i++) {
            // 是否为中文字符
            if (charOfCN[i] > 128) {
                String[] spellArray;
                try {
                    spellArray = PinyinHelper.toHanyuPinyinStringArray(
                            charOfCN[i], format);
                    // 取拼音首字母
                    if (null != spellArray) {
                        firstSpell.append(spellArray[0].charAt(0));
                    }
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    logger.error(e.getMessage(), e);
                }
            } else if ((charOfCN[i] >= '0' && charOfCN[i] <= '9')
                    || (charOfCN[i] >= 'a' && charOfCN[i] <= 'z')
                    || (charOfCN[i] >= 'A' && charOfCN[i] <= 'Z')) {
                firstSpell.append(charOfCN[i]);
            }
        }
        return firstSpell.toString();
    }

    /**
     * 取单个字符的拼音声母
     *
     * @param c //要转换的单个汉字
     * @return String 拼音声母
     */
    private static String getPYChar(String c) {
        byte[] array = null;
        try {
            array = String.valueOf(c).getBytes(Charset.forName("GBK"));
        } catch (Exception e) {
        }
        if (array == null || array.length <= 1) {
            return "";
        }
        int i = c.charAt(0);
        if (i < 0xB0A1) {
            return "";
        }
        if (i < 0xB0C5) {
            return "A";
        }
        if (i < 0xB2C1) {
            return "B";
        }
        if (i < 0xB4EE) {
            return "C";
        }
        if (i < 0xB6EA) {
            return "D";
        }
        if (i < 0xB7A2) {
            return "E";
        }
        if (i < 0xB8C1) {
            return "F";
        }
        if (i < 0xB9FE) {
            return "G";
        }
        if (i < 0xBBF7) {
            return "H";
        }
        if (i < 0xBFA6) {
            return "J";
        }
        if (i < 0xC0AC) {
            return "K";
        }
        if (i < 0xC2E8) {
            return "L";
        }
        if (i < 0xC4C3) {
            return "M";
        }
        if (i < 0xC5B6) {
            return "N";
        }
        if (i < 0xC5BE) {
            return "O";
        }
        if (i < 0xC6DA) {
            return "P";
        }
        if (i < 0xC8BB) {
            return "Q";
        }
        if (i < 0xC8F6) {
            return "R";
        }
        if (i < 0xCBFA) {
            return "S";
        }
        if (i < 0xCDDA) {
            return "T";
        }
        if (i < 0xCEF4) {
            return "W";
        }
        if (i < 0xD1B9) {
            return "X";
        }
        if (i < 0xD4D1) {
            return "Y";
        }
        if (i < 0xD7FA) {
            return "Z";
        }
        return "*";
    }
}
