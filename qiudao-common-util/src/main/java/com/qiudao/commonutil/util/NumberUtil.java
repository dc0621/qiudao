package com.qiudao.commonutil.util;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;

/**
 * Description: 数字工具类
 * 依赖 org.apache.commons.lang3.StringUtils
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class NumberUtil {

    private static final String SEP = ",";

    public static int parseInt(String str, int defaultValue) {
        if (StringUtils.isEmpty(str)) {
            return defaultValue;
        }
        int value;
        try {
            value = Integer.parseInt(str.trim());
        } catch (Exception e) {
            value = defaultValue;
        }
        return value;
    }

    public static Integer parseInt(String str) {
        if (StringUtils.isEmpty(str)) {
            return null;
        }

        Integer value;
        try {
            value = Integer.parseInt(str.trim());
        } catch (Exception e) {
            value = null;
        }
        return value;
    }

    public static float parseFloat(String str, float defaultValue) {
        if (StringUtils.isEmpty(str)) {
            return defaultValue;
        }
        float value;
        try {
            value = Float.parseFloat(str.trim());
        } catch (Exception e) {
            value = defaultValue;
        }
        return value;
    }

    public static long parseLong(String str, long defaultValue) {
        if (StringUtils.isEmpty(str)) {
            return defaultValue;
        }
        long value;
        try {
            value = Long.parseLong(str.trim());
        } catch (Exception e) {
            value = defaultValue;
        }
        return value;
    }

    // 非Double,返回null
    public static Double parseDouble(String str) {
        if (StringUtils.isEmpty(str)) {
            return null;
        }

        Double value;
        try {
            value = Double.parseDouble(str.trim());
        } catch (Exception e) {
            value = null;
        }
        return value;
    }

    public static double parseDouble(String str, double defaultValue) {
        if (StringUtils.isEmpty(str)) {
            return defaultValue;
        }
        double value;
        try {
            value = Double.parseDouble(str.trim());
        } catch (Exception e) {
            value = defaultValue;
        }
        return value;
    }

    public static long defaultIfNull(Long num, long defaultVal) {
        if (num == null) {
            return defaultVal;
        }
        return num.longValue();
    }

    public static int defaultIfNull(Integer num, int defaultVal) {
        if (num == null) {
            return defaultVal;
        }
        return num.intValue();
    }

    public static double defaultIfNull(Double num, double defaultVal) {
        if (num == null) {
            return defaultVal;
        }
        return num.doubleValue();
    }

    public static boolean greaterThan(String str1, String str2) {
        if (!isDouble(str1) || !isDouble(str2)) {
            return false;
        }

        double num1 = NumberUtil.parseDouble(str1, 0D);
        double num2 = NumberUtil.parseDouble(str2, 0D);
        return num1 > num2;
    }

    public static boolean greaterEqualThan(String str1, String str2) {
        if (!isDouble(str1) || !isDouble(str2)) {
            return false;
        }

        double num1 = NumberUtil.parseDouble(str1, 0D);
        double num2 = NumberUtil.parseDouble(str2, 0D);
        return num1 > num2;
    }

    public static boolean equalThan(String str1, String str2) {
        if (!isDouble(str1) || !isDouble(str2)) {
            return false;
        }

        double num1 = NumberUtil.parseDouble(str1, 0D);
        double num2 = NumberUtil.parseDouble(str2, 0D);
        return num1 == num2;
    }

    private static boolean isDouble(String str) {
        double d = parseDouble(str, -1);
        return d >= 0;
    }

    public static List<Long> parseLongs(String str) {
        return parseLongs(str, SEP);
    }

    public static List<Long> parseLongs(String str, String sep) {
        if (StringUtils.isBlank(str)) {
            return null;
        }

        List<Long> ids = new ArrayList<Long>();
        String[] arr = StringUtils.split(str, sep);
        for (String item : arr) {
            if (!StringUtils.isNumeric(item)) {
                continue;
            }

            Long val = NumberUtil.parseLong(StringUtils.trim(item), -1);
            ids.add(val);
        }
        return ids;
    }

    public static Set<Long> parseLongIdsSet(String str) {
        return parseLongIdsSet(str, SEP);
    }

    public static Set<Long> parseLongIdsSet(String str, String sep) {
        List<Long> ids = parseLongs(str, sep);
        if (ids != null) {
            Set<Long> set = new HashSet<Long>();
            for (Long id : ids) {
                set.add(id);
            }
            return set;
        }
        return null;
    }

    public static Long add(Long val1, Long val2) {
        Long tmpval1 = val1 != null ? val1 : 0;
        Long tmpval2 = val2 != null ? val2 : 0;
        return tmpval1.longValue() + tmpval2.longValue();
    }

    public static Integer add(Integer val1, Integer val2) {
        Integer tmpval1 = val1 != null ? val1 : 0;
        Integer tmpval2 = val2 != null ? val2 : 0;
        return tmpval1.intValue() + tmpval2.intValue();
    }

    public static Double add(Double val1, Double val2) {
        Double tmpval1 = val1 != null ? val1 : 0D;
        Double tmpval2 = val2 != null ? val2 : 0D;
        return tmpval1.doubleValue() + tmpval2.doubleValue();
    }

    public static String numberFormat(double f, int fractionDigits) {
        NumberFormat nf1 = NumberFormat.getInstance(Locale.CHINA);
        nf1.setMaximumFractionDigits(fractionDigits);
        return nf1.format(f);
    }

    public static String numberFormat(float n, int digs) {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(digs);
        return nf.format(n);
    }

    public static String numberFormat(double f) {
        NumberFormat nf1 = NumberFormat.getInstance(Locale.CHINA);
        return nf1.format(f);
    }

    public static String percentFormat(double d) throws ParseException {
        DecimalFormat nf = (DecimalFormat) NumberFormat.getPercentInstance();
        nf.applyPattern("##.##%");
        return nf.format(d);
    }

    public int divide100(long num) {
        return (int) (num / 100);
    }

    public String formateThree(int num) {
        DecimalFormat df = new DecimalFormat("###,###");
        return df.format(num);
    }

    public static String getPercent(int x, int total, boolean type) {
        String result = "0%";// 接受百分比的值
        double tempresult = x * 1.0 / total;
        DecimalFormat df1 = new DecimalFormat("0%"); // ##.00% 百分比格式，后面不足2位的用0补齐
        result = df1.format(tempresult);
        if (type) {
            result = result.replace("%", "");
        }
        return result;
    }

    /**
     * 是否小数点后六位中的最后4位不为0
     *
     * @param bigDecimal
     * @return false 不为0，存在小数位
     */
    public static Boolean isLastZero(BigDecimal bigDecimal) {
        BigDecimal temp = bigDecimal.multiply(new BigDecimal("100"));
        if (1 == temp.remainder(new BigDecimal("1")).compareTo(new BigDecimal("0"))) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param length 验证码长度
     * @return
     */
    public static String getRandomCode(int length) {
        int max = 1;
        for (int i = 0; i < length; i++) {
            max *= 10;
        }
        max = max - 1;
        Random random = new Random();
        int code = random.nextInt(max);

        String codeStr = String.valueOf(code);
        StringBuilder sb = new StringBuilder();
        if (codeStr.length() < length) {
            int nl = codeStr.length();
            for (int i = 0; i < length - nl; i++) {
                codeStr = sb.append("0").append(codeStr).toString();
            }
        }

        return codeStr;
    }

    public static void main(String[] args) {
        int ret = NumberUtil.parseInt("315", -1);
        String str = String.format("%03d", ret);
        System.out.println(str);
    }
}
