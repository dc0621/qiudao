package com.qiudao.commonutil.util;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;

/**
 * Description: Base64解析工具类 
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class Base64Util {

    /**
     * 加密字符串
     *
     * @param str 加密的内容
     * @return 加密后的内容
     */
    public static String encodeString(String str) {
        try {
            BASE64Encoder encoder = new BASE64Encoder();
            String encodedStr = encoder.encodeBuffer(str.getBytes());
            return encodedStr.trim();
        } catch (Exception e) { }
        return null;
    }

    /**
     * 解码字符串
     *
     * @param str 解码的内容
     * @return 解码后的内容
     * @throws IOException
     */
    public static String decodeString(String str) {
        try {
            BASE64Decoder dec = new BASE64Decoder();
            String value = new String(dec.decodeBuffer(str));
            return value;
        } catch (Exception e) { }
        return null;
    }

}
