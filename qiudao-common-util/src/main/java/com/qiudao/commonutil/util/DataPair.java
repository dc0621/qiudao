package com.qiudao.commonutil.util;

import lombok.*;

/**
 * Description:
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
@Data
public class DataPair {
    private Long startDate;
    private Long endDate;

    public DataPair(Long startDate, Long endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

}
