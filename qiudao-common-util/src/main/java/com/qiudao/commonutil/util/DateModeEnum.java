package com.qiudao.commonutil.util;

/**
 * Description: 日期枚举类
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public enum DateModeEnum {
    /**
     * 日期信息
     */
    YESTERDAY(0, "昨天"),
    TODAY(1, "今天"),
    THIS_WEEK(2, "本周"),
    THIS_MONTH(3, "本月"),
    PREV_MONTH(4, "上月"),
    CUSTOM(5, "自定义");

    private int code;
    private String name;

    private DateModeEnum(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static DateModeEnum codeOf(Integer code) {
        if (code == null) {
            return null;
        }
        for (DateModeEnum item : DateModeEnum.values()) {
            if (item.getCode() == code.intValue()) {
                return item;
            }
        }
        return null;
    }
}
