package com.qiudao.commonutil.util;

/**
 * Description: 缓存Util
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class CacheUtils {

    /**
     * 构造cacheKey
     *
     * @param prefix
     * @param clazz
     * @param key
     * @return
     */
    public static String getCacheKey(String prefix, Class<?> clazz, String key) {
        Assert.isNotNull(prefix, "prefix不能为空");
        Assert.isNotNull(key, "key不能为空");
        return prefix + clazz.getSimpleName() + "." + key;
    }

    /**
     * 构造cacheKey
     *
     * @param prefix
     * @param routeId
     * @param key
     * @return
     */
    public static String getCacheKey(String prefix, String routeId, String key) {
        Assert.isNotNull(prefix, "prefix不能为空");
        Assert.isNotNull(key, "keys不能为空");
        return prefix + routeId + "." + key;
    }

}

