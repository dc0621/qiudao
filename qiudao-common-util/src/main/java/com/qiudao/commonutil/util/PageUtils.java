package com.qiudao.commonutil.util;

/**
 * Description: 分页工具类
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class PageUtils {
    /**
     *  默认分页每页的数据条数
     */
    public static final int DEFAULT_PAGE_SIZE = 20;
    /**
     * 默认页码
     */
    public static final int DEFAULT_PAGE_NUM = 0;

    /**
     * 获取分页开始的索引
     *
     * @param showPage 页数
     * @param pageSize 每页的数据量
     * @return 分页开始的索引
     */
    public static int getStartIndex(int showPage, int pageSize) {
        int first = (showPage - 1) * pageSize;
        return first < 0 ? 0 : first;
    }

    /**
     * pageSize为空或小于0时使用默认的每页数据条数
     *
     * @param pageSize 每页数据条数
     * @return 每页数据条数
     */
    public static int getPageSize(Integer pageSize) {
        if (pageSize == null || pageSize <= 0) {
            return DEFAULT_PAGE_SIZE;
        }
        return pageSize;
    }

    /**
     * pageNum为空或小于0时使用默认的初始页
     *
     * @param pageNum   页数
     * @return          返回页数
     */
    public static int getPageNum(Integer pageNum) {
        if (pageNum == null || pageNum <= 0) {
            return DEFAULT_PAGE_NUM;
        }
        return pageNum;
    }

}
