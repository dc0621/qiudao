package com.qiudao.sqlhelper;

import com.qiudao.sqlhelper.data.Table;
import com.qiudao.sqlhelper.generator.ExcelTableConfigReader;
import org.junit.Test;

/**
 * Description:
 *
 * @Author: liuhuan
 * @Date: 2019-03-11  14:28
 * @Version 1.0
 */
/**
 * Description: 操作数据表导出
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class ExcelTableConfigReaderTest {

    @Test
    public void generateCreateSql() throws Exception {
        String fileName = "D:\\库表结构.xlsx";
        Table table = ExcelTableConfigReader.parseTableDefineFromFile(fileName, "表具实时状态信息表");
        System.out.println(table.generateCreateSql());
    }

    @Test
    public void generateIncrCreateSql() throws Exception {
        String fileName = "D:\\库表结构.xlsx";
        Table table = ExcelTableConfigReader.parseTableDefineFromFile(fileName, "表具实时状态信息表");
        System.out.println(table.generateIncrCreateSql());
    }

}
