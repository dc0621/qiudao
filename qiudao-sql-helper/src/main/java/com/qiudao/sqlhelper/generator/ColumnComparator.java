package com.qiudao.sqlhelper.generator;


import com.qiudao.sqlhelper.data.Column;

import java.util.Comparator;


/**
 * Description: 列比较器
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class ColumnComparator implements Comparator<Column> {

    @Override
    public int compare(Column o1, Column o2) {
        return o1.getSortCode() - o2.getSortCode();
    }

}
