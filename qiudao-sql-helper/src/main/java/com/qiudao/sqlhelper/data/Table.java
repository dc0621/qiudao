package com.qiudao.sqlhelper.data;


import com.qiudao.sqlhelper.generator.ColumnComparator;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Description:     Excel 单页对应的表结构  ，表名 + 列信息
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class Table {
    /**
     * 表昵称
     */
    private String tableName;

    /**
     * 物理表名称（数据库表名）
     */
    private String physicalName;

    /**
     * 列集合
     */
    private List<Column> columns = new ArrayList<>();

    public String getPhysicalName() {
        return physicalName;
    }

    public void setPhysicalName(String physicalName) {
        this.physicalName = physicalName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<Column> getColumns() {
        return columns;
    }

    public void addColumn(Column column) {
        if (column != null) {
            this.columns.add(column);
        }
    }

    public List<Column> getSortColumns() {
        if (this.columns != null) {
            Collections.sort(this.columns, new ColumnComparator());
        }
        return columns;
    }

    public List<Column> getIncrColumns() {
        if (this.columns != null) {
            List<Column> incrColumns = new ArrayList<>();
            for (Column column : this.columns) {
                if (column.isNew()) {
                    incrColumns.add(column);
                }
            }

            if (CollectionUtils.isEmpty(incrColumns)) {
                return new ArrayList<>();
            }

            Collections.sort(incrColumns, new ColumnComparator());
            return incrColumns;
        }
        return new ArrayList<>();
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("表名:").append(this.getTableName()).append(" 物理名称:").append(this.getPhysicalName()).append("\n");
        sb.append("======================================================  字段如下  ======================================================").append("\n");
        for (Column column : this.getSortColumns()) {
            sb.append(column.toString()).append("\n");
        }
        sb.append("======================================================================================================================");
        return sb.toString();
    }

    /**
     * 生成 创建Sql
     * @return  SQL
     */
    public String generateCreateSql() {
        StringBuffer sb = new StringBuffer();
        sb.append("CREATE TABLE ").append(this.physicalName).append("  ").append("(").append("\n");
        for (Column column : this.getSortColumns()) {
            sb.append("    ").append(column.generateCreateSql()).append("\n");
        }
        sb.append("PRIMARY KEY (`id`)").append("\n");
        sb.append(") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='").append(this.tableName).append("';");
        return sb.toString();
    }

    /**
     * 生成 新增字段
     * @return
     */
    public String generateIncrCreateSql() {
        StringBuffer sb = new StringBuffer();
        sb.append("ALTER TABLE ").append(this.physicalName);
        for (Column column : this.getIncrColumns()) {
            Column before = getBeforeColumn(column);
            sb.append(column.generateIncrCreateSql(before)).append(",");
        }
        if (sb.charAt(sb.length() - 1) == ',') {
            sb.deleteCharAt(sb.length() - 1);
        }
        sb.append(";");
        return sb.toString();
    }

    private Column getBeforeColumn(Column column) {
        if (column.getSortNum() == 0) {
            return null;
        }
        return this.columns.get(column.getSortNum() - 1);
    }
}
