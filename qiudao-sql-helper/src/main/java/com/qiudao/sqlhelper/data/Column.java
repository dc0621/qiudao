package com.qiudao.sqlhelper.data;

import org.apache.commons.lang3.StringUtils;

/**
 * Description: Excel 中对应的列信息
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class Column {
    /**
     * Excel 中 选择符号
     */
    public static final String SELECT = "〇";
    /**
     * 单引号
     */
    private static final String QUOTE = "'";
    private static final String COLUMN_SEP = "    ";

    //  编号 名称 物理名 类型 是否主键 是否外键 NOT NULL 备注
    /**
     * 系统产生，非Excel输入，对应Excel的先后顺序
     */
    private int sortNum;
    /**
     * 来自Excel的输入，只是记录，没什么用
     */
    private int sortCode;
    /**
     * 字段名称
     */
    private String aliasName;
    /**
     * 字段物理名
     */
    private String columnName;
    /**
     * 字段类型
     */
    private String columType;
    /**
     * 是否主键
     */
    private boolean isPrimary;
    /**
     * 是否外键
     */
    private boolean isForeign;
    /**
     * 是否不能为空
     */
    private boolean isNotNull;
    /**
     * 备注
     */
    private String comment;
    /**
     * 是否新增字段，需要在Excel 中新增一列，用 "〇" 标示
     */
    private boolean isNew;

    public String generateCreateSql() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.columnName).append(" ");
        sb.append(this.columType).append(" ");
        sb.append(this.isNotNull ? "NOT NULL" : "").append(" ");
        sb.append(" COMMENT ").append(QUOTE).append(this.getComment()).append(QUOTE);
        sb.append(",");
        return sb.toString();
    }

    public String generateIncrCreateSql(Column prevColumn) {
        StringBuffer sb = new StringBuffer();
        sb.append(" add column ");
        sb.append(this.columnName).append(" ");
        sb.append(this.columType).append(" ");
        sb.append(this.isNotNull ? "NOT NULL" : "").append(" ");
        sb.append(" COMMENT ").append(QUOTE).append(this.getComment()).append(QUOTE);
        if (prevColumn != null) {
            sb.append(" after ").append(prevColumn.getColumnName()).append(" ");
        }
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("序号:").append(this.getSortNum()).append(COLUMN_SEP);
        sb.append("Excel序号:").append(this.getSortCode()).append(COLUMN_SEP);
        sb.append("名称:").append(this.getAliasName()).append(COLUMN_SEP);
        sb.append("物理名:").append(this.getColumnName()).append(COLUMN_SEP);
        sb.append("类型:").append(this.getColumType()).append(COLUMN_SEP);
        sb.append("主键:").append(selectText(this.isPrimary)).append(COLUMN_SEP);
        sb.append("外键:").append(selectText(this.isForeign)).append(COLUMN_SEP);
        sb.append("NotNull(Y|N):").append(selectText(this.isNotNull)).append(COLUMN_SEP);
        sb.append("备注:").append(this.getComment()).append(COLUMN_SEP);
        sb.append("是否新增:").append(this.isNew()).append(COLUMN_SEP);
        return sb.toString();
    }

    public int getSortCode() {
        return sortCode;
    }

    public void setSortCode(int sortCode) {
        this.sortCode = sortCode;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumType() {
        return columType;
    }

    public void setColumType(String columType) {
        this.columType = columType;
    }

    public String getComment() {
        if (StringUtils.isNotBlank(this.comment)) {
            String tmp = StringUtils.trim(this.comment);
            tmp = StringUtils.replace(tmp, "\r", StringUtils.EMPTY);
            tmp = StringUtils.replace(tmp, "\n", StringUtils.EMPTY);
            return tmp;
        }
        return StringUtils.trim(comment);
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean isPrimary) {
        this.isPrimary = isPrimary;
    }

    public boolean isForeign() {
        return isForeign;
    }

    public void setForeign(boolean isForeign) {
        this.isForeign = isForeign;
    }

    public boolean isNotNull() {
        return isNotNull;
    }

    public void setNotNull(boolean isNotNull) {
        this.isNotNull = isNotNull;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean isNew) {
        this.isNew = isNew;
    }

    public int getSortNum() {
        return sortNum;
    }

    public void setSortNum(int sortNum) {
        this.sortNum = sortNum;
    }

    private String selectText(boolean isSelect) {
        return isSelect ? SELECT : StringUtils.EMPTY;
    }



}
