# 求道_基础版

### 1、Bean 注入
* 注入方式包含三种:
> @Configuration + @Bean    
> @Component  
> @ImportResource(value = "classpath:/spring/person-service.xml")

### 2、SpringBoot + 策略模式整合，优化过多 if else 的问题
* 可参考代码路径： 
> *com.qiudao.basestudy.util.strategy*  
> *com.qiudao.basestudy.service.impl.OrderServiceImpl*
* [参考](https://juejin.im/post/5c551122e51d457fcc5a9790#comment)
* [源代码](https://github.com/ciphermagic/java-learn/tree/master/sandbox/src/main/java/com/cipher/handler_demo)

### 3、配置统一异常处理类
> 可参考代码路径： *com.qiudao.basestudy.exception*

### 4、配置Swagger 生成接口API
* Web 访问API路径：
> http://{IP}:{PORT}/{PATH}/swagger-ui.html
* 可参考代码：  
> 配置类：*com.qiudao.basestudy.config.SwaggerConfig*  
> 应用类1：*com.qiudao.basestudy.controller.SwaggerController*  
> 应用类2：*com.qiudao.basestudy.bo.App*

### 5、H2 内存数据库使用
* 访问地址：  
> http://{IP}:{PORT}/{PATH}/h2-console
* 所需依赖：
> com.h2database.h2
* 可参考代码：  
> 配置文件：*/resources/application-dev.yml*  
> sql文件路径：*/resources/sql*  

### 6、Mybatis generator 代码自动生成
* 所需的依赖:
> mybatis-generator-core  
> lombok  
> springfox-swagger2  
> springfox-swagger-ui
* 组合：
> 上述依赖  
> resources/mybatis-generator 配置文件  
> com.qiudao.basestudy.util.mybatisPlugin 自定义插件  
> GeneratorMapperPOTest >> Mybatis Generator生成类

### 7、HTTP REST Client 使用
> http/rest-client.env.json 多环境切换   
> http/requestDemo.http     请求Demo 
* [参考](https://blog.csdn.net/weixin_33795833/article/details/91911123)

### 8、项目初始化后执行的方法
* CommandLineRunner 
* @Order    
> com.qiudao.basestudy.config.runner  

### 9、添加定时任务
* @EnableScheduling  
* @Scheduled    
> com.qiudao.basestudy.config.SchedulerTask
* [参考](https://www.jianshu.com/p/1defb0f22ed1)

### 10、添加 Spring Boot Actuator:健康检查、审计、统计和监控
* 添加 spring-boot-starter-actuator 依赖  
* yml文件，配置参数   
> http/actuator.http
* [参考](https://www.jianshu.com/p/d5943e303a1f)



### Util 工具类访问
* 路径：
> com.qiudao.basestudy.util
