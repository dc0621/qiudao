
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `app`
-- ----------------------------
DROP TABLE IF EXISTS `app`;
CREATE TABLE `app` (
  `id` bigint(32) NOT NULL COMMENT 'ID',
  `name` varchar(32) NOT NULL COMMENT '应用名称',
  `code` varchar(32) NOT NULL COMMENT '应用编码，平台唯一自生成',
  `password` varchar(32) NOT NULL COMMENT '应用认证密码',
  `app_url` varchar(255) DEFAULT NULL COMMENT '系统地址',
  `is_valid` smallint(2) DEFAULT NULL COMMENT '是否有效 0:无效 1:有效',
  `create_time` datetime DEFAULT NULL COMMENT '记录生成时间',
  `create_user_id` varchar(32) DEFAULT NULL COMMENT '记录生成人ID',
  `last_ver` int(11) DEFAULT NULL COMMENT '版本号',
  `op_time` datetime DEFAULT NULL COMMENT '操作时间',
  `op_user_id` varchar(32) DEFAULT NULL COMMENT '操作人ID',
  `op_user_name` varchar(50) DEFAULT NULL COMMENT '操作人名',
  PRIMARY KEY (`id`)
);
--) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='应用表';
-- 从 Mysql转h2的sql,需要将上一行注释掉

-- ----------------------------
-- Table structure for company
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `id` bigint(32) NOT NULL COMMENT 'ID',
  `code` varchar(32) NOT NULL COMMENT ' 公司编码，平台唯一自生成',
  `name` varchar(32) NOT NULL COMMENT '公司名称',
  `expand_code` varchar(32) DEFAULT NULL COMMENT '公司外部扩展码，用于第三方机构设置使用',
  `is_valid` smallint(2) DEFAULT NULL COMMENT '是否有效 0:无效 1:有效',
  `create_time` datetime DEFAULT NULL COMMENT '记录生成时间',
  `create_user_id` varchar(32) DEFAULT NULL COMMENT '记录生成人ID',
  `last_ver` int(11) DEFAULT NULL COMMENT '版本号',
  `op_time` datetime DEFAULT NULL COMMENT '操作时间',
  `op_user_id` varchar(32) DEFAULT NULL COMMENT '操作人ID',
  `op_user_name` varchar(50) DEFAULT NULL COMMENT '操作人名',
  PRIMARY KEY (`id`)
);
--) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='公司表';

-- ----------------------------
-- 创建客户表
-- ----------------------------
DROP TABLE IF EXISTS `tb_customer_information`;
CREATE TABLE `tb_customer_information`  (
    `id` bigint(32) NOT NULL COMMENT 'ID',
    `customer_name` varchar(100) DEFAULT NULL COMMENT '用户名称',
    `telephone` varchar(15) NOT NULL COMMENT '联系方式',
    `address` varchar(500) DEFAULT NULL COMMENT '居住地址',
    `birthday` datetime(0) DEFAULT NULL COMMENT '生日',
    `wechat_no` varchar(50) DEFAULT NULL COMMENT '微信号',
    `wechat_authorization_code` varchar(200) DEFAULT NULL COMMENT '微信授权码',
    `union_id` varchar(100) DEFAULT '' COMMENT '微信小程序公众号绑定unionId',
    `login_name` varchar(30) DEFAULT NULL COMMENT '登录名',
    `login_pwd` varchar(64) DEFAULT NULL COMMENT '登录密码',
    `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
    `gender` varchar(10) DEFAULT NULL COMMENT '性别',
    `description` varchar(600) DEFAULT NULL COMMENT '备注',
    `push_type` smallint(6) DEFAULT NULL COMMENT '推送方式0:微信 1:短信  2:电话',
    `emergency_telephone` varchar(15) DEFAULT NULL COMMENT '紧急联系人',
    `city` varchar(32) DEFAULT '' COMMENT '用户城市',
    `province` varchar(32) DEFAULT '' COMMENT '用户省份',
    `headimgurl` varchar(500) DEFAULT '' COMMENT '用户头像',
    `user_language` varchar(20) DEFAULT '' COMMENT '用户语言',
    `company_flag` smallint(6) DEFAULT NULL COMMENT '公司标识 0用户，1公司',
    `company_code` varchar(32) DEFAULT '' COMMENT '用户编码',
    `is_valid` smallint(6) DEFAULT NULL COMMENT '是否有效',
    `create_time` datetime(0) DEFAULT NULL COMMENT '记录生成时间',
    `create_user_id` varchar(32) DEFAULT NULL COMMENT '记录创建人编号',
    `last_ver` int(11) DEFAULT NULL COMMENT '版本号',
    `op_user_id` varchar(32) DEFAULT NULL COMMENT '操作人ID',
    `op_user_name` varchar(50) DEFAULT NULL COMMENT '操作人名',
    `op_time` datetime(0) DEFAULT NULL COMMENT '操作时间',
    PRIMARY KEY (`id`)
);
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = '用户信息表';


-- ----------------------------
-- 创建紧急联系人表
-- ----------------------------
DROP TABLE IF EXISTS `tb_emergency_manager`;
CREATE TABLE `tb_emergency_manager`  (
   `id` bigint(32) NOT NULL COMMENT 'ID',
   `name` varchar(100) NOT NULL COMMENT '紧急联系人名称',
   `telephone` varchar(15) NOT NULL COMMENT '联系方式',
   `image_url` varchar(355) DEFAULT NULL COMMENT '头像图片地址',
   `description` varchar(600) DEFAULT NULL COMMENT '备注',
   `is_valid` smallint(6) DEFAULT NULL COMMENT '是否有效',
   `create_time` datetime(0) DEFAULT NULL COMMENT '记录生成时间',
   `create_user_id` varchar(32) DEFAULT NULL COMMENT '记录创建人编号',
   `last_ver` int(11) DEFAULT NULL COMMENT '版本号',
   `op_user_id` varchar(32) DEFAULT NULL COMMENT '操作人ID',
   `op_user_name` varchar(50) DEFAULT NULL COMMENT '操作人名',
   `op_time` datetime(0) DEFAULT NULL COMMENT '操作时间',
   PRIMARY KEY (`id`)
);
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = '紧急联系人信息表';

-- ----------------------------
-- 创建客户、用户绑定表
-- ----------------------------
DROP TABLE IF EXISTS `tb_customer_user_binding_info`;
CREATE TABLE `tb_customer_user_binding_info`  (
    `id` bigint(32) NOT NULL COMMENT 'ID',
    `customer_id` bigint(32) NOT NULL COMMENT '用户编号',
    `sharding_user_id` bigint(32) NOT NULL COMMENT '被分享人或紧急联系人编号',
    `sharding_user_type` smallint(6) NOT NULL COMMENT '用户类型 1:分享人 2:紧急联系人',
    `is_valid` smallint(6) DEFAULT NULL COMMENT '是否有效 0:无效 1:有效',
    `create_time` datetime(0) DEFAULT NULL COMMENT '记录生成时间',
    `create_user_id` varchar(32) DEFAULT NULL COMMENT '记录创建人编号',
    `last_ver` int(11) DEFAULT NULL COMMENT '版本号',
    `op_user_id` varchar(32) DEFAULT NULL COMMENT '操作人ID',
    `op_user_name` varchar(50) DEFAULT NULL COMMENT '操作人名',
    `op_time` datetime(0) DEFAULT NULL COMMENT '操作时间',
    PRIMARY KEY (`id`)
);
--) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT = '用户被分享人或紧急联系人绑定表';


