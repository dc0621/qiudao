#! /bin/sh

export JAVA_HOME=/usr/java/jdk1.8.0_191
export PATH=$JAVA_HOME/bin:$PATH

# 此语句必填，否则 jenkins 执行脚本后，项目不启动
#BUILD_ID=dontkillme

# 项目名称
APPLICATION="qiudao-base-study"

# 项目启动jar包名称
APPLICATION_JAR="${APPLICATION}.jar"

# 先关闭服务
#echo stop ${APPLICATION} Application...
#sh shutdown.sh

# 安装路径
INSTALLDIR=/home/gdc/qiudao-base-study

mv ${INSTALLDIR}/boot/qiudao-base-study* ${INSTALLDIR}/boot/${APPLICATION_JAR}

# 外部配置文件绝对目录,如果是目录需要/结尾，也可以直接指定文件
# 如果指定的是目录,spring则会读取目录中的所有配置文件
CONFIG_DIR=${INSTALLDIR}/resources/

# 复制打包的项目文件到安装的目录，并启动项目
chmod -R 755  ${INSTALLDIR}/boot/${APPLICATION_JAR}

#==========================================================================================
# JVM Configuration
# -Xmx256m:设置JVM最大可用内存为256m,根据项目实际情况而定，建议最小和最大设置成一样。
# -Xms256m:设置JVM初始内存。此值可以设置与-Xmx相同,以避免每次垃圾回收完成后JVM重新分配内存
# -Xmn512m:设置年轻代大小为512m。整个JVM内存大小=年轻代大小 + 年老代大小 + 持久代大小。
#          持久代一般固定大小为64m,所以增大年轻代,将会减小年老代大小。此值对系统性能影响较大,Sun官方推荐配置为整个堆的3/8
# -XX:MetaspaceSize=64m:存储class的内存大小,该值越大触发Metaspace GC的时机就越晚
# -XX:MaxMetaspaceSize=320m:限制Metaspace增长的上限，防止因为某些情况导致Metaspace无限的使用本地内存，影响到其他程序
# -XX:-OmitStackTraceInFastThrow:解决重复异常不打印堆栈信息问题
#==========================================================================================
JAVA_OPT="-server -Xms2048m -Xmx2048m -Xmn4096m -XX:MetaspaceSize=64m -XX:MaxMetaspaceSize=256m -XX:MaxDirectMemorySize=8336m -XX:+UseG1GC -XX:MaxGCPauseMillis=200 -XX:+AggressiveOpts -XX:+UseFastAccessorMethods"
JAVA_OPT="${JAVA_OPT} -XX:-OmitStackTraceInFastThrow"

# 项目启动端口号
JAVA_PORT="-Dserver.port=8099"

# 项目允许进行服务器debug
JAVA_DEBUG_SERVER="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=50002 -Xnoagent -jar"

# 启动 Maven 项目 （不会打印日志）
nohup java -jar ${JAVA_OPT} ${JAVA_PORT}  ${JAVA_DEBUG_SERVER}  -DJEMonitor=true  ${INSTALLDIR}/boot/${APPLICATION_JAR} --spring.config.location=${CONFIG_DIR} &

echo "项目重新启动成功  \ ^-^ / "
sleep 30