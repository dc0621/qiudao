package com.qiudao.basestudy.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Description: 联系人请求参数
 * @author: gdc
 * @date: 2020/4/4
 * @version 1.0
 */
@Data
public class EmergencyRequest {

    @ApiModelProperty(value="ID")
    protected Long id;

    @ApiModelProperty(value="紧急联系人名称")
    private String name;

    @ApiModelProperty(value="联系方式")
    private String telephone;

    @ApiModelProperty(value="头像图片地址")
    private String imageUrl;

    @ApiModelProperty(value="备注")
    private String description;

    @ApiModelProperty(value="客户ID")
    protected Long customerId;

}
