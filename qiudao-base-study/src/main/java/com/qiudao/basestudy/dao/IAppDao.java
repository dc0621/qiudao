package com.qiudao.basestudy.dao;

import com.github.pagehelper.PageInfo;
import com.qiudao.basestudy.bo.App;

import java.util.List;

/**
 * Description: 应用Dao
 * @author: gdc
 * @date: 2020/3/21
 * @version 1.0
 */
public interface IAppDao {
    /**
     * 通过ID获取应用信息
     * @param id        ID
     * @return          结果
     */
    App get(Long id);

    /**
     * 获取 App 数据列表
     * @param app           查询条件
     * @return              结果信息
     */
    List<App> getAppList(App app);

    /**
     * 新增应用信息
     * @param app       应用
     * @return          结果
     */
    int insert(App app);

    /**
     * 通过ID 删除应用信息
     * @param id        ID
     * @return          结果
     */
    int delete(Long id);

    /**
     * 获取应用的分页数据
     * @param app       请求信息
     * @param pageNum   页码
     * @param pageSize  页面大小
     * @return          结果
     */
    PageInfo findAppByQuery(App app, Integer pageNum, Integer pageSize);
}
