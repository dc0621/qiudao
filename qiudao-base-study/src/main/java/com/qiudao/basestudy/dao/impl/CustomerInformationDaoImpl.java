package com.qiudao.basestudy.dao.impl;

import com.qiudao.basestudy.bo.CustomerInformation;
import com.qiudao.basestudy.dao.ICustomerInformationDao;
import com.qiudao.basestudy.mapper.CustomerInformationMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Description: 客户信息 Dao 实现
 * @author: gdc
 * @date: 2020/4/3
 * @version 1.0
 */
@Repository
public class CustomerInformationDaoImpl implements ICustomerInformationDao {

    @Resource
    private CustomerInformationMapper customerInformationMapper;

    @Override
    public CustomerInformation selectByPrimaryKey(Long id) {
        return customerInformationMapper.selectByPrimaryKey(id);
    }

    @Override
    public int insert(CustomerInformation record) {
        return customerInformationMapper.insert(record);
    }

    @Override
    public void insertList(List<CustomerInformation> items) {
        customerInformationMapper.insertList(items);
    }

    @Override
    public int update(CustomerInformation record) {
        return customerInformationMapper.update(record);
    }

    @Override
    public int updateList(CustomerInformation record, List<Long> idList) {
        return customerInformationMapper.updateList(record, idList);
    }
}
