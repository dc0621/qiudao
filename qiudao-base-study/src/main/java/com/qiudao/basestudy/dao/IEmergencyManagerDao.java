package com.qiudao.basestudy.dao;

import com.qiudao.basestudy.bo.EmergencyManager;

import java.util.List;

/**
 * Description: 联系人信息 Dao
 * @author: gdc
 * @date: 2020/4/3
 * @version 1.0
 */
public interface IEmergencyManagerDao {

    /**
     * 通过ID 查询信息
     * @param id            ID
     * @return              紧急联系人信息
     */
    EmergencyManager selectByPrimaryKey(Long id);

    /**
     *  通过ID列表获取 联系人信息列表
     * @param idList    ID 列表
     * @return          结果
     */
    List<EmergencyManager> listByIdList(List<Long> idList);

    /**
     * 新增
     * @param record        紧急联系人信息
     * @return              结果
     */
    int insert(EmergencyManager record);

    /**
     * 批量新增
     * @param items         紧急联系人列表
     */
    void insertList(List<EmergencyManager> items);

    /**
     * 更新
     * @param record        联系人信息
     * @return              结果
     */
    int update(EmergencyManager record);

    /**
     * 批量更新状态
     * @param record        状态信息
     * @param idList        ID 列表
     * @return              结果
     */
    int updateList(EmergencyManager record, List<Long> idList);


}
