package com.qiudao.basestudy.dao.impl;

import com.github.pagehelper.PageHelper;
import com.qiudao.basestudy.bo.CustomerUserBindingInfo;
import com.qiudao.basestudy.constant.Constants;
import com.qiudao.basestudy.dao.ICustomerUserBindingInfoDao;
import com.qiudao.basestudy.mapper.CustomerUserBindingInfoMapper;
import com.qiudao.basestudy.util.PageUtils;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Description: 客户、用户绑定信息 Dao 实现
 * @author: gdc
 * @date: 2020/4/3
 * @version 1.0
 */
@Repository
public class CustomerUserBindingInfoDaoImpl implements ICustomerUserBindingInfoDao {

    @Resource
    private CustomerUserBindingInfoMapper customerUserBindingInfoMapper;

    @Override
    public CustomerUserBindingInfo selectByPrimaryKey(Long id) {
        return customerUserBindingInfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<CustomerUserBindingInfo> listByQuery(Long customerId, Long shardingUserId, Short shardingUserType) {
        CustomerUserBindingInfo customerUserBindingInfo = new CustomerUserBindingInfo()
                .setCustomerId(customerId)
                .setShardingUserId(shardingUserId)
                .setShardingUserType(shardingUserType);
        return getList(customerUserBindingInfo, null, null);
    }

    @Override
    public int insert(CustomerUserBindingInfo record) {
        return customerUserBindingInfoMapper.insert(record);
    }

    @Override
    public void insertList(List<CustomerUserBindingInfo> items) {
        customerUserBindingInfoMapper.insertList(items);
    }

    @Override
    public int update(CustomerUserBindingInfo record) {
        return customerUserBindingInfoMapper.update(record);
    }

    @Override
    public int updateList(CustomerUserBindingInfo record, List<Long> idList) {
        return customerUserBindingInfoMapper.updateList(record, idList);
    }

    private List<CustomerUserBindingInfo> getList(CustomerUserBindingInfo record, Integer pageNum, Integer pageSize) {
        Map<String, Object> map = new HashMap<>(Constants.INIT_MAP_SIZE);
        map.put("customerId", record.getCustomerId());
        map.put("shardingUserId", record.getShardingUserId());
        map.put("shardingUserType", record.getShardingUserType());
        map.put("isValid", record.getIsValid());
        if (Objects.nonNull(pageSize) && Objects.nonNull(pageNum)) {
            // 分页查询 pageNum 页码， pageSize 显示条数
            PageHelper.startPage(PageUtils.getPageNum(pageNum), PageUtils.getPageSize(pageSize));
        }
        return customerUserBindingInfoMapper.findListByQuery(map);
    }
}
