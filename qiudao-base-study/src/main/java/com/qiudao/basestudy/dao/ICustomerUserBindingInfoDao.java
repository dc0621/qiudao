package com.qiudao.basestudy.dao;

import com.qiudao.basestudy.bo.CustomerUserBindingInfo;

import java.util.List;

/**
 * Description: 客户、用户绑定信息 Dao
 * @author: gdc
 * @date: 2020/4/3
 * @version 1.0
 */
public interface ICustomerUserBindingInfoDao {

    /**
     * 通过ID 查询信息
     * @param id            ID
     * @return              客户、用户绑定信息
     */
    CustomerUserBindingInfo selectByPrimaryKey(Long id);

    /**
     * 通过条件获取 客户绑定列表
     * @param customerId        客户ID
     * @param shardingUserId    用户（分享人、紧急联系人）ID
     * @param shardingUserType  类型
     * @return
     */
    List<CustomerUserBindingInfo> listByQuery(Long customerId, Long shardingUserId, Short shardingUserType);

    /**
     * 新增
     * @param record        绑定信息
     * @return              结果
     */
    int insert(CustomerUserBindingInfo record);

    /**
     * 批量新增
     * @param items         绑定信息
     */
    void insertList(List<CustomerUserBindingInfo> items);

    /**
     * 更新
     * @param record        绑定信息
     * @return              结果
     */
    int update(CustomerUserBindingInfo record);

    /**
     * 批量更新状态
     * @param record        状态信息
     * @param idList        ID 列表
     * @return              结果
     */
    int updateList(CustomerUserBindingInfo record, List<Long> idList);

}
