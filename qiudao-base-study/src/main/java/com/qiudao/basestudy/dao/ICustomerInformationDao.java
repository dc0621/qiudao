package com.qiudao.basestudy.dao;

import com.qiudao.basestudy.bo.CustomerInformation;

import java.util.List;

/**
 * Description: 客户信息 Dao
 * @author: gdc
 * @date: 2020/4/3
 * @version 1.0
 */
public interface ICustomerInformationDao {

    /**
     * 通过ID 查询信息
     * @param id            ID
     * @return              客户信息
     */
    CustomerInformation selectByPrimaryKey(Long id);

    /**
     * 新增客户
     * @param record        客户信息
     * @return              结果
     */
    int insert(CustomerInformation record);

    /**
     * 批量新增
     * @param items         客户列表
     */
    void insertList(List<CustomerInformation> items);

    /**
     * 更新
     * @param record        客户信息
     * @return              结果
     */
    int update(CustomerInformation record);

    /**
     * 批量更新状态
     * @param record        状态信息
     * @param idList        ID 列表
     * @return              结果
     */
    int updateList(CustomerInformation record, List<Long> idList);

}
