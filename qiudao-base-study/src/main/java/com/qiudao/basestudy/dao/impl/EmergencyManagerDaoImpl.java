package com.qiudao.basestudy.dao.impl;

import com.qiudao.basestudy.bo.EmergencyManager;
import com.qiudao.basestudy.dao.IEmergencyManagerDao;
import com.qiudao.basestudy.mapper.EmergencyManagerMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * Description: 联系人信息 Dao 实现
 * @author: gdc
 * @date: 2020/4/3
 * @version 1.0
 */
@Repository
public class EmergencyManagerDaoImpl implements IEmergencyManagerDao {

    @Resource
    private EmergencyManagerMapper emergencyManagerMapper;

    @Override
    public EmergencyManager selectByPrimaryKey(Long id) {
        return emergencyManagerMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<EmergencyManager> listByIdList(List<Long> idList) {
        if (CollectionUtils.isEmpty(idList)) {
            return null;
        }
        return emergencyManagerMapper.listByIdList(idList);
    }

    @Override
    public int insert(EmergencyManager record) {
        return emergencyManagerMapper.insert(record);
    }

    @Override
    public void insertList(List<EmergencyManager> items) {
        emergencyManagerMapper.insertList(items);
    }

    @Override
    public int update(EmergencyManager record) {
        return emergencyManagerMapper.update(record);
    }

    @Override
    public int updateList(EmergencyManager record, List<Long> idList) {
        return emergencyManagerMapper.updateList(record, idList);
    }
}
