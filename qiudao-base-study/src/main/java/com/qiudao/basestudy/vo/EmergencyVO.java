package com.qiudao.basestudy.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Description: 联系人 VO
 * @author: gdc
 * @date: 2020/4/4
 * @version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmergencyVO {

    @ApiModelProperty(value="ID")
    protected Long id;

    @ApiModelProperty(value="紧急联系人名称")
    private String name;

    @ApiModelProperty(value="联系方式")
    private String telephone;

    @ApiModelProperty(value="头像图片地址")
    private String imageUrl;

    @ApiModelProperty(value="备注")
    private String description;
}
