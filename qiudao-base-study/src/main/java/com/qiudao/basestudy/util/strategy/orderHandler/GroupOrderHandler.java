package com.qiudao.basestudy.util.strategy.orderHandler;

import com.qiudao.basestudy.util.strategy.HandlerType;
import org.springframework.stereotype.Component;

/**
 * Description: 具体实现 2
 * @author: gdc
 * @date: 2020/3/14
 * @version 1.0
 */
@Component
@HandlerType("2")
public class GroupOrderHandler implements IOrderHandler {

    @Override
    public String handle() {
        return "处理团购订单";
    }

}