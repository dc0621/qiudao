package com.qiudao.basestudy.util.strategy;

import java.lang.annotation.*;

/**
 * Description: 自定义注解@HandlerType，用于标识该处理器对应哪个订单类型
 * @author: gdc
 * @date: 2020/3/14
 * @version 1.0
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface HandlerType {
    String value();
}
