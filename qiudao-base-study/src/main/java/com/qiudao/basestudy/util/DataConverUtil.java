package com.qiudao.basestudy.util;

import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * Description:
 * 数据转换
 *
 * @author gdc
 * @version 1.0
 * @date 2019-07-20  15:28
 */
public class DataConverUtil {

    /**
     * 用于转换PageInfo 信息
     *
     * @param tClass       转换的PageInfo 类型
     * @param originalList 原始List
     * @param <T>          需要转换的类型
     * @return 返回结果
     */
    public static <T> PageInfo<T> pageInfoConver(Class<T> tClass, List originalList) {
        PageInfo originalPageInfo = new PageInfo(originalList);
        PageInfo<T> resultPageInfo = new PageInfo<>();
        BeanUtils.copyProperties(originalPageInfo, resultPageInfo);
        resultPageInfo.setList(listTypeConver(tClass, originalList));
        return resultPageInfo;
    }

    /**
     * 用于将原List数据转换为其他类型的列表数据
     *
     * @param tClass       目标类格式
     * @param originalList 原始列表数据
     * @return 返回指定类类型的列表数据
     */
    public static <T> List<T> listTypeConver(Class<T> tClass, List originalList) {
        List<T> resultList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(originalList)) {
            originalList.forEach(originalObj -> {
                T resultObj = null;
                try {
                    resultObj = tClass.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                BeanUtils.copyProperties(originalObj, resultObj);
                resultList.add(resultObj);
            });
        }
        return resultList;
    }

    /**
     * 获取分月表后缀
     *
     * @param date 给定日期
     */
    public static String getMonthTableSuffix(Date date) {
        StringBuffer result = new StringBuffer();
        return Objects.nonNull(date) ? result.append(DateUtil.format(date, "yy_")).append(Integer.parseInt(DateUtil.format(date, "MM"))).toString()
                : result.append(DateUtil.format(new Date(), "yy_")).append(Integer.parseInt(DateUtil.format(new Date(), "MM"))).toString();
    }

    /**
     *  获取两个日期间年月集合  格式： yy_MM,例如 20_1, 20_11
     * @param minDate       开始日期
     * @param maxDate       接收日期
     * @return 日期集合 格式为 yy_MM
     */
    public static List<String> getMonthBetween(Date minDate, Date maxDate){
        ArrayList<String> result = new ArrayList<>();
        Calendar min = Calendar.getInstance();
        Calendar max = Calendar.getInstance();
        min.setTime(minDate);
        min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);
        max.setTime(maxDate);
        max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);
        Calendar curr = min;
        while (curr.before(max)) {
            result.add(DateUtil.format(curr.getTime(), "yy_") + Integer.valueOf(DateUtil.format(curr.getTime(), "MM")));
            curr.add(Calendar.MONTH, 1);
        }
        return result;
    }

    /**
     * 列表去重
     * @param list1     原List
     * @param list2     去重List
     * @param <T>       类型
     * @return          结果
     */
    public static <T> List<T> listDeduplic(List<T> list1, List<T> list2) {
        HashSet hs1 = new HashSet(list1);
        HashSet hs2 = new HashSet(list2);
        hs1.removeAll(hs2);
        return new ArrayList<>(hs1);
    }

}
