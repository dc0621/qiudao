package com.qiudao.basestudy.util.autowired_bean;

import com.qiudao.basestudy.util.JacksonJsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Description: Springboot中如何在Utils类中使用@Autowired注入bean
 *
 * Springboot中如果希望在Utils工具类中，使用到我们已经定义过的Dao层或者Service层Bean，可以如下编写Utils类：
 *  1. 使用@Component注解标记工具类UserUtils：
 *  2. 使用@Autowired(@Autowired和@Resource的区别不再介绍)注入我们需要的bean：
 *  3. 在工具类中编写init()函数，并使用@PostConstruct注解标记工具类，初始化Bean：
 *
 *
 * @author: gdc
 * @date: 2021/9/14
 * @version 1.0
 */
@Component
public class UserUtils {

    @Autowired
    private UserBean userBean;

    private static UserUtils userUtils;

    /**
     * - @PostConstruct 是Java自己的注解
     * 用来修饰一个非静态的void（）方法。被@PostConstruct修饰的方法会在服务器加载Servlet的时候运行，并且只会被服务器执行一次。PostConstruct在构造函数之后执行，init（）方法之前执行。
     * 通常我们会是在Spring框架中使用到@PostConstruct注解 该注解的方法在整个Bean初始化中的执行顺序：
     * Constructor(构造方法) -> @Autowired(依赖注入) -> @PostConstruct(注释的方法)
     *
     * 应用：在静态方法中调用依赖注入的Bean中的方法。
     */
    @PostConstruct
    public void init() {
        userUtils = this;
        userUtils.userBean = userBean;
    }

    /**
     * 打印用户名称
     */
    public static String getUserName(){
        System.out.println("用户名称：" + userUtils.userBean.getName());
        return userUtils.userBean.getName();
    }

    /**
     * 打印用户名称
     */
    public static UserBean getUserInfo(){
        System.out.println("用户信息：" + JacksonJsonUtil.toJsonString(userUtils.userBean));
        return userUtils.userBean;
    }

}
