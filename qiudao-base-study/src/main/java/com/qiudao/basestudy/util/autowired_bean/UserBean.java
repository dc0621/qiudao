package com.qiudao.basestudy.util.autowired_bean;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Description: 用户信息
 * @author: gdc
 * @date: 2021/9/14
 * @version 1.0
 */
@Data
@Component
@ConfigurationProperties(prefix = "userinfo")
public class UserBean {

    /**
     * 名称
     */
    private String name;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 性别
     */
    private Boolean sex;

}
