package com.qiudao.basestudy.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Description: Jackson Util 工具类
 * 依赖 com.fasterxml.jackson.core
 *      com.fasterxml.jackson.databind
 *      slf4j
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class JacksonJsonUtil {
	private static final Logger logger = LoggerFactory.getLogger(JacksonJsonUtil.class);
	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();


    /**
     * Object对象转换为String
     *
     * @param data Object对象
     * @return Object对象对应的字符串
     */
    public static <T> String toJsonString(T data) {
        try {
            return OBJECT_MAPPER.writeValueAsString(data);
        } catch (JsonProcessingException ex) {
            if (logger.isErrorEnabled()) {
                logger.error("json parse error:", ex);
            }
            return "json parse error:" + ex.getMessage();
        }
    }

    /**
     * 字符串转换为对象
     *
     * @param json  字符串
     * @param clazz 类类型
     * @return clazz类型的对象
     */
    public static <R> R json2Object(String json, Class<R> clazz) {
        try {
            return OBJECT_MAPPER.readValue(json, clazz);
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("json parse error:", ex);
            }
        }
        return null;
    }

    /**
     * 字节转换为对象
     *
     * @param jsonBuffer 字节
     * @param clazz      类类型
     * @return clazz类型的对象
     */
    public static <R> R json2Object(byte[] jsonBuffer, Class<R> clazz) {
        try {
            return OBJECT_MAPPER.readValue(jsonBuffer, clazz);
        } catch (Exception ex) {
            if (logger.isErrorEnabled()) {
                logger.error("json parse error:", ex);
            }
        }
        return null;
    }

    /**
     * map  转JavaBean
     * @param map       Map 对象
     * @param clazz     class
     * @return          class类型的对象
     */
    public static <T> T map2pojo(Map map, Class<T> clazz) {
        return OBJECT_MAPPER.convertValue(map, clazz);
    }

    /**
     * json字符串转换为map
     * @param jsonString        json字符串
     * @param clazz             转换后的类型
     * @return                  Map 对象
     */
    public static <T> Map<String, T> json2map(String jsonString, Class<T> clazz) {
        Map<String, T> result = null;
        try {
            Map<String, Map<String, Object>> map = (Map<String, Map<String, Object>>) OBJECT_MAPPER.readValue(jsonString, new TypeReference<Map<String, T>>() {});
            result = new HashMap<>(16);
            for (Map.Entry<String, Map<String, Object>> entry : map.entrySet()) {
                result.put(entry.getKey(), map2pojo(entry.getValue(), clazz));
            }
        } catch (IOException e) {
            if (logger.isErrorEnabled()) {
                logger.error("json parse error:", e);
            }
        }
        return result;
    }

}