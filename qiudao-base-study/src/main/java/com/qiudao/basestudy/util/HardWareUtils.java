package com.qiudao.basestudy.util;

import javax.servlet.http.HttpServletRequest;
import java.io.*;

/**
 * Description: 获取电脑硬件信息工具类
 * 依赖 javax.servlet.servlet-api
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class HardWareUtils {

    /**
     * 获取主板序列号
     *
     * @return      主板序列号
     */
    public static String getMotherboardSN() {
        String result = "";
        try {
            File file = File.createTempFile("realhowto", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new FileWriter(file);
            String vbs = "Set objWMIService = GetObject(\"winmgmts:\\\\.\\root\\cimv2\")\n"
                    + "Set colItems = objWMIService.ExecQuery _ \n"
                    + "   (\"Select * from Win32_BaseBoard\") \n"
                    + "For Each objItem in colItems \n"
                    + "    Wscript.Echo objItem.SerialNumber \n"
                    + "    exit for  ' do the first cpu only! \n" + "Next \n";

            fw.write(vbs);
            fw.close();
            Process p = Runtime.getRuntime().exec(
                    "cscript //NoLogo " + file.getPath());
            BufferedReader input = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.trim();
    }

    /**
     * 获取硬盘序列号
     *
     * @param       drive 盘符
     * @return      硬盘序列号
     */
    public static String getHardDiskSN(String drive) {
        String result = "";
        try {
            File file = File.createTempFile("realhowto", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new FileWriter(file);

            String vbs = "Set objFSO = CreateObject(\"Scripting.FileSystemObject\")\n"
                    + "Set colDrives = objFSO.Drives\n"
                    + "Set objDrive = colDrives.item(\""
                    + drive
                    + "\")\n"
                    + "Wscript.Echo objDrive.SerialNumber"; // see note
            fw.write(vbs);
            fw.close();
            Process p = Runtime.getRuntime().exec(
                    "cscript //NoLogo " + file.getPath());
            BufferedReader input = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.trim();
    }

    /**
     * 获取CPU序列号
     *
     * @return      CPU序列号
     */
    public static String getCPUSerial() {
        String result = "";
        try {
            File file = File.createTempFile("tmp", ".vbs");
            file.deleteOnExit();
            FileWriter fw = new FileWriter(file);
            String vbs = "Set objWMIService = GetObject(\"winmgmts:\\\\.\\root\\cimv2\")\n"
                    + "Set colItems = objWMIService.ExecQuery _ \n"
                    + "   (\"Select * from Win32_Processor\") \n"
                    + "For Each objItem in colItems \n"
                    + "    Wscript.Echo objItem.ProcessorId \n"
                    + "    exit for  ' do the first cpu only! \n" + "Next \n";

            // + "    exit for  \r\n" + "Next";
            fw.write(vbs);
            fw.close();
            Process p = Runtime.getRuntime().exec(
                    "cscript //NoLogo " + file.getPath());
            BufferedReader input = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                result += line;
            }
            input.close();
            file.delete();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        if (result.trim().length() < 1 || result == null) {
            result = "无CPU_ID被读取";
        }
        return result.trim();
    }

    /**
     * 获取MAC地址
     * @return      MAC地址
     */
    public static String getMac() {
        String result = "";
        try {
            Process process = Runtime.getRuntime().exec("ipconfig /all");
            InputStreamReader ir = new InputStreamReader(process.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);
            String line;
            while ((line = input.readLine()) != null) {
                if (line.indexOf("Physical Address") > 0) {
                    String MACAddr = line.substring(line.indexOf("-") - 2);
                    result = MACAddr;
                }
            }
        } catch (IOException e) {
            System.err.println("IOException " + e.getMessage());
        }
        return result;
    }

    /**
     * 获取IP信息
     * @param request   Http 请求
     * @return          IP
     */
    public static String getIp(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        try {
            if (ip != null) {
                if (!ip.isEmpty() && !"unKnown".equalsIgnoreCase(ip)) {
                    int index = ip.indexOf(",");
                    if (index != -1) {
                        return ip.substring(0, index);
                    } else {
                        return ip;
                    }
                }
            }
            ip = request.getHeader("X-Real-IP");
            if (ip != null) {
                if (!ip.isEmpty() && !"unKnown".equalsIgnoreCase(ip)) {
                    return ip;
                }
            }
            ip = request.getHeader("Proxy-Client-IP");
            if (ip != null) {
                if (!ip.isEmpty() && !"unKnown".equalsIgnoreCase(ip)) {
                    return ip;
                }
            }
            ip = request.getHeader("WL-Proxy-Client-IP");
            if (ip != null) {
                if (!ip.isEmpty() && !"unKnown".equalsIgnoreCase(ip)) {
                    return ip;
                }
            }
            ip = request.getRemoteAddr();
        } catch (Exception e) {}
        return ("0:0:0:0:0:0:0:1").equals(ip) ? "127.0.0.1" : ip;
    }

}
