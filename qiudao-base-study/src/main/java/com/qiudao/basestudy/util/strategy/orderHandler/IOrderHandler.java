package com.qiudao.basestudy.util.strategy.orderHandler;

/**
 * Description: 自定义处理接口，所有需要实现的方式都需要实现该接口
 * @author: gdc
 * @date: 2020/3/14
 * @version 1.0
 */
public interface IOrderHandler {

    String handle();

    // .....此处编写抽象的方法
}