package com.qiudao.basestudy.util.strategy.orderHandler;

import com.qiudao.basestudy.util.strategy.HandlerType;
import org.springframework.stereotype.Component;

/**
 * Description: 具体实现 3
 * @author: gdc
 * @date: 2020/3/14
 * @version 1.0
 */
@Component
@HandlerType("3")
public class PromotionOrderHandler implements IOrderHandler {

    @Override
    public String handle() {
        return "处理促销订单";
    }

}