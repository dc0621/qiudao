package com.qiudao.basestudy.util.strategy;

import com.qiudao.basestudy.util.strategy.orderHandler.IOrderHandler;

import java.util.Map;

/**
 * Description:  该类用于注入到Spring 中
 * 参数handlerMap, key : @HandleType 的值
 *                value: 具体的实现类
 * @author: gdc
 * @date: 2020/3/14
 * @version 1.0
 */
public class HandlerContext {
    private Map<String, Class> handlerMap;
    public HandlerContext(Map<String, Class> handlerMap) {
        this.handlerMap = handlerMap;
    }

    /**
     * 通过传入参数获取具体实现对象
     * @param type          自定义注解中设置的值
     * @return              具体实例对象
     */
    public IOrderHandler getInstance(String type) {
        Class clazz = handlerMap.get(type);
        if (clazz == null) {
            throw new IllegalArgumentException("not found orderHandler for type: " + type);
        }
        return (IOrderHandler) BeanTool.getBean(clazz);
    }

}