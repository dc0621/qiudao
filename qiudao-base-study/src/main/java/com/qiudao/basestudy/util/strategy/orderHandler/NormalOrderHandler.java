package com.qiudao.basestudy.util.strategy.orderHandler;

import com.qiudao.basestudy.util.strategy.HandlerType;
import org.springframework.stereotype.Component;

/**
 * Description: 具体实现 1
 * @author: gdc
 * @date: 2020/3/14
 * @version 1.0
 */
@Component
@HandlerType("1")
public class NormalOrderHandler implements IOrderHandler {

    @Override
    public String handle() {
        return "处理普通订单";
    }

}