package com.qiudao.basestudy.util.result;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Description: 返回结果 枚举信息
 * @author: gdc
 * @date: 2020/3/21
 * @version 1.0
 */
@Getter
@AllArgsConstructor
public enum BaseResultEnum {
    /*
    返回的状态信息
     */
    SUCCESS(Boolean.TRUE, "0000", "成功"),
    ERROR(Boolean.FALSE, "9999", "失败"),
    NULL_POINT(Boolean.FALSE, "0001", "数据为空"),
    HTTP_CLIENT_ERROR(Boolean.FALSE, "0002", "网络请求异常"),
    ;


    @ApiModelProperty(value = "是否成功", required = true)
    private boolean success;
    @ApiModelProperty(value = "状态码", required = true)
    private String code;
    @ApiModelProperty(value = "状态信息")
    private String msg;
}
