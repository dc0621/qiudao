package com.qiudao.basestudy.config.runner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Description: CommandLineRunner 学习
 *
 * CommandLineRunner 接口是在容器启动成功后的最后一步回调（类似开机自启动）
 * - @Order 用来定义数据加载类的顺序
 * @author: gdc
 * @date: 2020/4/4
 * @version 1.0
 */
@Order(1)
@Component
public class FirstRunner implements CommandLineRunner {

    @Override
    public void run(String... args) {
        System.out.println("First, 初始化后我是第一个执行的");
    }
}
