package com.qiudao.basestudy.config.runner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(2)
@Component
public class SecondRunner implements CommandLineRunner {

    @Override
    public void run(String... args) {
        System.out.println("Second, 初始化后我是第二个执行的");
    }
}
