package com.qiudao.basestudy.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * Description: 客户信息表
 * @author: gdc
 * @date: 2020/4/3
 * @version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "客户信息表")
public class CustomerInformation extends BaseBo implements Serializable {

    @ApiModelProperty(value="用户名称")
    private String customerName;

    @ApiModelProperty(value="联系方式")
    private String telephone;

    @ApiModelProperty(value="居住地址")
    private String address;

    @ApiModelProperty(value="生日")
    private Date birthday;

    @ApiModelProperty(value="微信号")
    private String wechatNo;

    @ApiModelProperty(value="微信授权码")
    private String wechatAuthorizationCode;

    @ApiModelProperty(value="微信小程序公众号绑定unionId")
    private String unionId;

    @ApiModelProperty(value="登录名")
    private String loginName;

    @ApiModelProperty(value="登录密码")
    private String loginPwd;

    @ApiModelProperty(value="邮箱")
    private String email;

    @ApiModelProperty(value="性别")
    private String gender;

    @ApiModelProperty(value="备注")
    private String description;

    @ApiModelProperty(value="推送方式0:微信 1:短信  2:电话")
    private Short pushType;

    @ApiModelProperty(value="紧急联系人")
    private String emergencyTelephone;

    @ApiModelProperty(value="用户城市")
    private String city;

    @ApiModelProperty(value="用户省份")
    private String province;

    @ApiModelProperty(value="用户头像")
    private String headimgurl;

    @ApiModelProperty(value="用户语言")
    private String userLanguage;

    @ApiModelProperty(value="公司标识 0用户，1公司")
    private Short companyFlag;

    @ApiModelProperty(value="用户编码")
    private String companyCode;

    private static final long serialVersionUID = 1L;

}