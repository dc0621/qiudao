package com.qiudao.basestudy.bo;

import lombok.Data;

/**
 * Description: 定义一个小狗类
 * @author: gdc
 * @date: 2019/9/17
 * @version 1.0
 */
@Data
public class Dog {
    /**
     * 名称
     */
    private String name;
    /**
     * 年龄
     */
    private Integer age;
}
