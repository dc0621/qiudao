package com.qiudao.basestudy.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * Description: 客户用户绑定关系表
 * @author: gdc
 * @date: 2020/4/3
 * @version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "客户用户绑定关系表")
public class CustomerUserBindingInfo extends BaseBo implements Serializable {

    @ApiModelProperty(value="用户编号")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long customerId;

    @ApiModelProperty(value="被分享人或紧急联系人编号")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long shardingUserId;

    @ApiModelProperty(value="用户类型 1:分享人 2:紧急联系人")
    private Short shardingUserType;

    private static final long serialVersionUID = 1L;
}