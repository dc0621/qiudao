package com.qiudao.basestudy.bo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Description: 基础 BO 类
 * @author: gdc
 * @date: 2020/4/3
 * @version 1.0
 */
@Data
public class BaseBo implements Serializable {

    /**
     * - @JsonFormat(shape = JsonFormat.Shape.STRING)
     * 会将当前的Long类型以 String 类型进行返回
     */
    @ApiModelProperty(value="ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    protected Long id;

    @ApiModelProperty(value="是否有效")
    protected Short isValid;

    @ApiModelProperty(value="记录生成时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    protected Date createTime;

    @ApiModelProperty(value="记录创建人编号")
    protected String createUserId;

    @ApiModelProperty(value="版本号")
    protected Integer lastVer;

    @ApiModelProperty(value="操作人ID")
    protected String opUserId;

    @ApiModelProperty(value="操作人名")
    protected String opUserName;

    @ApiModelProperty(value="操作时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    protected Date opTime;
}
