package com.qiudao.basestudy.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * Description: 紧急联系人
 * @author: gdc
 * @date: 2020/4/3
 * @version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "紧急联系人")
public class EmergencyManager extends BaseBo implements Serializable {

    @ApiModelProperty(value="紧急联系人名称")
    private String name;

    @ApiModelProperty(value="联系方式")
    private String telephone;

    @ApiModelProperty(value="头像图片地址")
    private String imageUrl;

    @ApiModelProperty(value="备注")
    private String description;

    private static final long serialVersionUID = 1L;
}