package com.qiudao.basestudy.bo;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * Description: 定义一个人,主要的作用是为了引入指定配置文件中的属性值
 * @author: gdc
 * @date: 2019/9/17
 * @version 1.0
 *
 * -@ConfigurationProperties 可以读取配置文件中的信息
 * 并可以和@Validated 校验注解绑定使用
 * -@PropertySource：加载指定的配置文件；
 */
@Data
@Component
@Validated
@PropertySource(value = "classpath:/spring/application-study.properties")
@ConfigurationProperties(prefix = "properties.person")
public class Person {
    /**
     * 姓名
     * - @NotNull可以验证该熟悉值是否为空，但是该类必须使用 @Validated 注解
     */
    @NotNull
    private String name;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 性别
     */
    private Boolean sex;
    /**
     * 小狗
     */
    private Dog dog;
    /**
     * map
     */
    private Map<String, Object> pMap;
    /**
     * list
     */
    private List<String> pList;
}
