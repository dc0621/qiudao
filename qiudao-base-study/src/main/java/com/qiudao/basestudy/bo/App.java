package com.qiudao.basestudy.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * Description: App
 * @author: gdc
 * @date: 2020/3/21
 * @version 1.0
 *
 *  \@ApiModel()         用于响应类上，表示一个返回响应数据的信息
 *  value                表示对象名
 *  description          描述
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "应用类", description = "应用信息")
public class App extends BaseBo implements Serializable {

    /**
     * \@ApiOperation() 用于方法；表示一个http请求的操作 
     * value        用于方法描述 
     * notes        方法的备注说明
     * tags         可以重新分组（视情况而用） 
     * response     返回的对象
     */
    @ApiModelProperty(value="应用名称", required = true)
    private String name;

    @ApiModelProperty(value="应用编码，平台唯一自生成", required = true)
    private String code;

    @ApiModelProperty(value="应用认证密码")
    private String password;

    @ApiModelProperty(value="系统地址")
    private String appUrl;

    private static final long serialVersionUID = 1L;

}