package com.qiudao.basestudy.controller;

import com.qiudao.basestudy.request.EmergencyRequest;
import com.qiudao.basestudy.service.IEmergencyManagerService;
import com.qiudao.basestudy.util.result.BaseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Description: 联系人请求接口 
 * @author: gdc
 * @date: 2020/4/4
 * @version 1.0
 */
@Api(tags = "联系人请求接口")
@RestController
@RequestMapping("/emergency")
public class EmergencyManagerController {
    
    @Resource
    private IEmergencyManagerService emergencyManagerService;

    @ApiOperation(value = "根据ID查询查询紧急联系人信息", response = BaseResult.class)
    @GetMapping("/v1/getById/{id}")
    public BaseResult getById(@PathVariable("id") Long id) {
        return emergencyManagerService.get(id);
    }

    @ApiOperation(value = "根据客户查询紧急联系人信息列表", response = BaseResult.class)
    @PostMapping("/v1/listByCustomerId")
    public BaseResult listByCustomerId(@RequestBody EmergencyRequest request) {
        return emergencyManagerService.listByCustomerId(request.getCustomerId());
    }

}
