package com.qiudao.basestudy.controller;

import com.qiudao.basestudy.util.result.BaseResult;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Description: Swagger 接口请求地址
 * 请求地址 host:port/path/swagger-ui.html
 * 注解可以参考   https://www.jianshu.com/p/f30e0c646c63
 * @author: gdc
 * @date: 2019/9/14
 * @version 1.0
 *
 * \@Api Api 用在类上，说明该类的作用。可以标记一个Controller类做为swagger 文档资源
 * value        该参数没什么意义，不需要配置
 * tags         说明该类的作用，如果设置给值，value 值会被覆盖
 * description  对api资源的描述
 * hidden       配置为true将在文档中隐藏
 */
@Api(tags = "员工请求接口", description = "当前接口主要员工信息操作的")
@RestController
@RequestMapping("/swagger")
public class SwaggerController {

    /**
     * \@ApiOperation() 用于方法；表示一个http请求的操作 
     * value        用于方法描述 
     * notes        方法的备注说明
     * tags         可以重新分组（视情况而用） 
     * response     返回的对象
     */
    @ApiOperation(value = "根据条件分页查询员工信息", notes = "根据请求条件，进行分页查询", response = BaseResult.class)
    @PostMapping("/v1/emp_by_query")
    public BaseResult selectEmpByQuery(){
        return null;
    }

    /**
     * \@ApiImplicitParams()    用于方法，包含多个 @ApiImplicitParam 
     * \@ApiImplicitParam()     用于方法 ,表示单独的请求参数 
     * name         参数ming 
     * value        参数说明 
     * dataType     数据类型 
     * paramType    参数类型 
     * example      举例说明
     */
    @ApiOperation(value = "新增用户信息", notes = "新增用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "用户名", required = true, paramType = "form"),
            @ApiImplicitParam(name = "age", value = "年龄", required = true, paramType = "form", dataType = "Integer"),
            @ApiImplicitParam(name = "desc", value = "详情", paramType = "form")
    })
    @PostMapping("/v1/add_emp")
    public void addEmp(String name, Integer age, String desc){}


    /**
     * \@ApiParam() 用于方法，参数，字段说明；表示对参数的添加元数据（说明或是否必填等） 
     * name         参数名 
     * value        参数说明 
     * required     是否必填
     */
    @ApiOperation(value = "修改用户信息", notes = "修改用户信息")
    @PostMapping("/v1/edit_emp")
    public void editEmp(@ApiParam(value = "用户ID", required = true) Long id, @ApiParam(value = "用户名称") String name){}

    /**
     * \@ApiResponses：用于请求的方法上，表示一组响应
     * \@ApiResponse：用在@ApiResponses中，一般用于表达一个错误的响应信息
     * code         数字，例如400
     * message      信息，例如"请求参数没填好"
     * response     抛出异常的类
     */
    @ApiOperation(value = "删除用户信息", notes = "通过ID删除用户信息")
    @ApiResponses(value = {
            @ApiResponse(code = 401, message = "未未授权"),
            @ApiResponse(code = 500, message = "server error"),
            @ApiResponse(code = 200, message = "success")})
    @PostMapping("/v1/delete_emp")
    public void deleteEmp(){}

    /**
     * \@ApiIgnore()用于类或者方法上，可以不被swagger显示在页面上 
     */
    @ApiIgnore
    @PostMapping("/v1/emp_by_id")
    public void getById(){}

}
