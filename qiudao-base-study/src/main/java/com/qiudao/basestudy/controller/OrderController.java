package com.qiudao.basestudy.controller;

import com.qiudao.basestudy.service.IOrderService;
import com.qiudao.basestudy.util.result.BaseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Description: 该类主要来演示 SpringBoot + 策略模式
 *  参考： https://juejin.im/post/5c551122e51d457fcc5a9790#comment
 *  GitHup: https://github.com/ciphermagic/java-learn/tree/master/sandbox/src/main/java/com/cipher/handler_demo
 * @author: gdc
 * @date: 2020/3/14
 * @version 1.0
 */

@Api(tags = "订单请求接口", description = "主要来演示 SpringBoot + 策略模式")
@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private IOrderService orderService;

    /**
     * 请求地址：    http://localhost:10000/order/get/1
     * @param type          类型信息：1、2、3
     */
    @ApiOperation(value = "根据类型获取订单信息", response = BaseResult.class)
    @GetMapping("/get/{type}")
    public BaseResult get(@PathVariable("type") String type){
        return BaseResult.success(orderService.handle(type));
    }

}
