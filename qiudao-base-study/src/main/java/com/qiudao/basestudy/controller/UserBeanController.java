package com.qiudao.basestudy.controller;

import com.qiudao.basestudy.util.autowired_bean.UserUtils;
import com.qiudao.basestudy.util.result.BaseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description: Springboot中如何在Utils类中使用@Autowired注入bean
 * @author: gdc
 * @date: 2021/9/14
 * @version 1.0
 */
@Api(tags = "订单请求接口", description = "主要来演示 SpringBoot + 策略模式")
@RestController
@RequestMapping("/user")
public class UserBeanController {

    /**
     * 请求地址：    http://localhost:10000/user/getUserName
     */
    @ApiOperation(value = "获取用户名称", response = BaseResult.class)
    @GetMapping("/getUserName")
    public BaseResult getUserName(){
        return BaseResult.success(UserUtils.getUserName());
    }

    /**
     * 请求地址：    http://localhost:10000/user/getUserInfo
     */
    @ApiOperation(value = "获取用户信息", response = BaseResult.class)
    @GetMapping("/getUserInfo")
    public BaseResult getUserInfo(){
        return BaseResult.success(UserUtils.getUserInfo());
    }

}
