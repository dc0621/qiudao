package com.qiudao.basestudy.controller;

import com.qiudao.basestudy.bo.App;
import com.qiudao.basestudy.service.IAppService;
import com.qiudao.basestudy.util.result.BaseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Description: 应用请求接口
 * @author: gdc
 * @date: 2020/4/4
 * @version 1.0
 */
@Api(tags = "应用请求接口", description = "主要进行App信息操作")
@RestController
@RequestMapping("/app")
public class AppController {

    @Resource
    private IAppService appService;

    /**
     * 通过ID获取应用信息
     * @param id        ID
     * @return          结果
     */
    @ApiOperation(value = "根据ID查询应用信息", response = BaseResult.class)
    @GetMapping("/v1/get")
    public BaseResult get(Long id){
        return appService.get(id);
    }

    /**
     * 获取所有的信息
     * @return          结果
     */
    @ApiOperation(value = "获取应用信息列表", response = BaseResult.class)
    @RequestMapping("/v1/list")
    public BaseResult list(){
        return appService.list();
    }

    /**
     * 获取应用的分页数据 http://localhost:10000/app/v1/getPageByQuery/0/5
     * @return          结果
     */
    @ApiOperation(value = "获取应用的分页数据", response = BaseResult.class)
    @RequestMapping("/v1/getPageByQuery/{pageNum}/{pageSize}")
    public BaseResult getPageByQuery(@PathVariable("pageNum") Integer pageNum, @PathVariable("pageSize") Integer pageSize){
        return appService.getPageByQuery(pageNum, pageSize);
    }

    /**
     * 新增应用信息
     * @param app       应用
     * @return          结果
     */
    @ApiOperation(value = "新增应用信息", response = BaseResult.class)
    @GetMapping("/v1/insert")
    public BaseResult insert(App app){
        return appService.insert(app);
    }

    /**
     * 通过ID 删除应用信息
     * @param id        ID
     * @return          结果
     */
    @ApiOperation(value = "根据ID删除应用信息", response = BaseResult.class)
    @GetMapping("/v1/delete ")
    public BaseResult delete(Long id){
        return appService.delete(id);
    }


}
