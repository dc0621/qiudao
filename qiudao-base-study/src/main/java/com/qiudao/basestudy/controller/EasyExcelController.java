package com.qiudao.basestudy.controller;

import com.alibaba.excel.metadata.Sheet;
import com.qiudao.basestudy.bo.TableHeaderExcelProperty;
import com.qiudao.basestudy.util.DateUtil;
import com.qiudao.basestudy.util.ExcelUtil;
import com.qiudao.basestudy.util.JacksonJsonUtil;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * Description: Easy Excel 测试接口
 * @author: gdc
 * @date: 2020/4/4
 * @version 1.0
 */
@Slf4j
@Api(tags = "Easy Excel请求接口", description = "主要进行Easy Excel信息操作")
@RestController
@RequestMapping("/easyExcel")
public class EasyExcelController {
    String fileName = "d:/应用_" + DateUtil.format(new Date(), DateUtil.ISO_DATE_FORMAT) + ".xlsx";

    /**
     * Excel 生成，简单版
     */
    @GetMapping(value = "/v1/write/1")
    public void writeExcel1() {
        List<List<Object>> data = new ArrayList<>();
        data.add(Arrays.asList("111","222","333"));
        data.add(Arrays.asList("111","222","333"));
        data.add(Arrays.asList("111","222","333"));
        List<String> head = Arrays.asList("表头1", "表头2", "表头3");
        Sheet sheet = new Sheet(1, 1);
        sheet.setSheetName("应用信息");
        ExcelUtil.writeSimpleBySheet(fileName, data, head, sheet);
    }

    /**
     * Excel 生成，添加 Sheet信息
     */
    @GetMapping(value = "/v1/write/2")
    public void writeExcel2() {
        List<List<Object>> data = new ArrayList<>();
        data.add(Arrays.asList("111","222","333"));
        data.add(Arrays.asList("111","222","333"));
        data.add(Arrays.asList("111","222","333"));
        List<String> head = Arrays.asList("表头1", "表头2", "表头3");
        Sheet sheet = new Sheet(1, 1);
        sheet.setSheetName("应用信息");
        ExcelUtil.writeSimpleBySheet(fileName, data, head, sheet);
    }

    /**
     *  Excel 生成
     */
    @GetMapping(value = "/v1/write")
    public void writeExcel() {
        ArrayList<TableHeaderExcelProperty> originalList = new ArrayList<>();
        for(int i = 0; i < 4; i++){
            TableHeaderExcelProperty tableHeaderExcelProperty = new TableHeaderExcelProperty()
                    .setName("cmj" + i).setAge(22 + i).setSchool("清华大学" + i);
            originalList.add(tableHeaderExcelProperty);
        }
        ExcelUtil.MultipleSheelPropety multipleSheelPropety = new ExcelUtil.MultipleSheelPropety();
        multipleSheelPropety.setData(originalList);
        Sheet sheet = new Sheet(1, 1);
        sheet.setSheetName("应用信息");
        multipleSheelPropety.setSheet(sheet);
        List<ExcelUtil.MultipleSheelPropety> finalList = Collections.singletonList(multipleSheelPropety);
        ExcelUtil.writeWithMultipleSheel(fileName,finalList);
    }

    /**
     *  Excel 读取
     */
    @GetMapping(value = "/v1/read")
    public void ReadExcel(){
        // 读取少于1000行的excle
        List<Object> list1 = ExcelUtil.readMoreThan1000Row(fileName);
        log.info("获取的数据为： {}", JacksonJsonUtil.toJsonString(list1));

        log.info("--------------------------------------------------------");

        // 读取少于1000行的excle，可以指定sheet和从几行读起
        Sheet sheet = new Sheet(1, 1);
        List<Object> list2 = ExcelUtil.readLessThan1000RowBySheet(fileName, sheet);
        log.info("获取的数据为： {}", JacksonJsonUtil.toJsonString(list2));

        log.info("--------------------------------------------------------");

        // 读取大于1000行的excle
        // 带sheet参数的方法可参照测试方法readLessThan1000RowBySheet()
        List<Object> list3 = ExcelUtil.readMoreThan1000Row(fileName);
        log.info("获取的数据为： {}", JacksonJsonUtil.toJsonString(list3));

        log.info("--------------------------------------------------------");

        // 读取大于1000行的excle
        // 带sheet参数的方法可参照测试方法readLessThan1000RowBySheet()
        List<Object> list4 = ExcelUtil.readMoreThan1000RowBySheet(fileName, sheet);
        log.info("获取的数据为： {}", JacksonJsonUtil.toJsonString(list4));
    }
}
