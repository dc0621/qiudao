package com.qiudao.basestudy.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Description: 自定义异常类
 * @author: gdc
 * @date: 2020/3/21
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BizException extends RuntimeException {

    /**
     * 异常错误码
     */
    private String code;

    public BizException(String message) {
        super(message);
    }

    public BizException(String code, String message) {
        super(message);
        this.code = code;
    }
}
