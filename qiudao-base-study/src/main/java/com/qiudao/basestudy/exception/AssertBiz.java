package com.qiudao.basestudy.exception;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * Description: 业务断言类
 * @author: gdc
 * @date: 2019/9/1
 * @version 1.0
 */
public class AssertBiz {

    /**
     * 判断对象不为null，若为null,报出业务异常消息
     * @param source        断言对象
     * @param message       失败返回结果
     */
    public static void isNotNull(Object source, String message){
        if (source == null) {
            throw new BizException(message);
        }
    }

    /**
     * 判断字符串是否为空，若为空,报出业务异常消息
     * @param source        断言对象
     * @param message       失败返回结果
     */
    public static void hasLength(String source, String message) {
        if (StringUtils.isBlank(source)) {
            throw new BizException(message);
        }
    }

    /**
     * 判断条件是否为true，若为false,报出业务异常消息
     * @param b             判断条件
     * @param message       失败返回结果
     */
    public static void isTrue(boolean b, String message) {
        if (!b) {
            throw new BizException(message);
        }
    }

    /**
     * 判断条件是否为false，若为true,报出业务异常消息
     * @param b             判断条件
     * @param message       失败返回结果
     */
    public static void isFalse(boolean b, String message) {
        if (b) {
            throw new BizException(message);
        }
    }

    /**
     * 判断字符串是否不为空，若为空,报出业务异常消息
     * @param source        断言的字符串
     * @param message       失败返回结果
     */
    public static void isNotBlank(String source, String message) {
        if (StringUtils.isBlank(source)) {
            throw new BizException(message);
        }
    }

    /**
     * 判断字符串是否为数字，若不为数字,报出业务异常消息
     * 5.96 -> true
     * 100 -> true
     * abc -> false
     * @param source        断言的字符串
     * @param message       失败返回结果
     */
    public static void isNumber(String source, String message) {
        if (!NumberUtils.isCreatable(source)) {
            throw new BizException(message);
        }
    }

    /**
     * 判断字符串是否全为数字，若不为数字,报出业务异常消息
     * 5.96 -> false
     * 100 -> true
     * @param source        断言的字符串
     * @param message       失败返回结果
     */
    public static void isDigits(String source, String message) {
        if (!NumberUtils.isDigits(source)) {
            throw new BizException(message);
        }
    }

    /**
     * 断言数据的长度范围
     * @param source        断言的字符串
     * @param min           最小长度值
     * @param max           最大长度值
     * @param message       失败返回结果
     */
    public static void validateLength(String source, int min, int max, String message) {
        if (StringUtils.isBlank(source)) {
            throw new BizException(message);
        }

        int len = StringUtils.length(source);
        if (len < min || len > max) {
            throw new BizException(message);
        }
    }

    public static void main(String[] args){
        AssertBiz.isNotNull(null, "asdfasd");
    }

}


