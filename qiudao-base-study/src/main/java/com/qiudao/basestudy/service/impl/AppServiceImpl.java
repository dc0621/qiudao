package com.qiudao.basestudy.service.impl;

import com.github.pagehelper.PageInfo;
import com.qiudao.basestudy.bo.App;
import com.qiudao.basestudy.dao.IAppDao;
import com.qiudao.basestudy.service.IAppService;
import com.qiudao.basestudy.util.result.BaseResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Description: App Service 实现
 * @author: gdc
 * @date: 2020/3/21
 * @version 1.0
 */
@Service
public class AppServiceImpl implements IAppService {

    @Resource
    private IAppDao appDao;

    @Override
    public BaseResult get(Long id) {
        App app = appDao.get(id);
        return BaseResult.success(app);
    }

    @Override
    public BaseResult list() {
        List<App> result = appDao.getAppList(new App());
        return BaseResult.success(result);
    }

    @Override
    public BaseResult insert(App app) {
        appDao.insert(app);
        return BaseResult.success();
    }

    @Override
    public BaseResult delete(Long id) {
        appDao.delete(id);
        return BaseResult.success();
    }

    @Override
    public BaseResult getPageByQuery(Integer pageNum, Integer pageSize) {
        PageInfo info = appDao.findAppByQuery(new App(), pageNum, pageSize);
        return BaseResult.success(info);
    }
}
