package com.qiudao.basestudy.service;

import com.qiudao.basestudy.bo.EmergencyManager;
import com.qiudao.basestudy.util.result.BaseResult;
import com.qiudao.basestudy.vo.EmergencyVO;

import java.util.List;

/**
 * Description: 联系人 Service
 * @author: gdc
 * @date: 2020/3/21
 * @version 1.0
 */
public interface IEmergencyManagerService {

    /**
     * 通过ID获取联系人信息
     * @param id        ID
     * @return          结果
     */
    BaseResult<EmergencyManager> get(Long id);

    /**
     *  通过ID列表获取 联系人信息列表
     * @param idList    ID 列表
     * @return          结果
     */
    BaseResult<List<EmergencyManager>> listByIdList(List<Long> idList);

    /**
     * 通过客户ID 查询紧急联系人列表
     * @param customerId    客户ID
     * @return              紧急联系人列表
     */
    BaseResult<List<EmergencyVO>> listByCustomerId(Long customerId);

    /**
     * 新增联系人信息
     * @param record    联系人信息
     * @return          结果
     */
    BaseResult<Integer> insert(EmergencyManager record);

}
