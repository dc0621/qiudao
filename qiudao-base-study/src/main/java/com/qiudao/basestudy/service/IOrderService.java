package com.qiudao.basestudy.service;

/**
 * Description: 订单业务接口， 策略模式学习
 * @author: gdc
 * @date: 2020/3/14
 * @version 1.0
 */
public interface IOrderService {

    /**
     * 根据订单的不同类型作出不同的处理
     *
     * @param type      类型
     * @return 为了简单，返回字符串
     */
    String handle(String type);

}