package com.qiudao.basestudy.service;

import com.qiudao.basestudy.bo.App;
import com.qiudao.basestudy.util.result.BaseResult;

/**
 * Description: App Service
 * @author: gdc
 * @date: 2020/3/21
 * @version 1.0
 */
public interface IAppService {

    /**
     * 通过ID获取应用信息
     * @param id        ID
     * @return          结果
     */
    BaseResult get(Long id);

    /**
     * 获取所有的信息
     * @return          结果
     */
    BaseResult list();

    /**
     * 新增应用信息
     * @param app       应用
     * @return          结果
     */
    BaseResult insert(App app);

    /**
     * 通过ID 删除应用信息  
     * @param id        ID
     * @return          结果
     */
    BaseResult delete(Long id);

    /**
     * 获取应用的分页数据
     * @return          结果
     * @param pageNum   页码
     * @param pageSize  页面大小
     */
    BaseResult getPageByQuery(Integer pageNum, Integer pageSize);

}
