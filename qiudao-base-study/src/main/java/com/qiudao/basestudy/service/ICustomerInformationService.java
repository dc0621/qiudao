package com.qiudao.basestudy.service;

import com.qiudao.basestudy.bo.CustomerInformation;
import com.qiudao.basestudy.util.result.BaseResult;

/**
 * Description: 客户 Service
 * @author: gdc
 * @date: 2020/3/21
 * @version 1.0
 */
public interface ICustomerInformationService {

    /**
     * 通过ID获取客户信息
     * @param id        ID
     * @return          结果
     */
    BaseResult get(Long id);

    /**
     * 获取所有的客户信息
     * @return          结果
     */
    BaseResult list();

    /**
     * 新增客户信息
     * @param record    客户信息
     * @return          结果
     */
    BaseResult insert(CustomerInformation record);

    /**
     * 获取应用的分页数据
     * @return          结果
     * @param pageNum   页码
     * @param pageSize  页面大小
     */
    BaseResult getPageByQuery(Integer pageNum, Integer pageSize);

}
