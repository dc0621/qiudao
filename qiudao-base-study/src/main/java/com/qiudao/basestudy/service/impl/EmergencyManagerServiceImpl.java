package com.qiudao.basestudy.service.impl;

import com.qiudao.basestudy.bo.EmergencyManager;
import com.qiudao.basestudy.dao.IEmergencyManagerDao;
import com.qiudao.basestudy.service.ICustomerUserBindingInfoService;
import com.qiudao.basestudy.service.IEmergencyManagerService;
import com.qiudao.basestudy.util.result.BaseResult;
import com.qiudao.basestudy.vo.EmergencyVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Description: 联系人 Service 实现
 * @author: gdc
 * @date: 2020/3/21
 * @version 1.0
 */
@Service
public class EmergencyManagerServiceImpl implements IEmergencyManagerService {

    @Resource
    private IEmergencyManagerDao emergencyManagerDao;
    @Resource
    private ICustomerUserBindingInfoService customerUserBindingInfoService;

    @Override
    public BaseResult<EmergencyManager> get(Long id) {
        EmergencyManager emergencyManager = emergencyManagerDao.selectByPrimaryKey(id);
        return BaseResult.success(emergencyManager);
    }

    @Override
    public BaseResult<List<EmergencyManager>> listByIdList(List<Long> idList) {
        List<EmergencyManager> emergencyList = emergencyManagerDao.listByIdList(idList);
        return BaseResult.success(emergencyList);
    }

    @Override
    public BaseResult<List<EmergencyVO>> listByCustomerId(Long customerId) {
        List<Long> emergencyIdList = customerUserBindingInfoService.getEmergencyIdListByCustomerId(customerId).getData();
        List<EmergencyVO> emergencyVOs = new ArrayList<>();
        Optional.ofNullable(emergencyManagerDao.listByIdList(emergencyIdList))
                .orElse(new ArrayList<>())
                .forEach(emergencyManager -> {
                    EmergencyVO emergencyVO = new EmergencyVO();
                    BeanUtils.copyProperties(emergencyManager, emergencyVO);
                    emergencyVOs.add(emergencyVO);
                });
        return BaseResult.success(emergencyVOs);
    }

    @Override
    public BaseResult<Integer> insert(EmergencyManager record) {
        int result = emergencyManagerDao.insert(record);
        return BaseResult.success(result);
    }

}
