package com.qiudao.basestudy.service.impl;

import com.qiudao.basestudy.bo.CustomerInformation;
import com.qiudao.basestudy.dao.ICustomerInformationDao;
import com.qiudao.basestudy.service.ICustomerInformationService;
import com.qiudao.basestudy.util.result.BaseResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Description: App Service 实现
 * @author: gdc
 * @date: 2020/3/21
 * @version 1.0
 */
@Service
public class CustomerInformationServiceImpl implements ICustomerInformationService {

    @Resource
    private ICustomerInformationDao customerInformationDao;

    @Override
    public BaseResult get(Long id) {
        CustomerInformation customerInformation = customerInformationDao.selectByPrimaryKey(id);
        return BaseResult.success(customerInformation);
    }

    @Override
    public BaseResult list() {
        return null;
    }

    @Override
    public BaseResult insert(CustomerInformation record) {
        int result = customerInformationDao.insert(record);
        return BaseResult.success(result);
    }

    @Override
    public BaseResult getPageByQuery(Integer pageNum, Integer pageSize) {
        return null;
    }
}
