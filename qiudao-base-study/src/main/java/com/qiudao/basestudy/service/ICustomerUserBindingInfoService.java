package com.qiudao.basestudy.service;

import com.qiudao.basestudy.bo.CustomerUserBindingInfo;
import com.qiudao.basestudy.util.result.BaseResult;

import java.util.List;

/**
 * Description: 客户绑定信息 Service
 * @author: gdc
 * @date: 2020/3/21
 * @version 1.0
 */
public interface ICustomerUserBindingInfoService {

    /**
     * 通过ID获取客户绑定信息
     * @param id        ID
     * @return          结果
     */
    BaseResult get(Long id);

    /**
     * 通过条件获取 客户绑定列表
     * @param customerId        客户ID
     * @param shardingUserId    用户（分享人、紧急联系人）ID
     * @param shardingUserType  类型
     * @return                  结果
     */
    BaseResult listByQuery(Long customerId, Long shardingUserId, Short shardingUserType);

    /**
     * 通过条件获取 客户绑定列表
     * @param customerId        客户ID
     * @return                  结果
     */
    BaseResult<List<Long>> getEmergencyIdListByCustomerId(Long customerId);

    /**
     * 新增客户绑定信息
     * @param record    绑定信息
     * @return          结果
     */
    BaseResult insert(CustomerUserBindingInfo record);

    /**
     * 获取客户绑定的分页数据
     * @param pageNum   页码
     * @param pageSize  页面大小
     * @return          结果
     */
    BaseResult getPageByQuery(Integer pageNum, Integer pageSize);

}
