package com.qiudao.basestudy.service.impl;

import com.qiudao.basestudy.service.IOrderService;
import com.qiudao.basestudy.util.strategy.orderHandler.IOrderHandler;
import com.qiudao.basestudy.util.strategy.HandlerContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Description: 订单业务实现， 策略模式学习 
 * @author: gdc
 * @date: 2020/3/22
 * @version 1.0
 */
@Service
public class OrderServiceImpl implements IOrderService {

    @Autowired
    private HandlerContext handlerContext;

    /**
     * Description: 使用策略模式 
     * @author: gdc
     * @date: 2020/3/14
     * @version 1.0
     */
    @Override
    public String handle(String type) {
        IOrderHandler handler = handlerContext.getInstance(type);
        return handler.handle();
    }


    /**
     * 原方法，通过传入值 的 type 属性，执行指定方法
     */
    /*@Override
    public String handle(OrderDTO dto) {
        String type = dto.getType();
        if ("1".equals(type)) {
            return "处理普通订单";
        } else if ("2".equals(type)) {
            return "处理团购订单";
        } else if ("3".equals(type)) {
            return "处理促销订单";
        }
        return null;
    }*/

}