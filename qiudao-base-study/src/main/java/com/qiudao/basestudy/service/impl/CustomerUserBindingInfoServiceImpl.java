package com.qiudao.basestudy.service.impl;

import com.qiudao.basestudy.bo.CustomerUserBindingInfo;
import com.qiudao.basestudy.constant.Constants;
import com.qiudao.basestudy.dao.ICustomerUserBindingInfoDao;
import com.qiudao.basestudy.service.ICustomerUserBindingInfoService;
import com.qiudao.basestudy.util.result.BaseResult;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Description: App Service 实现
 * @author: gdc
 * @date: 2020/3/21
 * @version 1.0
 */
@Service
public class CustomerUserBindingInfoServiceImpl implements ICustomerUserBindingInfoService {

    @Resource
    private ICustomerUserBindingInfoDao customerUserBindingInfoDao;

    @Override
    public BaseResult get(Long id) {
        CustomerUserBindingInfo customerUserBindingInfo = customerUserBindingInfoDao.selectByPrimaryKey(id);
        return BaseResult.success(customerUserBindingInfo);
    }

    @Override
    public BaseResult listByQuery(Long customerId, Long shardingUserId, Short shardingUserType) {
        List<CustomerUserBindingInfo> customerUserBindingInfos = customerUserBindingInfoDao.listByQuery(customerId, shardingUserId, shardingUserType);
        return BaseResult.success(customerUserBindingInfos);
    }

    @Override
    public BaseResult<List<Long>> getEmergencyIdListByCustomerId(Long customerId) {
        List<Long> idList = Optional.ofNullable(customerUserBindingInfoDao.listByQuery(customerId, null, Constants.SHARDING_USERTYPE_EMERGENCY))
                .orElse(new ArrayList<>())
                .stream()
                .map(CustomerUserBindingInfo::getShardingUserId)
                .distinct()
                .collect(Collectors.toList());
        return BaseResult.success(idList);
    }

    @Override
    public BaseResult insert(CustomerUserBindingInfo record) {
        int result = customerUserBindingInfoDao.insert(record);
        return BaseResult.success(result);
    }

    @Override
    public BaseResult getPageByQuery(Integer pageNum, Integer pageSize) {
        return null;
    }
}
