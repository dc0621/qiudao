package com.qiudao.basestudy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * - @SpringBootApplication: Spring Boot应用标注在某个类上说明这个类是SpringBoot的主配置类
 * - @ImportResource：导入Spring的配置文件，让配置文件里面的内容生效；
 */
@ImportResource(value = "classpath:/spring/person-service.xml")
@SpringBootApplication
@MapperScan(basePackages = {"com.qiudao.basestudy.mapper"})
public class BaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaseApplication.class, args);
    }

}
