package com.qiudao.basestudy.constant;

/**
 * Description: 基本常量信息
 * @author: gdc
 * @date: 2020/4/4
 * @version 1.0
 */
public class Constants {

    /**
     * 基础常量 0 禁用   1启用
     */
    public static final Short CONSTANT_ZERO = 0;
    public static final Short CONSTANT_ONE = 1;
    public static final Short CONSTANT_TWO = 2;
    public static final Short CONSTANT_THREE = 3;
    public static final Short CONSTANT_FOUR = 4;
    public static final Short CONSTANT_FIVE = 5;
    public static final Short CONSTANT_SIX = 6;
    public static final Short CONSTANT_SEVEN = 7;
    public static final Short CONSTANT_EIGHT = 8;

    public static final Integer INIT_MAP_SIZE = 16;

    /**
     * 用户和用户绑定类型：1：分享人 2：紧急联系人
     */
    public final static Short SHARDING_USERTYPE_SHARE = 1;
    public final static Short SHARDING_USERTYPE_EMERGENCY = 2;


}
