package com.qiudao.basestudy.mapper;

import com.qiudao.basestudy.bo.CustomerUserBindingInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Description: 客户、用户绑定 Mapper
 * @author: gdc
 * @date: 2020/4/3
 * @version 1.0
 */
public interface CustomerUserBindingInfoMapper {

    /**
     * 通过ID 查询信息
     * @param id            ID
     * @return              客户、用户绑定信息
     */
    CustomerUserBindingInfo selectByPrimaryKey(Long id);

    /**
     * 新增
     * @param record        绑定信息
     * @return              结果
     */
    int insert(CustomerUserBindingInfo record);

    /**
     * 批量新增
     * @param items         绑定信息
     */
    void insertList(@Param("items") List<CustomerUserBindingInfo> items);

    /**
     * 更新
     * @param record        绑定信息
     * @return              结果
     */
    int update(CustomerUserBindingInfo record);

    /**
     * 批量更新状态
     * @param record        状态信息
     * @param idList        ID 列表
     * @return              结果
     */
    int updateList(@Param("record") CustomerUserBindingInfo record, @Param("idList") List<Long> idList);

    /**
     * 通过条件查询 绑定信息列表
     * @param map           条件
     * @return              结果
     */
    List<CustomerUserBindingInfo> findListByQuery(Map<String, Object> map);
}