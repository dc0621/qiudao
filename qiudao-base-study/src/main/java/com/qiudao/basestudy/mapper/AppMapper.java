package com.qiudao.basestudy.mapper;

import com.qiudao.basestudy.bo.App;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Description: App Mapper
 * @author: gdc
 * @date: 2020/3/21
 * @version 1.0
 */
public interface AppMapper {

    /**
     * 通过ID 获取 App
     * @param id        ID
     * @return          结果
     */
    App selectByPrimaryKey(Long id);

    /**
     * 通过查询条件获取应用列表
     * @param map               查询条件
     * @return                  应用列表
     */
    List<App> findListByQuery(Map<String, Object> map);

    /**
     * 新增 APP
     * @param record        APP
     * @return              结果
     */
    int insertSelective(App record);

    /**
     * 批量新增
     * @param items         App List
     */
    void batchInsert(@Param("items") List<App> items);

    /**
     * 更新 App
     * @param record        APP
     * @return              结果
     */
    int updateByPrimaryKeySelective(App record);

    /**
     * 通过ID 删除 App
     * @param id            ID
     * @return              结果
     */
    int deleteByPrimaryKey(Long id);




}