package com.qiudao.basestudy;

import org.junit.Test;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.api.ProgressCallback;
import org.mybatis.generator.api.VerboseProgressCallback;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GeneratorMapperPOTest {

    @Test
    public void testGenerator() {
        String xmlProjectPath = System.getProperty("user.dir") + "/src/main/resources";

        try {
            List<String> warnings = new ArrayList<>();
            File configFile = new File(xmlProjectPath + "/mybatis-generator/generator-config.xml");
            ConfigurationParser cp = new ConfigurationParser(warnings);
            Configuration config = cp.parseConfiguration(configFile);


            DefaultShellCallback callback = new DefaultShellCallback(true);
            ProgressCallback progressCallback = new VerboseProgressCallback();
            MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
            myBatisGenerator.generate(progressCallback);
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}