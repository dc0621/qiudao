package com.qiudao.basestudy;

import com.qiudao.basestudy.bo.Person;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@ImportResource(value = "classpath:/spring/person-service.xml")
public class BaseApplicationTests {

    @Resource
    private Person person;
    @Resource
    private ApplicationContext applicationContext;

    /**
     * 测试从指定属性文件读取属性信息
     */
    @Test
    public void testPropertiesRead() {
        log.info("通过属性读取到的信息为：【{}】", person);
    }

    /**
     * 测试 @ImportResource 注解是否将配置文件导入成功
     */
    @Test
    public void testImportResourceAnno(){
        boolean flag = applicationContext.containsBean("personService");
        if (flag) {
            log.info("@ImportResource 将配置文件注入成功，Bean 已导入 ");
        } else {
            log.error("@ImportResource 导入配置文件失败！！！");
        }
    }

    /**
     * 测试 @Bean 注解是否将 Bean 对象注入成功
     */
    @Test
    public void testBeanAnno(){
        boolean flag = applicationContext.containsBean("createRestApi");
        if (flag) {
            log.info("@Bean 将配置文件注入成功，Bean 已导入 ");
        } else {
            log.error("@Bean 导入配置文件失败！！！");
        }
    }

}
